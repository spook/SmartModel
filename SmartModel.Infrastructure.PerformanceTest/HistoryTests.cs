﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using SmartModel.Infrastructure.PerformanceTest.TestObjects;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SmartModel.Infrastructure.PerformanceTest
{
    [TestClass]
    public class HistoryTests
    {
        private const int GLOBAL_CHANGE_COUNT = 100000;

        [TestMethod]
        public void AssignmentTest()
        {
            const int CHANGE_COUNT = GLOBAL_CHANGE_COUNT;

            // Arrange

            var obj = new SingleFieldDocument();

            // Act

            var sw1 = Stopwatch.StartNew();
            for (int i = 0; i < CHANGE_COUNT; i++)
            {
                obj.IntField = i;
            }
            sw1.Stop();

            var history = new History();
            obj.History = history;

            Stopwatch sw2;
            using (history.StartUndoScope())
            {
                sw2 = Stopwatch.StartNew();
                for (int i = 0; i < CHANGE_COUNT; i++)
                {
                    obj.IntField = i;
                }
                sw2.Stop();
            }

            Stopwatch sw3;
            using (history.StartBinaryUndoScope(10000000))
            {
                sw3 = Stopwatch.StartNew();
                for (int i = 0; i < CHANGE_COUNT; i++)
                {
                    obj.IntField = i;
                }
                sw3.Stop();
            }

            history.Dispose();

            var elapsedMillis = Math.Max(1, sw1.ElapsedMilliseconds);
            Trace.WriteLine($"{CHANGE_COUNT} changes without history took {elapsedMillis}ms - {CHANGE_COUNT / elapsedMillis * 1000} changes per second.");
            elapsedMillis = Math.Max(1, sw2.ElapsedMilliseconds);
            Trace.WriteLine($"{CHANGE_COUNT} changes with history took {elapsedMillis}ms - {CHANGE_COUNT / elapsedMillis * 1000} changes per second.");
            elapsedMillis = Math.Max(1, sw3.ElapsedMilliseconds);
            Trace.WriteLine($"{CHANGE_COUNT} changes with binary scope took {elapsedMillis}ms - {CHANGE_COUNT / elapsedMillis * 1000} changes per second.");
        }

        [TestMethod]
        public void CollectionAddTest()
        {
            const int CHANGE_COUNT = GLOBAL_CHANGE_COUNT;

            // Arrange

            var obj = new CollectionDocument();

            // Act

            var sw1 = Stopwatch.StartNew();
            for (int i = 0; i < CHANGE_COUNT; i++)
            {
                obj.Collection.Add(new CollectionItem());
            }
            sw1.Stop();

            obj = new CollectionDocument();
            var history = new History();
            obj.History = history;

            Stopwatch sw2;
            using (history.StartUndoScope())
            {
                sw2 = Stopwatch.StartNew();
                for (int i = 0; i < CHANGE_COUNT; i++)
                {
                    obj.Collection.Add(new CollectionItem());
                }
                sw2.Stop();
            }

            Stopwatch sw3;
            using (history.StartBinaryUndoScope(10000000))
            {
                sw3 = Stopwatch.StartNew();
                for (int i = 0; i < CHANGE_COUNT; i++)
                {
                    obj.Collection.Add(new CollectionItem());
                }
                sw3.Stop();
            }

            history.Dispose();

            var elapsedMillis = Math.Max(1, sw1.ElapsedMilliseconds);
            Trace.WriteLine($"{CHANGE_COUNT} items added without history took {elapsedMillis}ms - {CHANGE_COUNT / elapsedMillis * 1000} items per second.");
            elapsedMillis = Math.Max(1, sw2.ElapsedMilliseconds);
            Trace.WriteLine($"{CHANGE_COUNT} items added with history took {elapsedMillis}ms - {CHANGE_COUNT / elapsedMillis * 1000} items per second.");
            elapsedMillis = Math.Max(1, sw3.ElapsedMilliseconds);
            Trace.WriteLine($"{CHANGE_COUNT} items added with binary scope took {elapsedMillis}ms - {CHANGE_COUNT / elapsedMillis * 1000} items per second.");
        }

        [TestMethod]
        public void CollectionPropertyChangeTest()
        {
            const int ITEMS_COUNT = GLOBAL_CHANGE_COUNT;
            const int CHANGE_COUNT = 10;

            // Arrange

            var obj = new CollectionDocument();
            for (int i = 0; i < ITEMS_COUNT; i++)
            {
                obj.Collection.Add(new CollectionItem());
            }

            // Act

            var sw1 = Stopwatch.StartNew();
            for (int i = 0; i < CHANGE_COUNT; i++)
            {
                for (int j = 0; j < ITEMS_COUNT; j++)
                    obj.Collection[j].Field = i;
            }
            sw1.Stop();

            var history = new History();
            obj.History = history;

            Stopwatch sw2;
            using (history.StartUndoScope())
            {
                sw2 = Stopwatch.StartNew();
                for (int i = 0; i < CHANGE_COUNT; i++)
                {
                    for (int j = 0; j < ITEMS_COUNT; j++)
                        obj.Collection[j].Field = i;
                }
                sw2.Stop();
            }

            Stopwatch sw3;
            using (history.StartBinaryUndoScope(10000000))
            {
                sw3 = Stopwatch.StartNew();
                for (int i = 0; i < CHANGE_COUNT; i++)
                {
                    for (int j = 0; j < ITEMS_COUNT; j++)
                        obj.Collection[j].Field = i;
                }
                sw3.Stop();
            }

            history.Dispose();

            var elapsedMillis = Math.Max(1, sw1.ElapsedMilliseconds);
            Trace.WriteLine($"{CHANGE_COUNT} changes for {ITEMS_COUNT} items without history took {elapsedMillis}ms - {(CHANGE_COUNT*ITEMS_COUNT) / elapsedMillis * 1000} changes per second.");
            elapsedMillis = Math.Max(1, sw2.ElapsedMilliseconds);
            Trace.WriteLine($"{CHANGE_COUNT} changes for {ITEMS_COUNT} items with history took {elapsedMillis}ms - {(CHANGE_COUNT*ITEMS_COUNT) / elapsedMillis * 1000} changes per second.");
            elapsedMillis = Math.Max(1, sw3.ElapsedMilliseconds);
            Trace.WriteLine($"{CHANGE_COUNT} changes for {ITEMS_COUNT} items with binary scope took {elapsedMillis}ms - {(CHANGE_COUNT * ITEMS_COUNT) / elapsedMillis * 1000} changes per second.");
        }
    }
}
