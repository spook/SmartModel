﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using SmartModel.Infrastructure.PerformanceTest.TestObjects;
using System;
using System.Diagnostics;
using System.IO;
using System.Text;
using System.Xml.Serialization;

namespace SmartModel.Infrastructure.PerformanceTest
{
    [TestClass]
    public class SerializationTests
    {
        [TestMethod]
        public void ReadMultipleItemsTest()
        {
            const int ITEM_COUNT = 10000;

            StringBuilder sb = new StringBuilder();

            sb.Append("<?xml version=\"1.0\" encoding=\"utf-16\"?>")
                .Append("<CollectionObject xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" xmlns:xsd=\"http://www.w3.org/2001/XMLSchema\">")
                .Append("<Collection>");

            for (int i = 0; i < ITEM_COUNT; i++)
            {
                sb.Append("<Item IntField=\"0\" />");
            }

            sb.Append("</Collection>")
                .Append("</CollectionObject>");

            string xml = sb.ToString();
            System.IO.StringReader sr = new StringReader(xml);

            XmlSerializer serializer = new XmlSerializer(typeof(CollectionDocument));

            Stopwatch sw = Stopwatch.StartNew();
            var obj = (CollectionDocument)serializer.Deserialize(sr);
            sw.Stop();

            System.Diagnostics.Debug.WriteLine($"Reading collection with {ITEM_COUNT} items took {sw.ElapsedMilliseconds}ms.");

            Assert.AreEqual(ITEM_COUNT, obj.Collection.Count);
        }
    }
}
