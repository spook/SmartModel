﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;

namespace SmartModel.Infrastructure.PerformanceTest.TestObjects
{
    public class CollectionItem : BaseCollectionItem
    {
        private readonly IField<int> intField;

        public CollectionItem()
        {
            intField = RegisterIntField(nameof(Field), 0);
        }

        [XmlAttribute("IntField")]
        public int Field
        {
            get => intField.Value;
            set => intField.Value = value;
        }
    }
}
