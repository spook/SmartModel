﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SmartModel.Infrastructure.PerformanceTest.TestObjects
{
    public class SingleFieldDocument : BaseDocument
    {
        private IField<int> intField;

        public SingleFieldDocument()
        {
            intField = RegisterIntField(nameof(IntField), 0);
        }

        public int IntField
        {
            get => intField.Value;
            set => intField.Value = value;
        }
    }
}
