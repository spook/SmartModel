﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;

namespace SmartModel.Infrastructure.PerformanceTest.TestObjects
{
    [XmlRoot(ElementName = "CollectionObject")]
    public class CollectionDocument : BaseDocument
    {
        private readonly RefCollection<CollectionItem> collection;

        public CollectionDocument()
        {
            collection = RegisterRefCollection<CollectionItem>(nameof(Collection));
        }

        [XmlArray("Collection")]
        [XmlArrayItem(ElementName = "Item", Type = typeof(CollectionItem))]
        public RefCollection<CollectionItem> Collection => collection;
    }
}
