﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SmartModel.Infrastructure
{
    internal enum ChangeSource
    {
        /// <summary>Source of the operation is normal usage, so
        /// an undo entry should be created. Additionally, redo stack
        /// should be cleared.</summary>
        Usage,
        /// <summary>Source of the operation is undo, so
        /// a redo operation should be created.</summary>
        Undo,
        /// <summary>Source of the operation is redo, so
        /// an undo operation should be created.</summary>
        Redo
    }
}
