﻿namespace SmartModel.Infrastructure.Types
{
	public enum CollectionType : byte
	{
		SbyteType = 0,
		ByteType = 1,
		ShortType = 2,
		UshortType = 3,
		IntType = 4,
		UintType = 5,
		LongType = 6,
		UlongType = 7,
		FloatType = 8,
		DoubleType = 9,
		DecimalType = 10,
		BoolType = 11,
		CharType = 12,
		StringType = 13,
		RefType
	}
}