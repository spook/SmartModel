﻿namespace SmartModel.Infrastructure
{
    public interface IField<T>
    {
        T Value { get; set; }
    }
}