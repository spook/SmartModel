﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading.Tasks;

namespace SmartModel.Infrastructure
{
    internal delegate void OnUpdateSnapshotDelegate(long instanceId, BaseFieldItemContainer snapshot);

    [DebuggerDisplay("[{InstanceId}] {GetType().Name}, {Fields.Count} fields, {Collections.Count} collections")]
    public abstract partial class BaseFieldItemContainer : BaseObject
    {
        // Private fields -----------------------------------------------------

        private static long instanceIdGenerator = 0;
        private long instanceId;

        private readonly Dictionary<string, BaseField> fields = new Dictionary<string, BaseField>();
        private readonly Dictionary<string, BaseCollection> collections = new Dictionary<string, BaseCollection>();

        // Private methods ----------------------------------------------------

        private void CheckExistingMember(string name)
        {
            if (fields.ContainsKey(name) || collections.ContainsKey(name))
                throw new ArgumentException($"Member with name {name} already exists!", nameof(name));
        }

        // Protected methods --------------------------------------------------

        protected IField<T> RegisterRefField<T>(string name, T defaultValue = default)
            where T : BaseItem, new()
        {
            CheckExistingMember(name);

            var field = new RefField<T>(name, this, defaultValue);
            fields[name] = field;

            return field;
        }

        protected RefCollection<TItem> RegisterRefCollection<TItem>(string name)
            where TItem : BaseCollectionItem, new()
        {
            CheckExistingMember(name);

            var collection = new RefCollection<TItem>(this, name);
            collections[name] = collection;

            return collection;
        }

        protected Collection<TItem> RegisterCollection<TItem>(string name)
        {
            CheckExistingMember(name);

            var collection = new Collection<TItem>(this, name);
            collections[name] = collection;

            return collection;
        }

        // Internal methods ---------------------------------------------------

        internal override void ResetDocumentRecursive()
        {
            base.ResetDocumentRecursive();

            foreach (var kvp in fields)
                kvp.Value.ClearDocument();
        }

        internal BaseFieldItemContainer CreateSnapshot(OnUpdateSnapshotDelegate onNewSnapshot)
        {
            var snapshot = (BaseFieldItemContainer)Activator.CreateInstance(this.GetType());

            // Snapshot will retain current instance ID. This object's
            // instance ID will change, so that it may be reused (eg.
            // added somewhere else in the model).
            // I exchange IDs to retain continuity in instance numbering.
            var temp = snapshot.instanceId;
            snapshot.instanceId = this.instanceId;
            this.instanceId = temp;

            // Clone fields
            foreach (var kvp in fields)
            {
                var otherField = snapshot.fields[kvp.Key];
                kvp.Value.SnapshotInto(otherField, onNewSnapshot);
            }

            // Clone collections
            foreach (var kvp in collections)
            {
                var otherCollection = snapshot.collections[kvp.Key];
                kvp.Value.SnapshotInto(otherCollection, onNewSnapshot);
            }

            onNewSnapshot?.Invoke(snapshot.instanceId, snapshot);

            return snapshot;
        }

        internal static BaseFieldItemContainer DeserializeSnapshot(BinaryReader reader, OnUpdateSnapshotDelegate onUpdateSnapshot)
        {
            // Restore object type
            string typeStr = reader.ReadString();

            // Instantiate object
            var type = Type.GetType(typeStr);
            if (!typeof(BaseFieldItemContainer).IsAssignableFrom(type))
                throw new InvalidOperationException("Invalid serialized item!");

            var snapshot = (BaseFieldItemContainer)Activator.CreateInstance(type);

            // Restore instance ID
            long instanceId = reader.ReadInt64();
            snapshot.instanceId = instanceId;

            // Restore fields
            int fieldCount = reader.ReadInt32();
            for (int i = 0; i < fieldCount; i++)
            {
                string key = reader.ReadString();
                var field = snapshot.Fields[key];

                field.DeserializeSnapshot(reader, onUpdateSnapshot);
            }

            // Restore collections
            int collectionCount = reader.ReadInt32();
            for (int i = 0; i < collectionCount; i++)
            {
                string key = reader.ReadString();
                var collection = snapshot.Collections[key];

                collection.DeserializeSnapshot(reader, onUpdateSnapshot);
            }

            onUpdateSnapshot?.Invoke(snapshot.instanceId, snapshot);
            return snapshot;
        }

        internal void CreateBinarySnapshot(BinaryWriter writer, OnUpdateSnapshotDelegate onNewSnapshot)
        {
            // Snapshot will retain current instance ID. This object's
            // instance ID will change, so that it may be reused (eg.
            // added somewhere else in the model).
            // I exchange IDs to retain continuity in instance numbering.

            // Store object type
            writer.Write(this.GetType().AssemblyQualifiedName);

            // Store instance ID
            writer.Write(instanceId);

            // Store fields
            writer.Write(fields.Count);
            foreach (var kvp in fields)
            {
                writer.Write(kvp.Key);
                kvp.Value.BinarySnapshotInto(writer, onNewSnapshot);
            }

            // Store collections
            writer.Write(collections.Count);
            foreach (var kvp in collections)
            {
                writer.Write(kvp.Key);
                kvp.Value.BinarySnapshotInto(writer, onNewSnapshot);
            }

            // There is no new snapshot instance, because it is serialized as bytes
            onNewSnapshot.Invoke(instanceId, null);

            this.instanceId = ++instanceIdGenerator;
        }

        // Internal properties ------------------------------------------------

        internal long InstanceId => instanceId;

        internal IReadOnlyDictionary<string, BaseField> Fields => fields;

        internal IReadOnlyDictionary<string, BaseCollection> Collections => collections;

        // Public methods -----------------------------------------------------

        public BaseFieldItemContainer()
        {
            instanceId = ++instanceIdGenerator;
        }

    }
}
