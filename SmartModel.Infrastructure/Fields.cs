﻿using SmartModel.Infrastructure.HistoryEntries;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;

namespace SmartModel.Infrastructure
{


    // *** sbyte ***

    internal class SbyteField : BaseTypedField<sbyte>
    {
        private sbyte value;

        private void SetValue(sbyte value)
        {
            Document?.History?.AddSbyteValueChangedEntry(this, this.value, ChangeSource.Usage);

            this.value = value;
        }

        private void Apply(sbyte oldValue, bool undo, OnUpdateSnapshotDelegate onUpdateSnapshot)
        {
            Document.History.AddSbyteValueChangedEntry(this, value, undo ? ChangeSource.Undo : ChangeSource.Redo);
            value = oldValue;
        }

        internal SbyteField(string name, BaseFieldItemContainer parent, sbyte defaultValue = default(sbyte))
            : base(name, parent)
        {
            value = defaultValue;
        }

        internal override void SnapshotInto(BaseField other, OnUpdateSnapshotDelegate onNewSnapshot)
        {
            var typedOther = (SbyteField)other;
            typedOther.value = value;
        }

        internal override void BinarySnapshotInto(BinaryWriter writer, OnUpdateSnapshotDelegate onCreateSnapshot)
        {
            writer.Write(value);
        }

        internal override void DeserializeSnapshot(BinaryReader reader, OnUpdateSnapshotDelegate onUpdateSnapshot)
        {
            value = reader.ReadSByte();
        }

        internal override void Apply(BinaryReader reader, HistoryEntryType historyEntry, bool undo, OnUpdateSnapshotDelegate onUpdateSnapshot)        
        {
            if (historyEntry != HistoryEntryType.SbyteValueChange)
                throw new InvalidOperationException("Invalid history entry type!");

            var oldValue = reader.ReadSByte();
            Apply(oldValue, undo, onUpdateSnapshot);
        }

        internal override void Apply(BaseHistoryEntry entry, bool undo, OnUpdateSnapshotDelegate onUpdateSnapshot)
        {
            if (entry is SbyteFieldValueChangedHistoryEntry valueEntry)
            {
                Apply(valueEntry.OldValue, undo, onUpdateSnapshot);
            }
            else
                throw new InvalidOperationException("Unsupported history entry!");
        }

        public override sbyte Value
        {
            get => value;
            set => SetValue(value);
        }        
    }

    // *** byte ***

    internal class ByteField : BaseTypedField<byte>
    {
        private byte value;

        private void SetValue(byte value)
        {
            Document?.History?.AddByteValueChangedEntry(this, this.value, ChangeSource.Usage);

            this.value = value;
        }

        private void Apply(byte oldValue, bool undo, OnUpdateSnapshotDelegate onUpdateSnapshot)
        {
            Document.History.AddByteValueChangedEntry(this, value, undo ? ChangeSource.Undo : ChangeSource.Redo);
            value = oldValue;
        }

        internal ByteField(string name, BaseFieldItemContainer parent, byte defaultValue = default(byte))
            : base(name, parent)
        {
            value = defaultValue;
        }

        internal override void SnapshotInto(BaseField other, OnUpdateSnapshotDelegate onNewSnapshot)
        {
            var typedOther = (ByteField)other;
            typedOther.value = value;
        }

        internal override void BinarySnapshotInto(BinaryWriter writer, OnUpdateSnapshotDelegate onCreateSnapshot)
        {
            writer.Write(value);
        }

        internal override void DeserializeSnapshot(BinaryReader reader, OnUpdateSnapshotDelegate onUpdateSnapshot)
        {
            value = reader.ReadByte();
        }

        internal override void Apply(BinaryReader reader, HistoryEntryType historyEntry, bool undo, OnUpdateSnapshotDelegate onUpdateSnapshot)        
        {
            if (historyEntry != HistoryEntryType.ByteValueChange)
                throw new InvalidOperationException("Invalid history entry type!");

            var oldValue = reader.ReadByte();
            Apply(oldValue, undo, onUpdateSnapshot);
        }

        internal override void Apply(BaseHistoryEntry entry, bool undo, OnUpdateSnapshotDelegate onUpdateSnapshot)
        {
            if (entry is ByteFieldValueChangedHistoryEntry valueEntry)
            {
                Apply(valueEntry.OldValue, undo, onUpdateSnapshot);
            }
            else
                throw new InvalidOperationException("Unsupported history entry!");
        }

        public override byte Value
        {
            get => value;
            set => SetValue(value);
        }        
    }

    // *** short ***

    internal class ShortField : BaseTypedField<short>
    {
        private short value;

        private void SetValue(short value)
        {
            Document?.History?.AddShortValueChangedEntry(this, this.value, ChangeSource.Usage);

            this.value = value;
        }

        private void Apply(short oldValue, bool undo, OnUpdateSnapshotDelegate onUpdateSnapshot)
        {
            Document.History.AddShortValueChangedEntry(this, value, undo ? ChangeSource.Undo : ChangeSource.Redo);
            value = oldValue;
        }

        internal ShortField(string name, BaseFieldItemContainer parent, short defaultValue = default(short))
            : base(name, parent)
        {
            value = defaultValue;
        }

        internal override void SnapshotInto(BaseField other, OnUpdateSnapshotDelegate onNewSnapshot)
        {
            var typedOther = (ShortField)other;
            typedOther.value = value;
        }

        internal override void BinarySnapshotInto(BinaryWriter writer, OnUpdateSnapshotDelegate onCreateSnapshot)
        {
            writer.Write(value);
        }

        internal override void DeserializeSnapshot(BinaryReader reader, OnUpdateSnapshotDelegate onUpdateSnapshot)
        {
            value = reader.ReadInt16();
        }

        internal override void Apply(BinaryReader reader, HistoryEntryType historyEntry, bool undo, OnUpdateSnapshotDelegate onUpdateSnapshot)        
        {
            if (historyEntry != HistoryEntryType.ShortValueChange)
                throw new InvalidOperationException("Invalid history entry type!");

            var oldValue = reader.ReadInt16();
            Apply(oldValue, undo, onUpdateSnapshot);
        }

        internal override void Apply(BaseHistoryEntry entry, bool undo, OnUpdateSnapshotDelegate onUpdateSnapshot)
        {
            if (entry is ShortFieldValueChangedHistoryEntry valueEntry)
            {
                Apply(valueEntry.OldValue, undo, onUpdateSnapshot);
            }
            else
                throw new InvalidOperationException("Unsupported history entry!");
        }

        public override short Value
        {
            get => value;
            set => SetValue(value);
        }        
    }

    // *** ushort ***

    internal class UshortField : BaseTypedField<ushort>
    {
        private ushort value;

        private void SetValue(ushort value)
        {
            Document?.History?.AddUshortValueChangedEntry(this, this.value, ChangeSource.Usage);

            this.value = value;
        }

        private void Apply(ushort oldValue, bool undo, OnUpdateSnapshotDelegate onUpdateSnapshot)
        {
            Document.History.AddUshortValueChangedEntry(this, value, undo ? ChangeSource.Undo : ChangeSource.Redo);
            value = oldValue;
        }

        internal UshortField(string name, BaseFieldItemContainer parent, ushort defaultValue = default(ushort))
            : base(name, parent)
        {
            value = defaultValue;
        }

        internal override void SnapshotInto(BaseField other, OnUpdateSnapshotDelegate onNewSnapshot)
        {
            var typedOther = (UshortField)other;
            typedOther.value = value;
        }

        internal override void BinarySnapshotInto(BinaryWriter writer, OnUpdateSnapshotDelegate onCreateSnapshot)
        {
            writer.Write(value);
        }

        internal override void DeserializeSnapshot(BinaryReader reader, OnUpdateSnapshotDelegate onUpdateSnapshot)
        {
            value = reader.ReadUInt16();
        }

        internal override void Apply(BinaryReader reader, HistoryEntryType historyEntry, bool undo, OnUpdateSnapshotDelegate onUpdateSnapshot)        
        {
            if (historyEntry != HistoryEntryType.UshortValueChange)
                throw new InvalidOperationException("Invalid history entry type!");

            var oldValue = reader.ReadUInt16();
            Apply(oldValue, undo, onUpdateSnapshot);
        }

        internal override void Apply(BaseHistoryEntry entry, bool undo, OnUpdateSnapshotDelegate onUpdateSnapshot)
        {
            if (entry is UshortFieldValueChangedHistoryEntry valueEntry)
            {
                Apply(valueEntry.OldValue, undo, onUpdateSnapshot);
            }
            else
                throw new InvalidOperationException("Unsupported history entry!");
        }

        public override ushort Value
        {
            get => value;
            set => SetValue(value);
        }        
    }

    // *** int ***

    internal class IntField : BaseTypedField<int>
    {
        private int value;

        private void SetValue(int value)
        {
            Document?.History?.AddIntValueChangedEntry(this, this.value, ChangeSource.Usage);

            this.value = value;
        }

        private void Apply(int oldValue, bool undo, OnUpdateSnapshotDelegate onUpdateSnapshot)
        {
            Document.History.AddIntValueChangedEntry(this, value, undo ? ChangeSource.Undo : ChangeSource.Redo);
            value = oldValue;
        }

        internal IntField(string name, BaseFieldItemContainer parent, int defaultValue = default(int))
            : base(name, parent)
        {
            value = defaultValue;
        }

        internal override void SnapshotInto(BaseField other, OnUpdateSnapshotDelegate onNewSnapshot)
        {
            var typedOther = (IntField)other;
            typedOther.value = value;
        }

        internal override void BinarySnapshotInto(BinaryWriter writer, OnUpdateSnapshotDelegate onCreateSnapshot)
        {
            writer.Write(value);
        }

        internal override void DeserializeSnapshot(BinaryReader reader, OnUpdateSnapshotDelegate onUpdateSnapshot)
        {
            value = reader.ReadInt32();
        }

        internal override void Apply(BinaryReader reader, HistoryEntryType historyEntry, bool undo, OnUpdateSnapshotDelegate onUpdateSnapshot)        
        {
            if (historyEntry != HistoryEntryType.IntValueChange)
                throw new InvalidOperationException("Invalid history entry type!");

            var oldValue = reader.ReadInt32();
            Apply(oldValue, undo, onUpdateSnapshot);
        }

        internal override void Apply(BaseHistoryEntry entry, bool undo, OnUpdateSnapshotDelegate onUpdateSnapshot)
        {
            if (entry is IntFieldValueChangedHistoryEntry valueEntry)
            {
                Apply(valueEntry.OldValue, undo, onUpdateSnapshot);
            }
            else
                throw new InvalidOperationException("Unsupported history entry!");
        }

        public override int Value
        {
            get => value;
            set => SetValue(value);
        }        
    }

    // *** uint ***

    internal class UintField : BaseTypedField<uint>
    {
        private uint value;

        private void SetValue(uint value)
        {
            Document?.History?.AddUintValueChangedEntry(this, this.value, ChangeSource.Usage);

            this.value = value;
        }

        private void Apply(uint oldValue, bool undo, OnUpdateSnapshotDelegate onUpdateSnapshot)
        {
            Document.History.AddUintValueChangedEntry(this, value, undo ? ChangeSource.Undo : ChangeSource.Redo);
            value = oldValue;
        }

        internal UintField(string name, BaseFieldItemContainer parent, uint defaultValue = default(uint))
            : base(name, parent)
        {
            value = defaultValue;
        }

        internal override void SnapshotInto(BaseField other, OnUpdateSnapshotDelegate onNewSnapshot)
        {
            var typedOther = (UintField)other;
            typedOther.value = value;
        }

        internal override void BinarySnapshotInto(BinaryWriter writer, OnUpdateSnapshotDelegate onCreateSnapshot)
        {
            writer.Write(value);
        }

        internal override void DeserializeSnapshot(BinaryReader reader, OnUpdateSnapshotDelegate onUpdateSnapshot)
        {
            value = reader.ReadUInt32();
        }

        internal override void Apply(BinaryReader reader, HistoryEntryType historyEntry, bool undo, OnUpdateSnapshotDelegate onUpdateSnapshot)        
        {
            if (historyEntry != HistoryEntryType.UintValueChange)
                throw new InvalidOperationException("Invalid history entry type!");

            var oldValue = reader.ReadUInt32();
            Apply(oldValue, undo, onUpdateSnapshot);
        }

        internal override void Apply(BaseHistoryEntry entry, bool undo, OnUpdateSnapshotDelegate onUpdateSnapshot)
        {
            if (entry is UintFieldValueChangedHistoryEntry valueEntry)
            {
                Apply(valueEntry.OldValue, undo, onUpdateSnapshot);
            }
            else
                throw new InvalidOperationException("Unsupported history entry!");
        }

        public override uint Value
        {
            get => value;
            set => SetValue(value);
        }        
    }

    // *** long ***

    internal class LongField : BaseTypedField<long>
    {
        private long value;

        private void SetValue(long value)
        {
            Document?.History?.AddLongValueChangedEntry(this, this.value, ChangeSource.Usage);

            this.value = value;
        }

        private void Apply(long oldValue, bool undo, OnUpdateSnapshotDelegate onUpdateSnapshot)
        {
            Document.History.AddLongValueChangedEntry(this, value, undo ? ChangeSource.Undo : ChangeSource.Redo);
            value = oldValue;
        }

        internal LongField(string name, BaseFieldItemContainer parent, long defaultValue = default(long))
            : base(name, parent)
        {
            value = defaultValue;
        }

        internal override void SnapshotInto(BaseField other, OnUpdateSnapshotDelegate onNewSnapshot)
        {
            var typedOther = (LongField)other;
            typedOther.value = value;
        }

        internal override void BinarySnapshotInto(BinaryWriter writer, OnUpdateSnapshotDelegate onCreateSnapshot)
        {
            writer.Write(value);
        }

        internal override void DeserializeSnapshot(BinaryReader reader, OnUpdateSnapshotDelegate onUpdateSnapshot)
        {
            value = reader.ReadInt64();
        }

        internal override void Apply(BinaryReader reader, HistoryEntryType historyEntry, bool undo, OnUpdateSnapshotDelegate onUpdateSnapshot)        
        {
            if (historyEntry != HistoryEntryType.LongValueChange)
                throw new InvalidOperationException("Invalid history entry type!");

            var oldValue = reader.ReadInt64();
            Apply(oldValue, undo, onUpdateSnapshot);
        }

        internal override void Apply(BaseHistoryEntry entry, bool undo, OnUpdateSnapshotDelegate onUpdateSnapshot)
        {
            if (entry is LongFieldValueChangedHistoryEntry valueEntry)
            {
                Apply(valueEntry.OldValue, undo, onUpdateSnapshot);
            }
            else
                throw new InvalidOperationException("Unsupported history entry!");
        }

        public override long Value
        {
            get => value;
            set => SetValue(value);
        }        
    }

    // *** ulong ***

    internal class UlongField : BaseTypedField<ulong>
    {
        private ulong value;

        private void SetValue(ulong value)
        {
            Document?.History?.AddUlongValueChangedEntry(this, this.value, ChangeSource.Usage);

            this.value = value;
        }

        private void Apply(ulong oldValue, bool undo, OnUpdateSnapshotDelegate onUpdateSnapshot)
        {
            Document.History.AddUlongValueChangedEntry(this, value, undo ? ChangeSource.Undo : ChangeSource.Redo);
            value = oldValue;
        }

        internal UlongField(string name, BaseFieldItemContainer parent, ulong defaultValue = default(ulong))
            : base(name, parent)
        {
            value = defaultValue;
        }

        internal override void SnapshotInto(BaseField other, OnUpdateSnapshotDelegate onNewSnapshot)
        {
            var typedOther = (UlongField)other;
            typedOther.value = value;
        }

        internal override void BinarySnapshotInto(BinaryWriter writer, OnUpdateSnapshotDelegate onCreateSnapshot)
        {
            writer.Write(value);
        }

        internal override void DeserializeSnapshot(BinaryReader reader, OnUpdateSnapshotDelegate onUpdateSnapshot)
        {
            value = reader.ReadUInt64();
        }

        internal override void Apply(BinaryReader reader, HistoryEntryType historyEntry, bool undo, OnUpdateSnapshotDelegate onUpdateSnapshot)        
        {
            if (historyEntry != HistoryEntryType.UlongValueChange)
                throw new InvalidOperationException("Invalid history entry type!");

            var oldValue = reader.ReadUInt64();
            Apply(oldValue, undo, onUpdateSnapshot);
        }

        internal override void Apply(BaseHistoryEntry entry, bool undo, OnUpdateSnapshotDelegate onUpdateSnapshot)
        {
            if (entry is UlongFieldValueChangedHistoryEntry valueEntry)
            {
                Apply(valueEntry.OldValue, undo, onUpdateSnapshot);
            }
            else
                throw new InvalidOperationException("Unsupported history entry!");
        }

        public override ulong Value
        {
            get => value;
            set => SetValue(value);
        }        
    }

    // *** float ***

    internal class FloatField : BaseTypedField<float>
    {
        private float value;

        private void SetValue(float value)
        {
            Document?.History?.AddFloatValueChangedEntry(this, this.value, ChangeSource.Usage);

            this.value = value;
        }

        private void Apply(float oldValue, bool undo, OnUpdateSnapshotDelegate onUpdateSnapshot)
        {
            Document.History.AddFloatValueChangedEntry(this, value, undo ? ChangeSource.Undo : ChangeSource.Redo);
            value = oldValue;
        }

        internal FloatField(string name, BaseFieldItemContainer parent, float defaultValue = default(float))
            : base(name, parent)
        {
            value = defaultValue;
        }

        internal override void SnapshotInto(BaseField other, OnUpdateSnapshotDelegate onNewSnapshot)
        {
            var typedOther = (FloatField)other;
            typedOther.value = value;
        }

        internal override void BinarySnapshotInto(BinaryWriter writer, OnUpdateSnapshotDelegate onCreateSnapshot)
        {
            writer.Write(value);
        }

        internal override void DeserializeSnapshot(BinaryReader reader, OnUpdateSnapshotDelegate onUpdateSnapshot)
        {
            value = reader.ReadSingle();
        }

        internal override void Apply(BinaryReader reader, HistoryEntryType historyEntry, bool undo, OnUpdateSnapshotDelegate onUpdateSnapshot)        
        {
            if (historyEntry != HistoryEntryType.FloatValueChange)
                throw new InvalidOperationException("Invalid history entry type!");

            var oldValue = reader.ReadSingle();
            Apply(oldValue, undo, onUpdateSnapshot);
        }

        internal override void Apply(BaseHistoryEntry entry, bool undo, OnUpdateSnapshotDelegate onUpdateSnapshot)
        {
            if (entry is FloatFieldValueChangedHistoryEntry valueEntry)
            {
                Apply(valueEntry.OldValue, undo, onUpdateSnapshot);
            }
            else
                throw new InvalidOperationException("Unsupported history entry!");
        }

        public override float Value
        {
            get => value;
            set => SetValue(value);
        }        
    }

    // *** double ***

    internal class DoubleField : BaseTypedField<double>
    {
        private double value;

        private void SetValue(double value)
        {
            Document?.History?.AddDoubleValueChangedEntry(this, this.value, ChangeSource.Usage);

            this.value = value;
        }

        private void Apply(double oldValue, bool undo, OnUpdateSnapshotDelegate onUpdateSnapshot)
        {
            Document.History.AddDoubleValueChangedEntry(this, value, undo ? ChangeSource.Undo : ChangeSource.Redo);
            value = oldValue;
        }

        internal DoubleField(string name, BaseFieldItemContainer parent, double defaultValue = default(double))
            : base(name, parent)
        {
            value = defaultValue;
        }

        internal override void SnapshotInto(BaseField other, OnUpdateSnapshotDelegate onNewSnapshot)
        {
            var typedOther = (DoubleField)other;
            typedOther.value = value;
        }

        internal override void BinarySnapshotInto(BinaryWriter writer, OnUpdateSnapshotDelegate onCreateSnapshot)
        {
            writer.Write(value);
        }

        internal override void DeserializeSnapshot(BinaryReader reader, OnUpdateSnapshotDelegate onUpdateSnapshot)
        {
            value = reader.ReadDouble();
        }

        internal override void Apply(BinaryReader reader, HistoryEntryType historyEntry, bool undo, OnUpdateSnapshotDelegate onUpdateSnapshot)        
        {
            if (historyEntry != HistoryEntryType.DoubleValueChange)
                throw new InvalidOperationException("Invalid history entry type!");

            var oldValue = reader.ReadDouble();
            Apply(oldValue, undo, onUpdateSnapshot);
        }

        internal override void Apply(BaseHistoryEntry entry, bool undo, OnUpdateSnapshotDelegate onUpdateSnapshot)
        {
            if (entry is DoubleFieldValueChangedHistoryEntry valueEntry)
            {
                Apply(valueEntry.OldValue, undo, onUpdateSnapshot);
            }
            else
                throw new InvalidOperationException("Unsupported history entry!");
        }

        public override double Value
        {
            get => value;
            set => SetValue(value);
        }        
    }

    // *** decimal ***

    internal class DecimalField : BaseTypedField<decimal>
    {
        private decimal value;

        private void SetValue(decimal value)
        {
            Document?.History?.AddDecimalValueChangedEntry(this, this.value, ChangeSource.Usage);

            this.value = value;
        }

        private void Apply(decimal oldValue, bool undo, OnUpdateSnapshotDelegate onUpdateSnapshot)
        {
            Document.History.AddDecimalValueChangedEntry(this, value, undo ? ChangeSource.Undo : ChangeSource.Redo);
            value = oldValue;
        }

        internal DecimalField(string name, BaseFieldItemContainer parent, decimal defaultValue = default(decimal))
            : base(name, parent)
        {
            value = defaultValue;
        }

        internal override void SnapshotInto(BaseField other, OnUpdateSnapshotDelegate onNewSnapshot)
        {
            var typedOther = (DecimalField)other;
            typedOther.value = value;
        }

        internal override void BinarySnapshotInto(BinaryWriter writer, OnUpdateSnapshotDelegate onCreateSnapshot)
        {
            writer.Write(value);
        }

        internal override void DeserializeSnapshot(BinaryReader reader, OnUpdateSnapshotDelegate onUpdateSnapshot)
        {
            value = reader.ReadDecimal();
        }

        internal override void Apply(BinaryReader reader, HistoryEntryType historyEntry, bool undo, OnUpdateSnapshotDelegate onUpdateSnapshot)        
        {
            if (historyEntry != HistoryEntryType.DecimalValueChange)
                throw new InvalidOperationException("Invalid history entry type!");

            var oldValue = reader.ReadDecimal();
            Apply(oldValue, undo, onUpdateSnapshot);
        }

        internal override void Apply(BaseHistoryEntry entry, bool undo, OnUpdateSnapshotDelegate onUpdateSnapshot)
        {
            if (entry is DecimalFieldValueChangedHistoryEntry valueEntry)
            {
                Apply(valueEntry.OldValue, undo, onUpdateSnapshot);
            }
            else
                throw new InvalidOperationException("Unsupported history entry!");
        }

        public override decimal Value
        {
            get => value;
            set => SetValue(value);
        }        
    }

    // *** bool ***

    internal class BoolField : BaseTypedField<bool>
    {
        private bool value;

        private void SetValue(bool value)
        {
            Document?.History?.AddBoolValueChangedEntry(this, this.value, ChangeSource.Usage);

            this.value = value;
        }

        private void Apply(bool oldValue, bool undo, OnUpdateSnapshotDelegate onUpdateSnapshot)
        {
            Document.History.AddBoolValueChangedEntry(this, value, undo ? ChangeSource.Undo : ChangeSource.Redo);
            value = oldValue;
        }

        internal BoolField(string name, BaseFieldItemContainer parent, bool defaultValue = default(bool))
            : base(name, parent)
        {
            value = defaultValue;
        }

        internal override void SnapshotInto(BaseField other, OnUpdateSnapshotDelegate onNewSnapshot)
        {
            var typedOther = (BoolField)other;
            typedOther.value = value;
        }

        internal override void BinarySnapshotInto(BinaryWriter writer, OnUpdateSnapshotDelegate onCreateSnapshot)
        {
            writer.Write(value);
        }

        internal override void DeserializeSnapshot(BinaryReader reader, OnUpdateSnapshotDelegate onUpdateSnapshot)
        {
            value = reader.ReadBoolean();
        }

        internal override void Apply(BinaryReader reader, HistoryEntryType historyEntry, bool undo, OnUpdateSnapshotDelegate onUpdateSnapshot)        
        {
            if (historyEntry != HistoryEntryType.BoolValueChange)
                throw new InvalidOperationException("Invalid history entry type!");

            var oldValue = reader.ReadBoolean();
            Apply(oldValue, undo, onUpdateSnapshot);
        }

        internal override void Apply(BaseHistoryEntry entry, bool undo, OnUpdateSnapshotDelegate onUpdateSnapshot)
        {
            if (entry is BoolFieldValueChangedHistoryEntry valueEntry)
            {
                Apply(valueEntry.OldValue, undo, onUpdateSnapshot);
            }
            else
                throw new InvalidOperationException("Unsupported history entry!");
        }

        public override bool Value
        {
            get => value;
            set => SetValue(value);
        }        
    }

    // *** char ***

    internal class CharField : BaseTypedField<char>
    {
        private char value;

        private void SetValue(char value)
        {
            Document?.History?.AddCharValueChangedEntry(this, this.value, ChangeSource.Usage);

            this.value = value;
        }

        private void Apply(char oldValue, bool undo, OnUpdateSnapshotDelegate onUpdateSnapshot)
        {
            Document.History.AddCharValueChangedEntry(this, value, undo ? ChangeSource.Undo : ChangeSource.Redo);
            value = oldValue;
        }

        internal CharField(string name, BaseFieldItemContainer parent, char defaultValue = default(char))
            : base(name, parent)
        {
            value = defaultValue;
        }

        internal override void SnapshotInto(BaseField other, OnUpdateSnapshotDelegate onNewSnapshot)
        {
            var typedOther = (CharField)other;
            typedOther.value = value;
        }

        internal override void BinarySnapshotInto(BinaryWriter writer, OnUpdateSnapshotDelegate onCreateSnapshot)
        {
            writer.Write(value);
        }

        internal override void DeserializeSnapshot(BinaryReader reader, OnUpdateSnapshotDelegate onUpdateSnapshot)
        {
            value = reader.ReadChar();
        }

        internal override void Apply(BinaryReader reader, HistoryEntryType historyEntry, bool undo, OnUpdateSnapshotDelegate onUpdateSnapshot)        
        {
            if (historyEntry != HistoryEntryType.CharValueChange)
                throw new InvalidOperationException("Invalid history entry type!");

            var oldValue = reader.ReadChar();
            Apply(oldValue, undo, onUpdateSnapshot);
        }

        internal override void Apply(BaseHistoryEntry entry, bool undo, OnUpdateSnapshotDelegate onUpdateSnapshot)
        {
            if (entry is CharFieldValueChangedHistoryEntry valueEntry)
            {
                Apply(valueEntry.OldValue, undo, onUpdateSnapshot);
            }
            else
                throw new InvalidOperationException("Unsupported history entry!");
        }

        public override char Value
        {
            get => value;
            set => SetValue(value);
        }        
    }

    // *** string ***

    internal class StringField : BaseTypedField<string>
    {
        private string value;

        private void SetValue(string value)
        {
            Document?.History?.AddStringValueChangedEntry(this, this.value, ChangeSource.Usage);

            this.value = value;
        }

        private void Apply(string oldValue, bool undo, OnUpdateSnapshotDelegate onUpdateSnapshot)
        {
            Document.History.AddStringValueChangedEntry(this, value, undo ? ChangeSource.Undo : ChangeSource.Redo);
            value = oldValue;
        }

        internal StringField(string name, BaseFieldItemContainer parent, string defaultValue = default(string))
            : base(name, parent)
        {
            value = defaultValue;
        }

        internal override void SnapshotInto(BaseField other, OnUpdateSnapshotDelegate onNewSnapshot)
        {
            var typedOther = (StringField)other;
            typedOther.value = value;
        }

        internal override void BinarySnapshotInto(BinaryWriter writer, OnUpdateSnapshotDelegate onCreateSnapshot)
        {
            writer.Write(value);
        }

        internal override void DeserializeSnapshot(BinaryReader reader, OnUpdateSnapshotDelegate onUpdateSnapshot)
        {
            value = reader.ReadString();
        }

        internal override void Apply(BinaryReader reader, HistoryEntryType historyEntry, bool undo, OnUpdateSnapshotDelegate onUpdateSnapshot)        
        {
            if (historyEntry != HistoryEntryType.StringValueChange)
                throw new InvalidOperationException("Invalid history entry type!");

            var oldValue = reader.ReadString();
            Apply(oldValue, undo, onUpdateSnapshot);
        }

        internal override void Apply(BaseHistoryEntry entry, bool undo, OnUpdateSnapshotDelegate onUpdateSnapshot)
        {
            if (entry is StringFieldValueChangedHistoryEntry valueEntry)
            {
                Apply(valueEntry.OldValue, undo, onUpdateSnapshot);
            }
            else
                throw new InvalidOperationException("Unsupported history entry!");
        }

        public override string Value
        {
            get => value;
            set => SetValue(value);
        }        
    }
}
