﻿using SmartModel.Infrastructure.Types;
using System;
using System.Collections.Generic;
using System.IO;

namespace SmartModel.Infrastructure.Utils
{
	internal static class BinaryCollections
	{
		private static readonly Dictionary<Type, Action<object, BinaryWriter>> ItemWriters = new Dictionary<Type, Action<object, BinaryWriter>> 
		{
			{ typeof(sbyte), (item, writer) => writer.Write((sbyte)item) },
			{ typeof(byte), (item, writer) => writer.Write((byte)item) },
			{ typeof(short), (item, writer) => writer.Write((short)item) },
			{ typeof(ushort), (item, writer) => writer.Write((ushort)item) },
			{ typeof(int), (item, writer) => writer.Write((int)item) },
			{ typeof(uint), (item, writer) => writer.Write((uint)item) },
			{ typeof(long), (item, writer) => writer.Write((long)item) },
			{ typeof(ulong), (item, writer) => writer.Write((ulong)item) },
			{ typeof(float), (item, writer) => writer.Write((float)item) },
			{ typeof(double), (item, writer) => writer.Write((double)item) },
			{ typeof(decimal), (item, writer) => writer.Write((decimal)item) },
			{ typeof(bool), (item, writer) => writer.Write((bool)item) },
			{ typeof(char), (item, writer) => writer.Write((char)item) },
			{ typeof(string), (item, writer) => writer.Write((string)item) },
		};

		private static void WriteSbyteCollection(IReadOnlyList<sbyte> collection, BinaryWriter writer)
		{
			writer.Write((byte)CollectionType.SbyteType);
			writer.Write(collection.Count);
			for (int i = 0; i < collection.Count; i++)
				writer.Write(collection[i]);
		}

		private static List<sbyte> ReadSbyteCollection(BinaryReader reader)
		{
			CollectionType type = (CollectionType)reader.ReadByte();
			if (type != CollectionType.SbyteType)
				throw new InvalidOperationException("Trying to deserialize collection of invalid type!");

			int count = reader.ReadInt32();

			List<sbyte> result = new List<sbyte>();

			for (int i = 0; i < count; i++)
				result.Add(reader.ReadSByte());

			return result;
		}

		private static void WriteByteCollection(IReadOnlyList<byte> collection, BinaryWriter writer)
		{
			writer.Write((byte)CollectionType.ByteType);
			writer.Write(collection.Count);
			for (int i = 0; i < collection.Count; i++)
				writer.Write(collection[i]);
		}

		private static List<byte> ReadByteCollection(BinaryReader reader)
		{
			CollectionType type = (CollectionType)reader.ReadByte();
			if (type != CollectionType.ByteType)
				throw new InvalidOperationException("Trying to deserialize collection of invalid type!");

			int count = reader.ReadInt32();

			List<byte> result = new List<byte>();

			for (int i = 0; i < count; i++)
				result.Add(reader.ReadByte());

			return result;
		}

		private static void WriteShortCollection(IReadOnlyList<short> collection, BinaryWriter writer)
		{
			writer.Write((byte)CollectionType.ShortType);
			writer.Write(collection.Count);
			for (int i = 0; i < collection.Count; i++)
				writer.Write(collection[i]);
		}

		private static List<short> ReadShortCollection(BinaryReader reader)
		{
			CollectionType type = (CollectionType)reader.ReadByte();
			if (type != CollectionType.ShortType)
				throw new InvalidOperationException("Trying to deserialize collection of invalid type!");

			int count = reader.ReadInt32();

			List<short> result = new List<short>();

			for (int i = 0; i < count; i++)
				result.Add(reader.ReadInt16());

			return result;
		}

		private static void WriteUshortCollection(IReadOnlyList<ushort> collection, BinaryWriter writer)
		{
			writer.Write((byte)CollectionType.UshortType);
			writer.Write(collection.Count);
			for (int i = 0; i < collection.Count; i++)
				writer.Write(collection[i]);
		}

		private static List<ushort> ReadUshortCollection(BinaryReader reader)
		{
			CollectionType type = (CollectionType)reader.ReadByte();
			if (type != CollectionType.UshortType)
				throw new InvalidOperationException("Trying to deserialize collection of invalid type!");

			int count = reader.ReadInt32();

			List<ushort> result = new List<ushort>();

			for (int i = 0; i < count; i++)
				result.Add(reader.ReadUInt16());

			return result;
		}

		private static void WriteIntCollection(IReadOnlyList<int> collection, BinaryWriter writer)
		{
			writer.Write((byte)CollectionType.IntType);
			writer.Write(collection.Count);
			for (int i = 0; i < collection.Count; i++)
				writer.Write(collection[i]);
		}

		private static List<int> ReadIntCollection(BinaryReader reader)
		{
			CollectionType type = (CollectionType)reader.ReadByte();
			if (type != CollectionType.IntType)
				throw new InvalidOperationException("Trying to deserialize collection of invalid type!");

			int count = reader.ReadInt32();

			List<int> result = new List<int>();

			for (int i = 0; i < count; i++)
				result.Add(reader.ReadInt32());

			return result;
		}

		private static void WriteUintCollection(IReadOnlyList<uint> collection, BinaryWriter writer)
		{
			writer.Write((byte)CollectionType.UintType);
			writer.Write(collection.Count);
			for (int i = 0; i < collection.Count; i++)
				writer.Write(collection[i]);
		}

		private static List<uint> ReadUintCollection(BinaryReader reader)
		{
			CollectionType type = (CollectionType)reader.ReadByte();
			if (type != CollectionType.UintType)
				throw new InvalidOperationException("Trying to deserialize collection of invalid type!");

			int count = reader.ReadInt32();

			List<uint> result = new List<uint>();

			for (int i = 0; i < count; i++)
				result.Add(reader.ReadUInt32());

			return result;
		}

		private static void WriteLongCollection(IReadOnlyList<long> collection, BinaryWriter writer)
		{
			writer.Write((byte)CollectionType.LongType);
			writer.Write(collection.Count);
			for (int i = 0; i < collection.Count; i++)
				writer.Write(collection[i]);
		}

		private static List<long> ReadLongCollection(BinaryReader reader)
		{
			CollectionType type = (CollectionType)reader.ReadByte();
			if (type != CollectionType.LongType)
				throw new InvalidOperationException("Trying to deserialize collection of invalid type!");

			int count = reader.ReadInt32();

			List<long> result = new List<long>();

			for (int i = 0; i < count; i++)
				result.Add(reader.ReadInt64());

			return result;
		}

		private static void WriteUlongCollection(IReadOnlyList<ulong> collection, BinaryWriter writer)
		{
			writer.Write((byte)CollectionType.UlongType);
			writer.Write(collection.Count);
			for (int i = 0; i < collection.Count; i++)
				writer.Write(collection[i]);
		}

		private static List<ulong> ReadUlongCollection(BinaryReader reader)
		{
			CollectionType type = (CollectionType)reader.ReadByte();
			if (type != CollectionType.UlongType)
				throw new InvalidOperationException("Trying to deserialize collection of invalid type!");

			int count = reader.ReadInt32();

			List<ulong> result = new List<ulong>();

			for (int i = 0; i < count; i++)
				result.Add(reader.ReadUInt64());

			return result;
		}

		private static void WriteFloatCollection(IReadOnlyList<float> collection, BinaryWriter writer)
		{
			writer.Write((byte)CollectionType.FloatType);
			writer.Write(collection.Count);
			for (int i = 0; i < collection.Count; i++)
				writer.Write(collection[i]);
		}

		private static List<float> ReadFloatCollection(BinaryReader reader)
		{
			CollectionType type = (CollectionType)reader.ReadByte();
			if (type != CollectionType.FloatType)
				throw new InvalidOperationException("Trying to deserialize collection of invalid type!");

			int count = reader.ReadInt32();

			List<float> result = new List<float>();

			for (int i = 0; i < count; i++)
				result.Add(reader.ReadSingle());

			return result;
		}

		private static void WriteDoubleCollection(IReadOnlyList<double> collection, BinaryWriter writer)
		{
			writer.Write((byte)CollectionType.DoubleType);
			writer.Write(collection.Count);
			for (int i = 0; i < collection.Count; i++)
				writer.Write(collection[i]);
		}

		private static List<double> ReadDoubleCollection(BinaryReader reader)
		{
			CollectionType type = (CollectionType)reader.ReadByte();
			if (type != CollectionType.DoubleType)
				throw new InvalidOperationException("Trying to deserialize collection of invalid type!");

			int count = reader.ReadInt32();

			List<double> result = new List<double>();

			for (int i = 0; i < count; i++)
				result.Add(reader.ReadDouble());

			return result;
		}

		private static void WriteDecimalCollection(IReadOnlyList<decimal> collection, BinaryWriter writer)
		{
			writer.Write((byte)CollectionType.DecimalType);
			writer.Write(collection.Count);
			for (int i = 0; i < collection.Count; i++)
				writer.Write(collection[i]);
		}

		private static List<decimal> ReadDecimalCollection(BinaryReader reader)
		{
			CollectionType type = (CollectionType)reader.ReadByte();
			if (type != CollectionType.DecimalType)
				throw new InvalidOperationException("Trying to deserialize collection of invalid type!");

			int count = reader.ReadInt32();

			List<decimal> result = new List<decimal>();

			for (int i = 0; i < count; i++)
				result.Add(reader.ReadDecimal());

			return result;
		}

		private static void WriteBoolCollection(IReadOnlyList<bool> collection, BinaryWriter writer)
		{
			writer.Write((byte)CollectionType.BoolType);
			writer.Write(collection.Count);
			for (int i = 0; i < collection.Count; i++)
				writer.Write(collection[i]);
		}

		private static List<bool> ReadBoolCollection(BinaryReader reader)
		{
			CollectionType type = (CollectionType)reader.ReadByte();
			if (type != CollectionType.BoolType)
				throw new InvalidOperationException("Trying to deserialize collection of invalid type!");

			int count = reader.ReadInt32();

			List<bool> result = new List<bool>();

			for (int i = 0; i < count; i++)
				result.Add(reader.ReadBoolean());

			return result;
		}

		private static void WriteCharCollection(IReadOnlyList<char> collection, BinaryWriter writer)
		{
			writer.Write((byte)CollectionType.CharType);
			writer.Write(collection.Count);
			for (int i = 0; i < collection.Count; i++)
				writer.Write(collection[i]);
		}

		private static List<char> ReadCharCollection(BinaryReader reader)
		{
			CollectionType type = (CollectionType)reader.ReadByte();
			if (type != CollectionType.CharType)
				throw new InvalidOperationException("Trying to deserialize collection of invalid type!");

			int count = reader.ReadInt32();

			List<char> result = new List<char>();

			for (int i = 0; i < count; i++)
				result.Add(reader.ReadChar());

			return result;
		}

		private static void WriteStringCollection(IReadOnlyList<string> collection, BinaryWriter writer)
		{
			writer.Write((byte)CollectionType.StringType);
			writer.Write(collection.Count);
			for (int i = 0; i < collection.Count; i++)
				writer.Write(collection[i]);
		}

		private static List<string> ReadStringCollection(BinaryReader reader)
		{
			CollectionType type = (CollectionType)reader.ReadByte();
			if (type != CollectionType.StringType)
				throw new InvalidOperationException("Trying to deserialize collection of invalid type!");

			int count = reader.ReadInt32();

			List<string> result = new List<string>();

			for (int i = 0; i < count; i++)
				result.Add(reader.ReadString());

			return result;
		}


		private static void WriteRefItem(BaseFieldItemContainer item, BinaryWriter writer, OnUpdateSnapshotDelegate onNewSnapshot)
		{
			writer.Write(item == null ? (byte)0 : (byte)1);
			if (item != null)
			{
				item.CreateBinarySnapshot(writer, onNewSnapshot);	
			}
		}

		public static void WriteRefCollection(IReadOnlyList<BaseCollectionItem> collection, BinaryWriter writer, OnUpdateSnapshotDelegate onNewSnapshot)
		{
			writer.Write((byte)CollectionType.RefType);
			writer.Write(collection.Count);
			for (int i = 0; i < collection.Count; i++)
			{
				writer.Write(collection[i] == null ? (byte)0 : (byte)1);
				if (collection[i] != null)
					collection[i].CreateBinarySnapshot(writer, onNewSnapshot);
			}
		}

		public static List<TRef> ReadRefCollection<TRef>(BinaryReader reader, OnUpdateSnapshotDelegate onUpdateSnapshot)
			where TRef : BaseCollectionItem
		{
			CollectionType type = (CollectionType)reader.ReadByte();
			if (type != CollectionType.RefType)
				throw new InvalidOperationException("Trying to deserialize collection of wrong type!");

			var result = new List<TRef>();

			int count = reader.ReadInt32();
			for (int i = 0; i < count; i++)
			{
				byte isNull = reader.ReadByte();
				if (isNull == 0)
					result.Add(null);
				else
				{
					TRef @ref = BaseFieldItemContainer.DeserializeSnapshot(reader, onUpdateSnapshot) as TRef;
					result.Add(@ref);
				}
			}

			return result;
		}

		/// <remarks>Supports both value-typed collections and ref-collections</remarks>
		public static void WriteCollection<TItem>(IReadOnlyList<TItem> collection, BinaryWriter writer, OnUpdateSnapshotDelegate onNewSnapshot)
		{
			// TODO: Speed up using dictionary			

			if (typeof(BaseCollectionItem).IsAssignableFrom(typeof(TItem)))
			{
				WriteRefCollection((IReadOnlyList<BaseCollectionItem>)collection, writer, onNewSnapshot);
			}
			else if (typeof(TItem) == typeof(sbyte))
			{
				WriteSbyteCollection((IReadOnlyList<sbyte>)collection, writer);
			}
			else if (typeof(TItem) == typeof(byte))
			{
				WriteByteCollection((IReadOnlyList<byte>)collection, writer);
			}
			else if (typeof(TItem) == typeof(short))
			{
				WriteShortCollection((IReadOnlyList<short>)collection, writer);
			}
			else if (typeof(TItem) == typeof(ushort))
			{
				WriteUshortCollection((IReadOnlyList<ushort>)collection, writer);
			}
			else if (typeof(TItem) == typeof(int))
			{
				WriteIntCollection((IReadOnlyList<int>)collection, writer);
			}
			else if (typeof(TItem) == typeof(uint))
			{
				WriteUintCollection((IReadOnlyList<uint>)collection, writer);
			}
			else if (typeof(TItem) == typeof(long))
			{
				WriteLongCollection((IReadOnlyList<long>)collection, writer);
			}
			else if (typeof(TItem) == typeof(ulong))
			{
				WriteUlongCollection((IReadOnlyList<ulong>)collection, writer);
			}
			else if (typeof(TItem) == typeof(float))
			{
				WriteFloatCollection((IReadOnlyList<float>)collection, writer);
			}
			else if (typeof(TItem) == typeof(double))
			{
				WriteDoubleCollection((IReadOnlyList<double>)collection, writer);
			}
			else if (typeof(TItem) == typeof(decimal))
			{
				WriteDecimalCollection((IReadOnlyList<decimal>)collection, writer);
			}
			else if (typeof(TItem) == typeof(bool))
			{
				WriteBoolCollection((IReadOnlyList<bool>)collection, writer);
			}
			else if (typeof(TItem) == typeof(char))
			{
				WriteCharCollection((IReadOnlyList<char>)collection, writer);
			}
			else if (typeof(TItem) == typeof(string))
			{
				WriteStringCollection((IReadOnlyList<string>)collection, writer);
			}
			else
				throw new ArgumentException("Unsupported collection type", nameof(collection));
		}

		public static List<TItem> ReadCollection<TItem>(BinaryReader reader, OnUpdateSnapshotDelegate onUpdateSnapshot)
		{
			// TODO: Speed up using dictionary

			if (typeof(TItem) == typeof(sbyte))
			{
				return ReadSbyteCollection(reader) as List<TItem>;
			}
			else if (typeof(TItem) == typeof(byte))
			{
				return ReadByteCollection(reader) as List<TItem>;
			}
			else if (typeof(TItem) == typeof(short))
			{
				return ReadShortCollection(reader) as List<TItem>;
			}
			else if (typeof(TItem) == typeof(ushort))
			{
				return ReadUshortCollection(reader) as List<TItem>;
			}
			else if (typeof(TItem) == typeof(int))
			{
				return ReadIntCollection(reader) as List<TItem>;
			}
			else if (typeof(TItem) == typeof(uint))
			{
				return ReadUintCollection(reader) as List<TItem>;
			}
			else if (typeof(TItem) == typeof(long))
			{
				return ReadLongCollection(reader) as List<TItem>;
			}
			else if (typeof(TItem) == typeof(ulong))
			{
				return ReadUlongCollection(reader) as List<TItem>;
			}
			else if (typeof(TItem) == typeof(float))
			{
				return ReadFloatCollection(reader) as List<TItem>;
			}
			else if (typeof(TItem) == typeof(double))
			{
				return ReadDoubleCollection(reader) as List<TItem>;
			}
			else if (typeof(TItem) == typeof(decimal))
			{
				return ReadDecimalCollection(reader) as List<TItem>;
			}
			else if (typeof(TItem) == typeof(bool))
			{
				return ReadBoolCollection(reader) as List<TItem>;
			}
			else if (typeof(TItem) == typeof(char))
			{
				return ReadCharCollection(reader) as List<TItem>;
			}
			else if (typeof(TItem) == typeof(string))
			{
				return ReadStringCollection(reader) as List<TItem>;
			}
			else
				throw new ArgumentException("Unsupported collection type");

		}

		public static void WriteItem(object item, BinaryWriter writer, OnUpdateSnapshotDelegate onNewSnapshot)
		{
			// TODO: Speed up using dictionary

			if (item is BaseFieldItemContainer containerItem)
			{
				WriteRefItem(containerItem, writer, onNewSnapshot);
			}
			else
			{
				ItemWriters[item.GetType()](item, writer);
			}
		}

		public static object ReadItem<TItem>(BinaryReader reader, OnUpdateSnapshotDelegate onUpdateSnapshot)
		{
			if (typeof(BaseFieldItemContainer).IsAssignableFrom(typeof(TItem)))
			{
				byte isNull = reader.ReadByte();
				if (isNull == 0)
					return 0;

				BaseFieldItemContainer item = BaseFieldItemContainer.DeserializeSnapshot(reader, onUpdateSnapshot);
				return item;
			}
			else if (typeof(TItem) == typeof(sbyte))
			{
				return reader.ReadSByte();
			}
			else if (typeof(TItem) == typeof(byte))
			{
				return reader.ReadByte();
			}
			else if (typeof(TItem) == typeof(short))
			{
				return reader.ReadInt16();
			}
			else if (typeof(TItem) == typeof(ushort))
			{
				return reader.ReadUInt16();
			}
			else if (typeof(TItem) == typeof(int))
			{
				return reader.ReadInt32();
			}
			else if (typeof(TItem) == typeof(uint))
			{
				return reader.ReadUInt32();
			}
			else if (typeof(TItem) == typeof(long))
			{
				return reader.ReadInt64();
			}
			else if (typeof(TItem) == typeof(ulong))
			{
				return reader.ReadUInt64();
			}
			else if (typeof(TItem) == typeof(float))
			{
				return reader.ReadSingle();
			}
			else if (typeof(TItem) == typeof(double))
			{
				return reader.ReadDouble();
			}
			else if (typeof(TItem) == typeof(decimal))
			{
				return reader.ReadDecimal();
			}
			else if (typeof(TItem) == typeof(bool))
			{
				return reader.ReadBoolean();
			}
			else if (typeof(TItem) == typeof(char))
			{
				return reader.ReadChar();
			}
			else if (typeof(TItem) == typeof(string))
			{
				return reader.ReadString();
			}
			else
				throw new ArgumentException("Unsupported item type");
		}
	}
}