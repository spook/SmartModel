﻿using SmartModel.Infrastructure.HistoryEntries;
using SmartModel.Infrastructure.Utils;
using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;

namespace SmartModel.Infrastructure
{
    public class RefCollection<TItem> : BaseCollection, IList<TItem>, IReadOnlyList<TItem>, ICollection<TItem>, ICollection, IEnumerable<TItem>, IEnumerable
        where TItem : BaseCollectionItem, new()
    {
        // Private fields -----------------------------------------------------

        private readonly List<TItem> items;

        private void SetItem(int index, TItem value)
        {
            Document?.History?.AddRefCollectionItemReplacedEntry(this, index, items[index], ChangeSource.Usage);

            var current = items[index];
            current?.DetachFromCollection(this);

            value?.AttachToCollection(this);
            items[index] = value;
        }

        // Internal fields ----------------------------------------------------

        internal RefCollection(BaseFieldItemContainer parent, string name)
            : base(parent, name)
        {
            items = new List<TItem>();
        }

        internal override void ResetDocumentRecursive()
        {
            base.ResetDocumentRecursive();

            foreach (var item in items)
            {
                item.ResetDocumentRecursive();
            }
        }

        internal override void SnapshotInto(BaseCollection collection, OnUpdateSnapshotDelegate onNewSnapshot)
        {
            var typedCollection = (RefCollection<TItem>)collection;
            typedCollection.Clear();

            foreach (var item in items)
                typedCollection.Add((TItem)item?.CreateSnapshot(onNewSnapshot));
        }

        internal void ApplyCollectionCleared(bool undo, IList<TItem> items, OnUpdateSnapshotDelegate onUpdateSnapshot)
        {
            Document.History.AddRefCollectionRestoredHistoryEntry(this, undo ? ChangeSource.Undo : ChangeSource.Redo);

            this.items.Clear();
            foreach (var item in items)
            {
                item?.AttachToCollection(this);
                this.items.Add(item);

                onUpdateSnapshot(item.InstanceId, item);
            }
        }

        internal void ApplyCollectionRestored(bool undo, OnUpdateSnapshotDelegate onUpdateSnapshot)
        {
            Document?.History?.AddRefCollectionClearedEntry(this, items.ToList(), undo ? ChangeSource.Undo : ChangeSource.Redo);

            foreach (var item in items)
                item?.DetachFromCollection(this);

            items.Clear();
        }

        internal void ApplyCollectionItemReplaced(bool undo, int index, TItem oldItem, OnUpdateSnapshotDelegate onUpdateSnapshot)
        {
            Document.History.AddRefCollectionItemReplacedEntry(this, index, items[index], undo ? ChangeSource.Undo : ChangeSource.Redo);

            var current = items[index];
            current.DetachFromCollection(this);

            oldItem?.AttachToCollection(this);
            items[index] = oldItem;

            if (oldItem != null)
                onUpdateSnapshot(oldItem.InstanceId, oldItem);
        }

        internal void ApplyCollectionItemRemoved(bool undo, int index, TItem oldItem, OnUpdateSnapshotDelegate onUpdateSnapshot)
        {
            Document.History.AddRefCollectionItemInsertedEntry(this, index, undo ? ChangeSource.Undo : ChangeSource.Redo);

            oldItem.AttachToCollection(this);
            items.Insert(index, oldItem);

            onUpdateSnapshot(oldItem.InstanceId, oldItem);
        }

        internal void ApplyCollectionItemInserted(bool undo, int index, OnUpdateSnapshotDelegate onUpdateSnapshot)
        {
            Document.History.AddRefCollectionItemRemovedEntry(this, index, items[index], undo ? ChangeSource.Undo : ChangeSource.Redo);

            var item = items[index];
            item?.DetachFromCollection(this);

            items.RemoveAt(index);
        }

        internal void ApplyCollectionItemAdded(bool undo, OnUpdateSnapshotDelegate onUpdateSnapshot)
        {
            Document.History.AddRefCollectionItemRemovedEntry(this, items.Count - 1, items[items.Count - 1], undo ? ChangeSource.Undo : ChangeSource.Redo);

            var item = items[items.Count - 1];
            item?.DetachFromCollection(this);

            items.RemoveAt(items.Count - 1);
        }

        internal override void Apply(BaseHistoryEntry entry, bool undo, OnUpdateSnapshotDelegate onUpdateSnapshot)
        {
            switch (entry)
            {
                case CollectionClearedHistoryEntry<TItem> collectionCleared:
                    {
                        ApplyCollectionCleared(undo, collectionCleared.Snapshot, onUpdateSnapshot);
                        break;
                    }
                case CollectionItemAddedHistoryEntry itemAdded:
                    {
                        ApplyCollectionItemAdded(undo, onUpdateSnapshot);
                        break;
                    }
                case CollectionItemInsertedHistoryEntry itemInserted:
                    {
                        ApplyCollectionItemInserted(undo, itemInserted.Index, onUpdateSnapshot);
                        break;
                    }
                case CollectionItemRemovedHistoryEntry<TItem> itemRemoved:
                    {
                        ApplyCollectionItemRemoved(undo, itemRemoved.Index, itemRemoved.OldItem, onUpdateSnapshot);
                        break;
                    }
                case CollectionItemReplacedHistoryEntry<TItem> itemReplaced:
                    {
                        ApplyCollectionItemReplaced(undo, itemReplaced.Index, itemReplaced.OldItem, onUpdateSnapshot);
                        break;
                    }
                case CollectionRestoredHistoryEntry restored:
                    {
                        ApplyCollectionRestored(undo, onUpdateSnapshot);
                        break;
                    }
                default:
                    throw new InvalidOperationException("Unsupported history entry!");
            }
        }

        internal override void Apply(BinaryReader reader, HistoryEntryType entryType, bool undo, OnUpdateSnapshotDelegate onUpdateSnapshot)
        {
            switch (entryType)
            {
                case HistoryEntryType.CollectionClearedChange:
                    {
                        List<TItem> oldCollection = BinaryCollections.ReadRefCollection<TItem>(reader, onUpdateSnapshot);
                        ApplyCollectionCleared(undo, oldCollection, onUpdateSnapshot);

                        break;
                    }
                case HistoryEntryType.CollectionItemAddedChange:
                    {
                        ApplyCollectionItemAdded(undo, onUpdateSnapshot);

                        break;
                    }
                case HistoryEntryType.CollectionItemInsertedChange:
                    {
                        int index = reader.ReadInt32();
                        ApplyCollectionItemInserted(undo, index, onUpdateSnapshot);

                        break;
                    }
                case HistoryEntryType.CollectionItemRemovedChange:
                    {
                        int index = reader.ReadInt32();
                        TItem item = (TItem)BinaryCollections.ReadItem<TItem>(reader, onUpdateSnapshot);

                        ApplyCollectionItemRemoved(undo, index, item, onUpdateSnapshot);

                        break;
                    }
                case HistoryEntryType.CollectionItemReplacedChange:
                    {
                        int index = reader.ReadInt32();
                        TItem item = (TItem)BinaryCollections.ReadItem<TItem>(reader, onUpdateSnapshot);

                        ApplyCollectionItemReplaced(undo, index, item, onUpdateSnapshot);

                        break;
                    }
                case HistoryEntryType.CollectionRestoredChange:
                    {
                        ApplyCollectionRestored(undo, onUpdateSnapshot);

                        break;
                    }
            }
        }

        internal override void BinarySnapshotInto(BinaryWriter writer, OnUpdateSnapshotDelegate onNewSnapshot)
        {
            BinaryCollections.WriteRefCollection(this, writer, onNewSnapshot);
        }

        internal override void DeserializeSnapshot(BinaryReader reader, OnUpdateSnapshotDelegate onUpdateSnapshot)
        {
            var items = BinaryCollections.ReadRefCollection<TItem>(reader, onUpdateSnapshot);
            this.Clear();
            this.AddRange(items);
        }


        // IEnumerable implementation -----------------------------------------

        IEnumerator IEnumerable.GetEnumerator()
        {
            return items.GetEnumerator();
        }

        // Public methods -----------------------------------------------------

        public int IndexOf(TItem item)
        {
            return items.IndexOf(item);
        }

        public void Insert(int index, TItem item)
        {
            Document?.History?.AddRefCollectionItemInsertedEntry(this, index, ChangeSource.Usage);

            if (item != null)
                item.AttachToCollection(this);

            items.Insert(index, item);
        }

        public void RemoveAt(int index)
        {
            Document?.History?.AddRefCollectionItemRemovedEntry(this, index, items[index], ChangeSource.Usage);

            var item = items[index];            
            item?.DetachFromCollection(this);

            items.RemoveAt(index);
        }

        public void Add(TItem item)
        {
            Document?.History?.AddRefCollectionItemAddedEntry(this, ChangeSource.Usage);

            item?.AttachToCollection(this);

            items.Add(item);
        }

        public void AddRange(IEnumerable<TItem> items)
        {
            foreach (var item in items)
                Add(item);
        }

        public void Clear()
        {
            Document?.History?.AddRefCollectionClearedEntry(this, items.ToList(), ChangeSource.Usage);

            foreach (var item in items)
                item?.DetachFromCollection(this);

            items.Clear();
        }

        public bool Contains(TItem item)
        {
            return items.Contains(item);
        }

        public void CopyTo(TItem[] array, int arrayIndex)
        {
            items.CopyTo(array, arrayIndex);
        }

        public bool Remove(TItem item)
        {
            int index = items.IndexOf(item);
            if (index >= 0)
            {
                Document?.History?.AddRefCollectionItemRemovedEntry(this, index, item, ChangeSource.Usage);

                item?.DetachFromCollection(this);
                items.RemoveAt(index);
                return true;
            }
            else
                return false;
        }

        public IEnumerator<TItem> GetEnumerator()
        {
            return items.GetEnumerator();
        }

        public void CopyTo(Array array, int index)
        {
            ((ICollection)items).CopyTo(array, index);
        }

        // Public properties --------------------------------------------------

        public int Count => items.Count;

        public bool IsReadOnly => ((ICollection<TItem>)items).IsReadOnly;

        public object SyncRoot => ((ICollection)items).SyncRoot;

        public bool IsSynchronized => ((ICollection)items).IsSynchronized;

        public TItem this[int index] 
        { 
            get => ((IList<TItem>)items)[index]; 
            set => SetItem(index, value); 
        }
    }
}
