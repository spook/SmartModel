﻿using SmartModel.Infrastructure.HistoryEntries;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SmartModel.Infrastructure
{
    internal class RefField<TRef> : BaseTypedField<TRef>
        where TRef : BaseItem, new()
    {
        // Private fields -----------------------------------------------------

        private TRef value;

        // Private methods ----------------------------------------------------

        private void SetValue(TRef value)
        {
            Document?.History?.AddReferenceChangedEntry(this, this.value, ChangeSource.Usage);

            if (this.value != null)
                this.value.DetachFromParent(parent);

            this.value = value;

            value?.AttachToParent(parent);
        }

        private void Apply(TRef oldValue, bool undo, OnUpdateSnapshotDelegate onUpdateSnapshot)
        {
            Document.History.AddReferenceChangedEntry(this, value, undo ? ChangeSource.Undo : ChangeSource.Redo);
            this.value?.DetachFromParent(parent);

            this.value = oldValue;
            this.value?.AttachToParent(parent);

            if (value != null)
                onUpdateSnapshot(value.InstanceId, value);
        }

        // Internal methods ---------------------------------------------------

        internal RefField(string name, BaseFieldItemContainer parent, TRef defaultValue = default) 
            : base(name, parent)
        {
            value = defaultValue;
            if (value != null)
            {
                value.AttachToParent(parent);
            }
        }

        internal override void SnapshotInto(BaseField other, OnUpdateSnapshotDelegate onNewSnapshot)
        {
            var typedOther = (RefField<TRef>)other;
            typedOther.Value = (TRef)value?.CreateSnapshot(onNewSnapshot);
        }

        internal override void BinarySnapshotInto(BinaryWriter writer, OnUpdateSnapshotDelegate onNewSnapshot)
        {
            writer.Write(value == null ? (byte)0 : (byte)1);
            if (value != null)
                value.CreateBinarySnapshot(writer, onNewSnapshot);
        }

        internal override void DeserializeSnapshot(BinaryReader reader, OnUpdateSnapshotDelegate onUpdateSnapshot)
        {
            byte isNull = reader.ReadByte();
            if (isNull == 0)
                value = null;
            else
                value = (TRef)BaseFieldItemContainer.DeserializeSnapshot(reader, onUpdateSnapshot);
        }

        internal override void Apply(BaseHistoryEntry entry, bool undo, OnUpdateSnapshotDelegate onUpdateSnapshot)
        {
            if (entry is RefFieldValueChangedHistoryEntry<TRef> refEntry)
            {
                Apply(refEntry.OldValue, undo, onUpdateSnapshot);
            }
            else
                throw new InvalidOperationException("Unsupported history entry!");
        }

        internal override void Apply(BinaryReader reader, HistoryEntryType historyEntry, bool undo, OnUpdateSnapshotDelegate onUpdateSnapshot)
        {
            byte isNull = reader.ReadByte();
            if (isNull == 0)
            {
                Apply(null, undo, onUpdateSnapshot);
            }
            else
            {
                var obj = (TRef)BaseFieldItemContainer.DeserializeSnapshot(reader, onUpdateSnapshot);
                Apply(obj, undo, onUpdateSnapshot);
            }
        }

        // Public properties --------------------------------------------------

        public override TRef Value
        {
            get => value;
            set => SetValue(value);
        }
    }
}
