﻿using SmartModel.Infrastructure.HistoryEntries;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SmartModel.Infrastructure
{
    public abstract class BaseCollection : BaseObject
    {
        private readonly BaseFieldItemContainer parent;
        private readonly string name;

        protected override BaseObject GetParent() => parent;

        internal BaseCollection(BaseFieldItemContainer parent, string name)
        {
            this.parent = parent;
            this.name = name;
        }

        internal abstract void SnapshotInto(BaseCollection collection, OnUpdateSnapshotDelegate onNewSnapshot);
        internal abstract void Apply(BaseHistoryEntry entry, bool undo, OnUpdateSnapshotDelegate handleSnapshotUpdated);
        internal abstract void Apply(BinaryReader reader, HistoryEntryType entryType, bool undo, OnUpdateSnapshotDelegate handleSnapshotUpdated);
        internal abstract void BinarySnapshotInto(BinaryWriter writer, OnUpdateSnapshotDelegate onNewSnapshot);
        internal abstract void DeserializeSnapshot(BinaryReader reader, OnUpdateSnapshotDelegate onUpdateSnapshot);

        public BaseFieldItemContainer Parent => parent;
        public string Name => name;
    }
}
