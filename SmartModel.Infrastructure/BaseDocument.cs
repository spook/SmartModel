﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SmartModel.Infrastructure
{
    public class BaseDocument : BaseFieldItemContainer
    {
        protected override BaseObject GetParent() => null;

        public History History { get; set; }
    }
}
