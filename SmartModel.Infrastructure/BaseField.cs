﻿using SmartModel.Infrastructure.HistoryEntries;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SmartModel.Infrastructure
{
    public abstract class BaseField
    {
        private Lazy<BaseDocument> document;

        private BaseDocument GetDocument()
        {
            if (parent == null)
                return null;

            return parent.Document;
        }

        protected readonly string name;
        protected readonly BaseFieldItemContainer parent;

        internal BaseField(string name, BaseFieldItemContainer parent)
        {
            this.name = name;
            this.parent = parent;
            document = new Lazy<BaseDocument>(() => GetDocument());
        }

        internal void ClearDocument()
        {
            if (document.IsValueCreated)
                document = new Lazy<BaseDocument>(() => GetDocument());
        }

        internal abstract void Apply(BaseHistoryEntry entry, bool undo, OnUpdateSnapshotDelegate onUpdateSnapshot);

        internal abstract void Apply(BinaryReader reader, HistoryEntryType historyEntry, bool undo, OnUpdateSnapshotDelegate onUpdateSnapshot);

        internal abstract void SnapshotInto(BaseField other, OnUpdateSnapshotDelegate onNewSnapshot);

        internal abstract void BinarySnapshotInto(BinaryWriter writer, OnUpdateSnapshotDelegate onNewSnapshot);

        internal abstract void DeserializeSnapshot(BinaryReader reader, OnUpdateSnapshotDelegate onUpdateSnapshot);

        public BaseFieldItemContainer Parent => parent;

        public BaseDocument Document => document.Value;

        public string Name => name;
    }
}
