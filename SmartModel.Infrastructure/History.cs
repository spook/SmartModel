﻿using SmartModel.Infrastructure.HistoryEntries;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SmartModel.Infrastructure
{
    public partial class History : IDisposable
    {
        // Private types ------------------------------------------------------

        private abstract class BaseHistoryScope : IDisposable
        {
            protected readonly Action<BaseHistoryScope> disposeAction;
            protected readonly ChangeSource source;

            protected BaseHistoryScope(Action<BaseHistoryScope> disposeAction, ChangeSource source)
            {
                this.disposeAction = disposeAction;
                this.source = source;
            }

            public ChangeSource Source => source;

            public abstract void Dispose();
        }

        [DebuggerDisplay("History scope with source {Source} and {Entries.Count} entries")]
        private class HistoryScope : BaseHistoryScope
        {
            public HistoryScope(Action<BaseHistoryScope> disposeAction, ChangeSource source)
                : base(disposeAction, source)
            {
                Entries = new List<BaseHistoryEntry>();
            }

            public override void Dispose()
            {
                disposeAction(this);
            }

            public List<BaseHistoryEntry> Entries { get; }
        }

        private class BinaryScope : BaseHistoryScope, IBinaryHistoryEntrySerializer
        {
            private readonly MemoryStream data;
            private readonly BinaryWriter writer;
            private long previousEntryOffset = -1;
            private long currentEntryOffset = 0;
            private int refCount;

            public BinaryScope(Action<BaseHistoryScope> disposeAction, ChangeSource source, int? preallocateBytes)
                : base(disposeAction, source)
            {
                if (preallocateBytes != null)
                    data = new MemoryStream(preallocateBytes.Value);
                else
                    data = new MemoryStream();

                writer = new BinaryWriter(data);
                refCount = 1;
            }

            public void WritePreviousEntryOffset()
            {
                writer.Write(previousEntryOffset);
                previousEntryOffset = currentEntryOffset;
                currentEntryOffset = data.Position;
            }

            public void WriteEntryHeader(HistoryEntryType entryType, long instanceId)
            {
                writer.Write((byte)entryType);
                writer.Write(instanceId);
            }

            public MemoryStream Data => data;

            public BinaryWriter Writer => writer;

            public override void Dispose()
            {
                if (refCount > 1)
                {
                    refCount--;
                }
                else
                {
                    disposeAction(this);
                }
            }

            internal void IncreaseRefCount()
            {
                refCount++;
            }
        }

        // Private fields -----------------------------------------------------

        private readonly Dictionary<long, HistoryObjectReference> objectCache;

        private readonly List<BaseHistoryEntry> undoEntries;
        private readonly List<BaseHistoryEntry> redoEntries;

        private readonly Stack<BaseHistoryScope> scopes = new Stack<BaseHistoryScope>();

        // Private methods ----------------------------------------------------

        private void HandleLastReferenceRemoved(HistoryObjectReference sender)
        {
            objectCache.Remove(sender.InstanceId);
        }

        private HistoryObjectReference GetReference(BaseFieldItemContainer obj)
        {
            if (!objectCache.TryGetValue(obj.InstanceId, out HistoryObjectReference reference))
            {
                reference = new HistoryObjectReference(obj.InstanceId, obj, HandleLastReferenceRemoved);
                objectCache.Add(obj.InstanceId, reference);
            }

            return reference;
        }

        private void AddEntry(BaseHistoryEntry entry, ChangeSource source)
        {
            if (scopes.Any())
            {
                var historyScope = (HistoryScope)scopes.Peek();

                if (historyScope.Source != source)
                    throw new InvalidOperationException($"Cannot add operation with source {source} to scope with source {scopes.Peek().Source}.");

                historyScope.Entries.Add(entry);
            }
            else
            {
                switch (source)
                {
                    case ChangeSource.Undo:
                        {
                            redoEntries.Add(entry);
                            break;
                        }
                    case ChangeSource.Redo:
                        {
                            undoEntries.Add(entry);
                            break;
                        }
                    case ChangeSource.Usage:
                        {
                            if (redoEntries.Any())
                            {
                                foreach (var redoEntry in redoEntries)
                                    redoEntry.Dispose();

                                redoEntries.Clear();
                            }

                            undoEntries.Add(entry);
                            break;
                        }
                    default:
                        throw new InvalidOperationException("Unsupported change source!");
                }
            }
        }

        private void HandleSnapshotCreated(long instanceId, BaseFieldItemContainer snapshot)
        {
            // This means, that one of the objects we may possibly track with
            // itemCache might have been frozen. We need to switch the stored
            // reference to the snapshot, because other history items
            // may be referring to this object.

            if (objectCache.TryGetValue(instanceId, out HistoryObjectReference reference))
                reference.Item = null;
        }

        private void HandleSnapshotUpdated(long instanceId, BaseFieldItemContainer snapshot)
        {
            if (objectCache.TryGetValue(instanceId, out HistoryObjectReference reference))
                reference.Item = snapshot;
        }

        private void DoApplyEntry(BaseHistoryEntry entry, bool undo)
        {
            switch (entry)
            {
                case BaseFieldValueHistoryEntry valueEntry:
                    {
                        valueEntry.Reference.Fields[valueEntry.FieldName].Apply(entry, undo, HandleSnapshotUpdated);
                        break;
                    }
                case BaseRefFieldHistoryEntry refEntry:
                    {
                        refEntry.Reference.Fields[refEntry.FieldName].Apply(entry, undo, HandleSnapshotUpdated);
                        break;
                    }
                case BaseCollectionHistoryEntry collectionEntry:
                    {
                        collectionEntry.Reference.Collections[collectionEntry.CollectionName].Apply(entry, undo, HandleSnapshotUpdated);
                        break;
                    }
                case GroupHistoryEntry groupEntry:
                    {
                        // We need a scope to create redo-scope
                        using (var scope = undo ? StartScope(ChangeSource.Undo) : StartScope(ChangeSource.Redo))
                        {
                            // Both undo and redo are always processed from end to beginning
                            for (int i = groupEntry.Entries.Count - 1; i >= 0; i--)
                            {
                                var grouppedEntry = groupEntry.Entries[i];
                                DoApplyEntry(grouppedEntry, undo);

                                // We don't dispose sub-entries; they will get disposed along
                                // with the group entry, which is the topmost (on undoEntries
                                // or redoEntries list)
                            }
                        }

                        break;
                    }
                case BinaryHistoryEntry binaryEntry:
                    {
                        // We need a binary scope to create redo-scope
                        using (var scope = undo ? StartBinaryScope(ChangeSource.Undo) : StartBinaryScope(ChangeSource.Redo))
                        {
                            DoApplyBinaryEntry(undo, binaryEntry);
                        }

                        break;
                    }
                default:
                    throw new InvalidOperationException("Unsupported history entry!");
            }
        }

        private void HandleScopeClosed(BaseHistoryScope sourceScope)
        {
            if (scopes.Peek() != sourceScope)
                throw new InvalidOperationException("Cannot close the scope: scopes must be closed in reverse order of their creation!");

            if (scopes.Peek() is HistoryScope)
            {
                var scope = scopes.Pop() as HistoryScope;
                if (!scope.Entries.Any())
                    return;

                var historyEntry = new GroupHistoryEntry(scope.Entries);
                AddEntry(historyEntry, scope.Source);
            }
            else if (scopes.Peek() is BinaryScope)
            {
                var scope = scopes.Pop() as BinaryScope;

                if (scope.Data.Length == 0)
                    return;

                // Write offset to last entry
                scope.WritePreviousEntryOffset();

                scope.Data.Seek(0, SeekOrigin.Begin);
                var binaryEntry = new BinaryHistoryEntry(objectCache, scope.Data);
                AddEntry(binaryEntry, scope.Source);
            }
            else
                throw new InvalidOperationException("Unsupported scope type!");
        }

        private IDisposable StartScope(ChangeSource source)
        {
            if (scopes.Any() && scopes.Peek().Source != source)
                throw new InvalidOperationException($"Cannot start scope with source {source}, when the topmost scope has source {scopes.Peek().Source}.");

            // Binary scope cannot be recursive
            if (scopes.Any() && scopes.Peek() is BinaryScope binaryScope)
            {
                binaryScope.IncreaseRefCount();
                return binaryScope;
            }

            var scope = new HistoryScope(HandleScopeClosed, source);
            scopes.Push(scope);

            return scope;
        }

        private IDisposable StartBinaryScope(ChangeSource source, int? preallocateBytes = null)
        {
            if (scopes.Any() && scopes.Peek().Source != source)
                throw new InvalidOperationException($"Cannot start scope with source {source}, when the topmost scope has source {scopes.Peek().Source}.");

            // Binary scope cannot be recursive
            if (scopes.Any() && scopes.Peek() is BinaryScope binaryScope)
            {
                binaryScope.IncreaseRefCount();
                return binaryScope;
            }

            var scope = new BinaryScope(HandleScopeClosed, source, preallocateBytes);
            scopes.Push(scope);

            return scope;
        }

        // Internal methods ---------------------------------------------------

        // Reference field

        internal void AddReferenceChangedEntry<TRef>(RefField<TRef> refField, TRef value, ChangeSource source)
            where TRef : BaseItem, new()
        {
            HistoryObjectReference reference = GetReference(refField.Parent);

            if (scopes.Any() && scopes.Peek() is BinaryScope binaryScope)
            {
                RefFieldValueChangedHistoryEntry<TRef>.Serialize(reference, refField.Name, value, binaryScope, HandleSnapshotCreated);
            }
            else
            {
                TRef snapshot = (TRef)value?.CreateSnapshot(HandleSnapshotCreated);
                var entry = new RefFieldValueChangedHistoryEntry<TRef>(reference, refField.Name, snapshot);

                AddEntry(entry, source);
            }
        }

        // Reference collection

        internal void AddRefCollectionClearedEntry<TItem>(RefCollection<TItem> collection, List<TItem> data, ChangeSource source)
            where TItem : BaseCollectionItem, new()
        {
            HistoryObjectReference reference = GetReference(collection.Parent);

            if (scopes.Any() && scopes.Peek() is BinaryScope binaryScope)
            {
                CollectionClearedHistoryEntry<TItem>.Serialize(reference, collection.Name, collection, binaryScope, HandleSnapshotCreated);
            }
            else
            {
                List<TItem> snapshot = data
                .Select(item => item.CreateSnapshot(HandleSnapshotCreated))
                .Cast<TItem>()
                .ToList();

                var entry = new CollectionClearedHistoryEntry<TItem>(reference, collection.Name, snapshot);

                AddEntry(entry, source);
            }
        }

        internal void AddRefCollectionItemReplacedEntry<TItem>(RefCollection<TItem> collection, int index, TItem item, ChangeSource source)
            where TItem : BaseCollectionItem, new()
        {
            HistoryObjectReference reference = GetReference(collection.Parent);

            if (scopes.Any() && scopes.Peek() is BinaryScope binaryScope)
            {
                CollectionItemReplacedHistoryEntry<TItem>.Serialize(reference, collection.Name, index, item, binaryScope, HandleSnapshotCreated);
            }
            else
            {
                TItem snapshot = (TItem)item?.CreateSnapshot(HandleSnapshotCreated);

                var entry = new CollectionItemReplacedHistoryEntry<TItem>(reference, collection.Name, index, snapshot);

                AddEntry(entry, source);
            }
        }

        internal void AddRefCollectionItemInsertedEntry<TItem>(RefCollection<TItem> collection, int index, ChangeSource source)
            where TItem : BaseCollectionItem, new()
        {
            HistoryObjectReference reference = GetReference(collection.Parent);

            if (scopes.Any() && scopes.Peek() is BinaryScope binaryScope)
            {
                CollectionItemInsertedHistoryEntry.Serialize(reference, collection.Name, index, binaryScope, HandleSnapshotCreated);
            }
            else
            {
                var entry = new CollectionItemInsertedHistoryEntry(reference, collection.Name, index);

                AddEntry(entry, source);
            }
        }

        internal void AddRefCollectionItemRemovedEntry<TItem>(RefCollection<TItem> collection, int index, TItem item, ChangeSource source)
            where TItem : BaseCollectionItem, new()
        {
            HistoryObjectReference reference = GetReference(collection.Parent);

            if (scopes.Any() && scopes.Peek() is BinaryScope binaryScope)
            {
                CollectionItemRemovedHistoryEntry<TItem>.Serialize(reference, collection.Name, index, item, binaryScope, HandleSnapshotCreated);
            }
            else
            {
                TItem snapshot = (TItem)item?.CreateSnapshot(HandleSnapshotCreated);
                var entry = new CollectionItemRemovedHistoryEntry<TItem>(reference, collection.Name, index, snapshot);

                AddEntry(entry, source);
            }
        }

        internal void AddRefCollectionItemAddedEntry<TItem>(RefCollection<TItem> collection, ChangeSource source)
            where TItem : BaseCollectionItem, new()
        {
            HistoryObjectReference reference = GetReference(collection.Parent);

            if (scopes.Any() && scopes.Peek() is BinaryScope binaryScope)
            {
                CollectionItemAddedHistoryEntry.Serialize(reference, collection.Name, binaryScope, HandleSnapshotCreated);
            }
            else
            {
                var entry = new CollectionItemAddedHistoryEntry(reference, collection.Name);
                AddEntry(entry, source);
            }
        }

        internal void AddRefCollectionRestoredHistoryEntry<TItem>(RefCollection<TItem> collection, ChangeSource source)
            where TItem : BaseCollectionItem, new()
        {
            HistoryObjectReference reference = GetReference(collection.Parent);

            if (scopes.Any() && scopes.Peek() is BinaryScope binaryScope)
            {
                CollectionRestoredHistoryEntry.Serialize(reference, collection.Name, binaryScope, HandleSnapshotCreated);
            }
            else
            {
                var entry = new CollectionRestoredHistoryEntry(reference, collection.Name);
                AddEntry(entry, source);
            }
        }

        // Collection

        internal void AddCollectionRestoredHistoryEntry<TItem>(Collection<TItem> collection, ChangeSource source)
        {
            HistoryObjectReference reference = GetReference(collection.Parent);

            if (scopes.Any() && scopes.Peek() is BinaryScope binaryScope)
            {
                CollectionRestoredHistoryEntry.Serialize(reference, collection.Name, binaryScope, HandleSnapshotCreated);
            }
            else
            {
                var entry = new CollectionRestoredHistoryEntry(reference, collection.Name);
                AddEntry(entry, source);
            }
        }

        internal void AddCollectionItemRemovedEntry<TItem>(Collection<TItem> collection, int index, TItem item, ChangeSource source)
        {
            HistoryObjectReference reference = GetReference(collection.Parent);

            if (scopes.Any() && scopes.Peek() is BinaryScope binaryScope)
            { 
                CollectionItemRemovedHistoryEntry<TItem>.Serialize(reference, collection.Name, index, item, binaryScope, HandleSnapshotCreated);
            }
            else
            {
                var entry = new CollectionItemRemovedHistoryEntry<TItem>(reference, collection.Name, index, item);

                AddEntry(entry, source);
            }
        }

        internal void AddCollectionItemAddedEntry<TItem>(Collection<TItem> collection, ChangeSource source)
        {
            HistoryObjectReference reference = GetReference(collection.Parent);

            if (scopes.Any() && scopes.Peek() is BinaryScope binaryScope)
            {
                CollectionItemAddedHistoryEntry.Serialize(reference, collection.Name, binaryScope, HandleSnapshotCreated);
            }
            else
            {
                var entry = new CollectionItemAddedHistoryEntry(reference, collection.Name);

                AddEntry(entry, source);
            }
        }

        internal void AddCollectionClearedEntry<TItem>(Collection<TItem> collection, List<TItem> data, ChangeSource source)
        {
            HistoryObjectReference reference = GetReference(collection.Parent);

            if (scopes.Any() && scopes.Peek() is BinaryScope binaryScope)
            {
                CollectionClearedHistoryEntry<TItem>.Serialize(reference, collection.Name, collection, binaryScope, HandleSnapshotCreated);
            }
            else
            {
                var entry = new CollectionClearedHistoryEntry<TItem>(reference, collection.Name, data.ToList());

                AddEntry(entry, source);
            }
        }

        internal void AddCollectionItemReplacedEntry<TItem>(Collection<TItem> collection, int index, TItem item, ChangeSource source)
        {
            HistoryObjectReference reference = GetReference(collection.Parent);

            if (scopes.Any() && scopes.Peek() is BinaryScope binaryScope)
            {
                CollectionItemReplacedHistoryEntry<TItem>.Serialize(reference, collection.Name, index, item, binaryScope, HandleSnapshotCreated);
            }
            else
            {
                var entry = new CollectionItemReplacedHistoryEntry<TItem>(reference, collection.Name, index, item);

                AddEntry(entry, source);
            }
        }

        internal void AddCollectionItemInsertedEntry<TItem>(Collection<TItem> collection, int index, ChangeSource source)
        {
            HistoryObjectReference reference = GetReference(collection.Parent);

            if (scopes.Any() && scopes.Peek() is BinaryScope binaryScope)
            {
                CollectionItemInsertedHistoryEntry.Serialize(reference, collection.Name, index, binaryScope, HandleSnapshotCreated);
            }
            else
            {
                var entry = new CollectionItemInsertedHistoryEntry(reference, collection.Name, index);

                AddEntry(entry, source);
            }
        }

        // Public methods -----------------------------------------------------

        public History()
        {
            objectCache = new Dictionary<long, HistoryObjectReference>();
            undoEntries = new List<BaseHistoryEntry>();
            redoEntries = new List<BaseHistoryEntry>();
        }

        public void Undo()
        {
            if (!undoEntries.Any())
                return;

            var entry = undoEntries.Last();
            undoEntries.RemoveAt(undoEntries.Count - 1);

            DoApplyEntry(entry, true);

            entry.Dispose();
        }

        public void Redo()
        {
            if (!redoEntries.Any())
                return;

            var entry = redoEntries.Last();
            redoEntries.RemoveAt(redoEntries.Count - 1);

            DoApplyEntry(entry, false);

            entry.Dispose();
        }

        public IDisposable StartUndoScope()
        {
            return StartScope(ChangeSource.Usage);
        }

        public IDisposable StartBinaryUndoScope(int? preallocateBytes = null)
        {
            return StartBinaryScope(ChangeSource.Usage, preallocateBytes);
        }

        public void Dispose()
        {
            if (scopes.Any())
                throw new InvalidOperationException("Cannot dispose history when there's an open history scope!");

            foreach (var entry in undoEntries)
                entry.Dispose();
            foreach (var entry in redoEntries)
                entry.Dispose();

            undoEntries.Clear();
            redoEntries.Clear();
            objectCache.Clear();
        }
    }
}
