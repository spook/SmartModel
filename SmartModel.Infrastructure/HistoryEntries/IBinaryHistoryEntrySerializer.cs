﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SmartModel.Infrastructure.HistoryEntries
{
    internal interface IBinaryHistoryEntrySerializer
    {
        void WritePreviousEntryOffset();

        void WriteEntryHeader(HistoryEntryType entryType, long instanceId);
        BinaryWriter Writer { get; }
    }
}
