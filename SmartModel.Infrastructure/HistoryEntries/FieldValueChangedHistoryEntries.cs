﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading.Tasks;

namespace SmartModel.Infrastructure.HistoryEntries
{
    [DebuggerDisplay("SbyteField {FieldName} in object [{Reference.InstanceId}] {Reference.GetType().Name} changed from {OldValue}")]
    internal class SbyteFieldValueChangedHistoryEntry : BaseFieldValueHistoryEntry
    {
        public SbyteFieldValueChangedHistoryEntry(HistoryObjectReference objectReference, string fieldName, sbyte oldValue)
            : base(objectReference, fieldName)
        {
            OldValue = oldValue;
        }

        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        public static void Serialize(HistoryObjectReference reference, 
            string fieldName, 
            sbyte value, 
            IBinaryHistoryEntrySerializer serializer)
        {
            reference.AddReference();

            serializer.WriteEntryHeader(HistoryEntryType.SbyteValueChange, reference.InstanceId);
            serializer.Writer.Write(fieldName);
            serializer.Writer.Write(value);

            serializer.WritePreviousEntryOffset();
        }

        public sbyte OldValue { get; }
    }

    [DebuggerDisplay("ByteField {FieldName} in object [{Reference.InstanceId}] {Reference.GetType().Name} changed from {OldValue}")]
    internal class ByteFieldValueChangedHistoryEntry : BaseFieldValueHistoryEntry
    {
        public ByteFieldValueChangedHistoryEntry(HistoryObjectReference objectReference, string fieldName, byte oldValue)
            : base(objectReference, fieldName)
        {
            OldValue = oldValue;
        }

        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        public static void Serialize(HistoryObjectReference reference, 
            string fieldName, 
            byte value, 
            IBinaryHistoryEntrySerializer serializer)
        {
            reference.AddReference();

            serializer.WriteEntryHeader(HistoryEntryType.ByteValueChange, reference.InstanceId);
            serializer.Writer.Write(fieldName);
            serializer.Writer.Write(value);

            serializer.WritePreviousEntryOffset();
        }

        public byte OldValue { get; }
    }

    [DebuggerDisplay("ShortField {FieldName} in object [{Reference.InstanceId}] {Reference.GetType().Name} changed from {OldValue}")]
    internal class ShortFieldValueChangedHistoryEntry : BaseFieldValueHistoryEntry
    {
        public ShortFieldValueChangedHistoryEntry(HistoryObjectReference objectReference, string fieldName, short oldValue)
            : base(objectReference, fieldName)
        {
            OldValue = oldValue;
        }

        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        public static void Serialize(HistoryObjectReference reference, 
            string fieldName, 
            short value, 
            IBinaryHistoryEntrySerializer serializer)
        {
            reference.AddReference();

            serializer.WriteEntryHeader(HistoryEntryType.ShortValueChange, reference.InstanceId);
            serializer.Writer.Write(fieldName);
            serializer.Writer.Write(value);

            serializer.WritePreviousEntryOffset();
        }

        public short OldValue { get; }
    }

    [DebuggerDisplay("UshortField {FieldName} in object [{Reference.InstanceId}] {Reference.GetType().Name} changed from {OldValue}")]
    internal class UshortFieldValueChangedHistoryEntry : BaseFieldValueHistoryEntry
    {
        public UshortFieldValueChangedHistoryEntry(HistoryObjectReference objectReference, string fieldName, ushort oldValue)
            : base(objectReference, fieldName)
        {
            OldValue = oldValue;
        }

        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        public static void Serialize(HistoryObjectReference reference, 
            string fieldName, 
            ushort value, 
            IBinaryHistoryEntrySerializer serializer)
        {
            reference.AddReference();

            serializer.WriteEntryHeader(HistoryEntryType.UshortValueChange, reference.InstanceId);
            serializer.Writer.Write(fieldName);
            serializer.Writer.Write(value);

            serializer.WritePreviousEntryOffset();
        }

        public ushort OldValue { get; }
    }

    [DebuggerDisplay("IntField {FieldName} in object [{Reference.InstanceId}] {Reference.GetType().Name} changed from {OldValue}")]
    internal class IntFieldValueChangedHistoryEntry : BaseFieldValueHistoryEntry
    {
        public IntFieldValueChangedHistoryEntry(HistoryObjectReference objectReference, string fieldName, int oldValue)
            : base(objectReference, fieldName)
        {
            OldValue = oldValue;
        }

        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        public static void Serialize(HistoryObjectReference reference, 
            string fieldName, 
            int value, 
            IBinaryHistoryEntrySerializer serializer)
        {
            reference.AddReference();

            serializer.WriteEntryHeader(HistoryEntryType.IntValueChange, reference.InstanceId);
            serializer.Writer.Write(fieldName);
            serializer.Writer.Write(value);

            serializer.WritePreviousEntryOffset();
        }

        public int OldValue { get; }
    }

    [DebuggerDisplay("UintField {FieldName} in object [{Reference.InstanceId}] {Reference.GetType().Name} changed from {OldValue}")]
    internal class UintFieldValueChangedHistoryEntry : BaseFieldValueHistoryEntry
    {
        public UintFieldValueChangedHistoryEntry(HistoryObjectReference objectReference, string fieldName, uint oldValue)
            : base(objectReference, fieldName)
        {
            OldValue = oldValue;
        }

        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        public static void Serialize(HistoryObjectReference reference, 
            string fieldName, 
            uint value, 
            IBinaryHistoryEntrySerializer serializer)
        {
            reference.AddReference();

            serializer.WriteEntryHeader(HistoryEntryType.UintValueChange, reference.InstanceId);
            serializer.Writer.Write(fieldName);
            serializer.Writer.Write(value);

            serializer.WritePreviousEntryOffset();
        }

        public uint OldValue { get; }
    }

    [DebuggerDisplay("LongField {FieldName} in object [{Reference.InstanceId}] {Reference.GetType().Name} changed from {OldValue}")]
    internal class LongFieldValueChangedHistoryEntry : BaseFieldValueHistoryEntry
    {
        public LongFieldValueChangedHistoryEntry(HistoryObjectReference objectReference, string fieldName, long oldValue)
            : base(objectReference, fieldName)
        {
            OldValue = oldValue;
        }

        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        public static void Serialize(HistoryObjectReference reference, 
            string fieldName, 
            long value, 
            IBinaryHistoryEntrySerializer serializer)
        {
            reference.AddReference();

            serializer.WriteEntryHeader(HistoryEntryType.LongValueChange, reference.InstanceId);
            serializer.Writer.Write(fieldName);
            serializer.Writer.Write(value);

            serializer.WritePreviousEntryOffset();
        }

        public long OldValue { get; }
    }

    [DebuggerDisplay("UlongField {FieldName} in object [{Reference.InstanceId}] {Reference.GetType().Name} changed from {OldValue}")]
    internal class UlongFieldValueChangedHistoryEntry : BaseFieldValueHistoryEntry
    {
        public UlongFieldValueChangedHistoryEntry(HistoryObjectReference objectReference, string fieldName, ulong oldValue)
            : base(objectReference, fieldName)
        {
            OldValue = oldValue;
        }

        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        public static void Serialize(HistoryObjectReference reference, 
            string fieldName, 
            ulong value, 
            IBinaryHistoryEntrySerializer serializer)
        {
            reference.AddReference();

            serializer.WriteEntryHeader(HistoryEntryType.UlongValueChange, reference.InstanceId);
            serializer.Writer.Write(fieldName);
            serializer.Writer.Write(value);

            serializer.WritePreviousEntryOffset();
        }

        public ulong OldValue { get; }
    }

    [DebuggerDisplay("FloatField {FieldName} in object [{Reference.InstanceId}] {Reference.GetType().Name} changed from {OldValue}")]
    internal class FloatFieldValueChangedHistoryEntry : BaseFieldValueHistoryEntry
    {
        public FloatFieldValueChangedHistoryEntry(HistoryObjectReference objectReference, string fieldName, float oldValue)
            : base(objectReference, fieldName)
        {
            OldValue = oldValue;
        }

        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        public static void Serialize(HistoryObjectReference reference, 
            string fieldName, 
            float value, 
            IBinaryHistoryEntrySerializer serializer)
        {
            reference.AddReference();

            serializer.WriteEntryHeader(HistoryEntryType.FloatValueChange, reference.InstanceId);
            serializer.Writer.Write(fieldName);
            serializer.Writer.Write(value);

            serializer.WritePreviousEntryOffset();
        }

        public float OldValue { get; }
    }

    [DebuggerDisplay("DoubleField {FieldName} in object [{Reference.InstanceId}] {Reference.GetType().Name} changed from {OldValue}")]
    internal class DoubleFieldValueChangedHistoryEntry : BaseFieldValueHistoryEntry
    {
        public DoubleFieldValueChangedHistoryEntry(HistoryObjectReference objectReference, string fieldName, double oldValue)
            : base(objectReference, fieldName)
        {
            OldValue = oldValue;
        }

        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        public static void Serialize(HistoryObjectReference reference, 
            string fieldName, 
            double value, 
            IBinaryHistoryEntrySerializer serializer)
        {
            reference.AddReference();

            serializer.WriteEntryHeader(HistoryEntryType.DoubleValueChange, reference.InstanceId);
            serializer.Writer.Write(fieldName);
            serializer.Writer.Write(value);

            serializer.WritePreviousEntryOffset();
        }

        public double OldValue { get; }
    }

    [DebuggerDisplay("DecimalField {FieldName} in object [{Reference.InstanceId}] {Reference.GetType().Name} changed from {OldValue}")]
    internal class DecimalFieldValueChangedHistoryEntry : BaseFieldValueHistoryEntry
    {
        public DecimalFieldValueChangedHistoryEntry(HistoryObjectReference objectReference, string fieldName, decimal oldValue)
            : base(objectReference, fieldName)
        {
            OldValue = oldValue;
        }

        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        public static void Serialize(HistoryObjectReference reference, 
            string fieldName, 
            decimal value, 
            IBinaryHistoryEntrySerializer serializer)
        {
            reference.AddReference();

            serializer.WriteEntryHeader(HistoryEntryType.DecimalValueChange, reference.InstanceId);
            serializer.Writer.Write(fieldName);
            serializer.Writer.Write(value);

            serializer.WritePreviousEntryOffset();
        }

        public decimal OldValue { get; }
    }

    [DebuggerDisplay("BoolField {FieldName} in object [{Reference.InstanceId}] {Reference.GetType().Name} changed from {OldValue}")]
    internal class BoolFieldValueChangedHistoryEntry : BaseFieldValueHistoryEntry
    {
        public BoolFieldValueChangedHistoryEntry(HistoryObjectReference objectReference, string fieldName, bool oldValue)
            : base(objectReference, fieldName)
        {
            OldValue = oldValue;
        }

        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        public static void Serialize(HistoryObjectReference reference, 
            string fieldName, 
            bool value, 
            IBinaryHistoryEntrySerializer serializer)
        {
            reference.AddReference();

            serializer.WriteEntryHeader(HistoryEntryType.BoolValueChange, reference.InstanceId);
            serializer.Writer.Write(fieldName);
            serializer.Writer.Write(value);

            serializer.WritePreviousEntryOffset();
        }

        public bool OldValue { get; }
    }

    [DebuggerDisplay("CharField {FieldName} in object [{Reference.InstanceId}] {Reference.GetType().Name} changed from {OldValue}")]
    internal class CharFieldValueChangedHistoryEntry : BaseFieldValueHistoryEntry
    {
        public CharFieldValueChangedHistoryEntry(HistoryObjectReference objectReference, string fieldName, char oldValue)
            : base(objectReference, fieldName)
        {
            OldValue = oldValue;
        }

        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        public static void Serialize(HistoryObjectReference reference, 
            string fieldName, 
            char value, 
            IBinaryHistoryEntrySerializer serializer)
        {
            reference.AddReference();

            serializer.WriteEntryHeader(HistoryEntryType.CharValueChange, reference.InstanceId);
            serializer.Writer.Write(fieldName);
            serializer.Writer.Write(value);

            serializer.WritePreviousEntryOffset();
        }

        public char OldValue { get; }
    }

    [DebuggerDisplay("StringField {FieldName} in object [{Reference.InstanceId}] {Reference.GetType().Name} changed from {OldValue}")]
    internal class StringFieldValueChangedHistoryEntry : BaseFieldValueHistoryEntry
    {
        public StringFieldValueChangedHistoryEntry(HistoryObjectReference objectReference, string fieldName, string oldValue)
            : base(objectReference, fieldName)
        {
            OldValue = oldValue;
        }

        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        public static void Serialize(HistoryObjectReference reference, 
            string fieldName, 
            string value, 
            IBinaryHistoryEntrySerializer serializer)
        {
            reference.AddReference();

            serializer.WriteEntryHeader(HistoryEntryType.StringValueChange, reference.InstanceId);
            serializer.Writer.Write(fieldName);
            serializer.Writer.Write(value);

            serializer.WritePreviousEntryOffset();
        }

        public string OldValue { get; }
    }

}
