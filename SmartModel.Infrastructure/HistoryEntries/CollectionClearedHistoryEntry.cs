﻿using SmartModel.Infrastructure.Utils;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading.Tasks;

namespace SmartModel.Infrastructure.HistoryEntries
{
    [DebuggerDisplay("Cleared collection {CollectionName} in object [{Reference.InstanceId}] {Reference.GetType().Name}")]
    internal class CollectionClearedHistoryEntry<TItem> : BaseCollectionHistoryEntry
    {
        public CollectionClearedHistoryEntry(HistoryObjectReference objectReference, string collectionName, List<TItem> snapshot) 
            : base(objectReference, collectionName)
        {
            Snapshot = snapshot;
        }

        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        public static void Serialize(HistoryObjectReference reference, 
            string collectionName,             
            IReadOnlyList<TItem> currentItems, 
            IBinaryHistoryEntrySerializer serializer,
            OnUpdateSnapshotDelegate onNewSnapshot)
        {
            reference.AddReference();

            serializer.WriteEntryHeader(HistoryEntryType.CollectionClearedChange, reference.InstanceId);
            serializer.Writer.Write(collectionName);

            BinaryCollections.WriteCollection(currentItems, serializer.Writer, onNewSnapshot);

            serializer.WritePreviousEntryOffset();
        }


        public List<TItem> Snapshot { get; }
    }
}
