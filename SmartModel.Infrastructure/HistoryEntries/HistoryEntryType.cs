﻿namespace SmartModel.Infrastructure.HistoryEntries
{
	public enum HistoryEntryType : byte
	{
		SbyteValueChange = 1,	
		ByteValueChange = 2,	
		ShortValueChange = 3,	
		UshortValueChange = 4,	
		IntValueChange = 5,	
		UintValueChange = 6,	
		LongValueChange = 7,	
		UlongValueChange = 8,	
		FloatValueChange = 9,	
		DoubleValueChange = 10,	
		DecimalValueChange = 11,	
		BoolValueChange = 12,	
		CharValueChange = 13,	
		StringValueChange = 14,	
		RefFieldChange = 15,
		CollectionClearedChange = 16,
		CollectionItemAddedChange = 17,
		CollectionItemInsertedChange = 18,
		CollectionItemRemovedChange = 19,
		CollectionItemReplacedChange = 20,
		CollectionRestoredChange = 21
	}
}