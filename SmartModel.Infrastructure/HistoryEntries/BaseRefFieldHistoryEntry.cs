﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SmartModel.Infrastructure.HistoryEntries
{
    internal class BaseRefFieldHistoryEntry : BaseReferenceHistoryEntry
    {
        public BaseRefFieldHistoryEntry(HistoryObjectReference objectReference, string fieldName) 
            : base(objectReference)
        {
            FieldName = fieldName;
        }

        public string FieldName { get; }
    }
}
