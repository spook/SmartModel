﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SmartModel.Infrastructure.HistoryEntries
{
    internal abstract class BaseReferenceHistoryEntry : BaseHistoryEntry, IDisposable
    {
        private HistoryObjectReference objectReference;
        private bool isDisposed = false;

        public BaseReferenceHistoryEntry(HistoryObjectReference objectReference)
        {
            this.objectReference = objectReference;
            objectReference.AddReference();
        }

        public override void Dispose()
        {
            if (isDisposed)
                throw new InvalidOperationException("Cannot dispose BaseHistoryEntry: already disposed!");

            objectReference.RemoveReference();
            objectReference = null;

            isDisposed = true;
        }

        ~BaseReferenceHistoryEntry()
        {
            if (!isDisposed)
                System.Diagnostics.Debug.WriteLine("History entry was garbage-collected, but not disposed!");
        }

        public BaseFieldItemContainer Reference => objectReference.Item;
    }
}
