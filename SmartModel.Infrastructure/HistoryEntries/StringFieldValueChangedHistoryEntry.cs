﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SmartModel.Infrastructure.HistoryEntries
{
    [DebuggerDisplay("Field {FieldName} in object [{Reference.InstanceId}] {Reference.GetType().Name} changed from {OldValue}")]
    internal class StringFieldValueChangedHistoryEntry : BaseFieldValueHistoryEntry
    {
        public StringFieldValueChangedHistoryEntry(HistoryObjectReference objectReference, string fieldName, string oldValue)
            : base(objectReference, fieldName)
        {
            OldValue = oldValue;
        }

        public string OldValue { get; }
    }
}
