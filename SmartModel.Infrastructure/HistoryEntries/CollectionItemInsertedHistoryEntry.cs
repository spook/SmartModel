﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading.Tasks;

namespace SmartModel.Infrastructure.HistoryEntries
{
    [DebuggerDisplay("Item inserted to collection {CollectionName} in object [{Reference.InstanceId}] {Reference.GetType().Name}")]
    internal class CollectionItemInsertedHistoryEntry : BaseCollectionHistoryEntry
    {
        public CollectionItemInsertedHistoryEntry(HistoryObjectReference objectReference, string collectionName, int index)
            : base(objectReference, collectionName)
        {
            Index = index;
        }

        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        public static void Serialize(HistoryObjectReference reference, 
            string collectionName, 
            int index, 
            IBinaryHistoryEntrySerializer serializer, 
            OnUpdateSnapshotDelegate onNewSnapshot)
        {
            reference.AddReference();

            serializer.WriteEntryHeader(HistoryEntryType.CollectionItemInsertedChange, reference.InstanceId);
            serializer.Writer.Write(collectionName);
            serializer.Writer.Write(index);

            serializer.WritePreviousEntryOffset();
        }

        public int Index { get; }
    }
}
