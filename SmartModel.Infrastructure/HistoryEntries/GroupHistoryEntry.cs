﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SmartModel.Infrastructure.HistoryEntries
{
    [DebuggerDisplay("A group of {Entries.Count} history entries")]
    internal class GroupHistoryEntry : BaseHistoryEntry
    {
        private bool isDisposed = false;

        public GroupHistoryEntry(IList<BaseHistoryEntry> entries) 
        {
            Entries = entries;
        }

        public override void Dispose()
        {
            if (isDisposed)
                throw new InvalidOperationException("Cannot dispose group history entry, already disposed!");

            foreach (var entry in Entries)
                entry.Dispose();

            isDisposed = true;
        }

        public IList<BaseHistoryEntry> Entries { get; }
    }
}
