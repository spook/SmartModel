﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading.Tasks;

namespace SmartModel.Infrastructure.HistoryEntries
{
    [DebuggerDisplay("Collection {CollectionName} restored in object [{Reference.InstanceId}] {Reference.GetType().Name}")]
    internal class CollectionRestoredHistoryEntry : BaseCollectionHistoryEntry
    {
        public CollectionRestoredHistoryEntry(HistoryObjectReference objectReference, string collectionName) 
            : base(objectReference, collectionName)
        {

        }

        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        public static void Serialize(HistoryObjectReference reference, 
            string collectionName, 
            IBinaryHistoryEntrySerializer serializer, 
            OnUpdateSnapshotDelegate onNewSnapshot)
        {
            reference.AddReference();

            serializer.WriteEntryHeader(HistoryEntryType.CollectionRestoredChange, reference.InstanceId);
            serializer.Writer.Write(collectionName);

            serializer.WritePreviousEntryOffset();
        }
    }
}
