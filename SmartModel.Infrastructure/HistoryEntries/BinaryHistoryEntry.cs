﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SmartModel.Infrastructure.HistoryEntries
{
    internal class BinaryHistoryEntry : BaseHistoryEntry
    {
        private readonly IDictionary<long, HistoryObjectReference> objectCache;
        private readonly MemoryStream data;
        // private readonly List<long> referencedInstanceIds;

        public BinaryHistoryEntry(IDictionary<long, HistoryObjectReference> objectCache, MemoryStream data)
        {
            this.objectCache = objectCache;
            this.data = data;
        }

        public override void Dispose()
        {
            // Collect reference IDs

            // A long at the end points to start of last entry
            data.Seek(-sizeof(long), SeekOrigin.End);
            var reader = new BinaryReader(data);

            // int i = referencedInstanceIds.Count - 1;

            // This points to start of next entries (counted from end to start)
            long entryStartOffset = reader.ReadInt64();
            // This points to place in stream, which contains offset to previous
            // entry.
            long nextEntryOffsetOffset = data.Length - sizeof(long) - sizeof(long);

            // Important note: Undo operation is performed from the last entry to the first one (in reverse
            //                 order than operations were performed) - thus specific arrangement of
            //                 BinaryHistoryEntry data. So "Next" entry effectively means previous
            //                 in absolute terms.
            //
            //  Every entry has the following structure:
            //  [Entry type - 1 byte] [InstanceId - 1 long] [ (entry-specific data) ] [NextEntryOffset - 1 long]
            //
            //  1. At the start:
            //  xxxxxx] [NextEntryOffset] [Entry data xxxxxxxx] [NextEntryOffset] [Entry data xxxxxxxx] [NextEntryOffset] [FirstEntryOffset]
            //                                                                    ^ entryStartOffset    ^nextEntryOffsetOffset
            //
            //  2. First step:
            //  xxxxxx] [NextEntryOffset] [Entry data xxxxxxxx] [NextEntryOffset] [Entry data xxxxxxxx] [NextEntryOffset] [FirstEntryOffset]
            //                            ^ entryStartOffset    ^nextEntryOffsetOffset
            //
            //  ... and so on.
            // 
            // The reason for the following implementation is that entry data size is variable - so even
            // when we know, where entry starts, we don't know, where it ends. However, next entry
            // offset is always last item for an entry, so we also know, that it is exactly sizeof(long)
            // before previous entry start. So knowing, where previous entry started, we can subtract
            // sizeof(long) and retrieve current entry's next entry offset.

            // Last entry's next entry offset equals to -1 by design - this will stop the loop.
            while (entryStartOffset >= 0)
            {
                data.Seek(entryStartOffset + 1, SeekOrigin.Begin);
                long instanceId = reader.ReadInt64();

                objectCache[instanceId].RemoveReference();

                // if (instanceId != referencedInstanceIds[i])
                //     throw new InvalidOperationException("Mismatch between binary and stored instance IDs!");

                // i--;
                
                // We know, that next entry's next entry offset (counting from end)
                // is just sizeof(long) before current entry's start.
                long newNextEntryOffsetOffset = entryStartOffset - sizeof(long);

                // We are reading current entry's next entry offset
                data.Seek(nextEntryOffsetOffset, SeekOrigin.Begin);
                entryStartOffset = reader.ReadInt64();

                // We are setting nextEntryOffsetOffset to an offset stored earlier.
                // Now it will be pointing to current entry's next entry offset.
                nextEntryOffsetOffset = newNextEntryOffsetOffset;
            }

            // foreach (var id in referencedInstanceIds)
            //     objectCache[id].RemoveReference();

            data.Close();
        }

        public MemoryStream Data => data;
    }
}
