﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SmartModel.Infrastructure.HistoryEntries
{
    [DebuggerDisplay("Field {FieldName} in object [{Reference.InstanceId}] {Reference.GetType().Name} changed from {OldValue}")]
    internal class FieldValueChangedHistoryEntry<TValue> : BaseFieldValueHistoryEntry
        where TValue : struct
    {
        public FieldValueChangedHistoryEntry(HistoryObjectReference objectReference, string fieldName, TValue oldValue)
            : base(objectReference, fieldName)
        {
            OldValue = oldValue;
        }

        public TValue OldValue { get; }
    }
}
