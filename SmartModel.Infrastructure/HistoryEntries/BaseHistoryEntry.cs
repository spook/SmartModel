﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SmartModel.Infrastructure.HistoryEntries
{
    internal abstract class BaseHistoryEntry : IDisposable
    {
        public abstract void Dispose();
    }
}
