﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SmartModel.Infrastructure.HistoryEntries
{
    internal class BaseCollectionHistoryEntry : BaseReferenceHistoryEntry
    {
        public BaseCollectionHistoryEntry(HistoryObjectReference objectReference, string collectionName) : base(objectReference)
        {
            CollectionName = collectionName;
        }

        public string CollectionName { get; }
    }
}
