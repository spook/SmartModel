﻿using SmartModel.Infrastructure.Utils;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading.Tasks;

namespace SmartModel.Infrastructure.HistoryEntries
{
    [DebuggerDisplay("Item replaced in collection {CollectionName} in object [{Reference.InstanceId}] {Reference.GetType().Name}")]
    internal class CollectionItemReplacedHistoryEntry<TItem> : BaseCollectionHistoryEntry
    {
        public CollectionItemReplacedHistoryEntry(HistoryObjectReference objectReference, string collectionName, int index, TItem oldItem) 
            : base(objectReference, collectionName)
        {
            Index = index;
            OldItem = oldItem;
        }

        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        public static void Serialize(HistoryObjectReference reference, 
            string collectionName, 
            int index, 
            TItem oldItem, 
            IBinaryHistoryEntrySerializer serializer, 
            OnUpdateSnapshotDelegate onNewSnapshot)
        {
            reference.AddReference();

            serializer.WriteEntryHeader(HistoryEntryType.CollectionItemReplacedChange, reference.InstanceId);
            serializer.Writer.Write(collectionName);
            serializer.Writer.Write(index);
            BinaryCollections.WriteItem(oldItem, serializer.Writer, onNewSnapshot);

            serializer.WritePreviousEntryOffset();
        }

        public int Index { get; }
        public TItem OldItem { get; }
    }
}
