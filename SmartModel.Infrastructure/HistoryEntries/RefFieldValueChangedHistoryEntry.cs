﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading.Tasks;

namespace SmartModel.Infrastructure.HistoryEntries
{
    [DebuggerDisplay("Field {FieldName} in object [{Reference.InstanceId}] {Reference.GetType().Name} changed from [{OldValue?.InstanceId}] {OldValue?.GetType().Name}")]
    internal class RefFieldValueChangedHistoryEntry<TRef> : BaseRefFieldHistoryEntry
        where TRef : BaseItem, new()
    {
        public RefFieldValueChangedHistoryEntry(HistoryObjectReference objectReference, string fieldName, TRef oldValue) 
            : base(objectReference, fieldName)
        {
            OldValue = oldValue;
        }

        [MethodImpl(MethodImplOptions.AggressiveInlining)]        
        public static void Serialize(HistoryObjectReference reference, 
            string fieldName,
            TRef oldValue, 
            IBinaryHistoryEntrySerializer serializer, 
            OnUpdateSnapshotDelegate onNewSnapshot)
        {
            reference.AddReference();

            serializer.WriteEntryHeader(HistoryEntryType.RefFieldChange, reference.InstanceId);
            serializer.Writer.Write(fieldName);
            serializer.Writer.Write((oldValue != null) ? (byte)1 : (byte)0);
            if (oldValue != null)
                oldValue.CreateBinarySnapshot(serializer.Writer, onNewSnapshot);

            serializer.WritePreviousEntryOffset();
        }

        public TRef OldValue { get; }
    }
}
