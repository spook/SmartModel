﻿namespace SmartModel.Infrastructure
{
    public abstract partial class BaseFieldItemContainer
    {       
        protected IField<sbyte> RegisterSbyteField(string name, sbyte defaultValue = default(sbyte))
        {
            CheckExistingMember(name);

            var field = new SbyteField(name, this, defaultValue);
            fields[name] = field;

            return field;
        }
        protected IField<byte> RegisterByteField(string name, byte defaultValue = default(byte))
        {
            CheckExistingMember(name);

            var field = new ByteField(name, this, defaultValue);
            fields[name] = field;

            return field;
        }
        protected IField<short> RegisterShortField(string name, short defaultValue = default(short))
        {
            CheckExistingMember(name);

            var field = new ShortField(name, this, defaultValue);
            fields[name] = field;

            return field;
        }
        protected IField<ushort> RegisterUshortField(string name, ushort defaultValue = default(ushort))
        {
            CheckExistingMember(name);

            var field = new UshortField(name, this, defaultValue);
            fields[name] = field;

            return field;
        }
        protected IField<int> RegisterIntField(string name, int defaultValue = default(int))
        {
            CheckExistingMember(name);

            var field = new IntField(name, this, defaultValue);
            fields[name] = field;

            return field;
        }
        protected IField<uint> RegisterUintField(string name, uint defaultValue = default(uint))
        {
            CheckExistingMember(name);

            var field = new UintField(name, this, defaultValue);
            fields[name] = field;

            return field;
        }
        protected IField<long> RegisterLongField(string name, long defaultValue = default(long))
        {
            CheckExistingMember(name);

            var field = new LongField(name, this, defaultValue);
            fields[name] = field;

            return field;
        }
        protected IField<ulong> RegisterUlongField(string name, ulong defaultValue = default(ulong))
        {
            CheckExistingMember(name);

            var field = new UlongField(name, this, defaultValue);
            fields[name] = field;

            return field;
        }
        protected IField<float> RegisterFloatField(string name, float defaultValue = default(float))
        {
            CheckExistingMember(name);

            var field = new FloatField(name, this, defaultValue);
            fields[name] = field;

            return field;
        }
        protected IField<double> RegisterDoubleField(string name, double defaultValue = default(double))
        {
            CheckExistingMember(name);

            var field = new DoubleField(name, this, defaultValue);
            fields[name] = field;

            return field;
        }
        protected IField<decimal> RegisterDecimalField(string name, decimal defaultValue = default(decimal))
        {
            CheckExistingMember(name);

            var field = new DecimalField(name, this, defaultValue);
            fields[name] = field;

            return field;
        }
        protected IField<bool> RegisterBoolField(string name, bool defaultValue = default(bool))
        {
            CheckExistingMember(name);

            var field = new BoolField(name, this, defaultValue);
            fields[name] = field;

            return field;
        }
        protected IField<char> RegisterCharField(string name, char defaultValue = default(char))
        {
            CheckExistingMember(name);

            var field = new CharField(name, this, defaultValue);
            fields[name] = field;

            return field;
        }
        protected IField<string> RegisterStringField(string name, string defaultValue = default(string))
        {
            CheckExistingMember(name);

            var field = new StringField(name, this, defaultValue);
            fields[name] = field;

            return field;
        }
    }
}