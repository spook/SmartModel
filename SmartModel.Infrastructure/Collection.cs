﻿using SmartModel.Infrastructure.HistoryEntries;
using SmartModel.Infrastructure.Utils;
using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SmartModel.Infrastructure
{
    public class Collection<TItem> : BaseCollection, IList<TItem>, IReadOnlyList<TItem>, ICollection<TItem>, ICollection, IEnumerable<TItem>, IEnumerable
    {
        // Private constants --------------------------------------------------

        private static readonly HashSet<TypeCode> allowedCodes = new HashSet<TypeCode>
        {
            TypeCode.Boolean,
            TypeCode.Char,
            TypeCode.SByte,
            TypeCode.Byte,
            TypeCode.Int16,
            TypeCode.UInt16,
            TypeCode.Int32,
            TypeCode.UInt32,
            TypeCode.Int64,
            TypeCode.UInt64,
            TypeCode.Single,
            TypeCode.Double,
            TypeCode.Decimal,
            TypeCode.DateTime,
            TypeCode.String,
        };

        // Private fields -----------------------------------------------------

        private readonly List<TItem> items;

        private void SetItem(int index, TItem value)
        {
            Document?.History?.AddCollectionItemReplacedEntry(this, index, items[index], ChangeSource.Usage);
            items[index] = value;
        }

        // Internal fields ----------------------------------------------------

        internal Collection(BaseFieldItemContainer parent, string name)
            : base(parent, name)
        {
            if (!allowedCodes.Contains(Type.GetTypeCode(typeof(TItem))))
                throw new InvalidOperationException("Only simple types + string are allowed for Collection. If you have ref-object, use RegisterRefCollection instead.");

            items = new List<TItem>();
        }

        internal override void SnapshotInto(BaseCollection collection, OnUpdateSnapshotDelegate onNewSnapshot)
        {
            var typedCollection = (Collection<TItem>)collection;
            typedCollection.Clear();

            foreach (var item in items)
                typedCollection.Add((TItem)item);
        }

        private void ApplyCollectionRestored(bool undo, OnUpdateSnapshotDelegate onUpdateSnapshot)
        {
            Document?.History?.AddCollectionClearedEntry(this, items.ToList(), undo ? ChangeSource.Undo : ChangeSource.Redo);

            items.Clear();
        }

        private void ApplyCollectionItemReplaced(bool undo, int index, TItem oldItem, OnUpdateSnapshotDelegate onUpdateSnapshot)
        {
            Document.History.AddCollectionItemReplacedEntry(this, index, items[index], undo ? ChangeSource.Undo : ChangeSource.Redo);

            items[index] = oldItem;
        }

        private void ApplyCollectionItemRemoved(bool undo, int index, TItem oldItem, OnUpdateSnapshotDelegate onUpdateSnapshot)
        {
            Document.History.AddCollectionItemInsertedEntry(this, index, undo ? ChangeSource.Undo : ChangeSource.Redo);

            items.Insert(index, oldItem);
        }

        private void ApplyCollectionItemInserted(bool undo, int index, OnUpdateSnapshotDelegate onUpdateSnapshot)
        {
            Document.History.AddCollectionItemRemovedEntry(this, index, items[index], undo ? ChangeSource.Undo : ChangeSource.Redo);

            items.RemoveAt(index);
        }

        private void ApplyCollectionItemAdded(bool undo, OnUpdateSnapshotDelegate onUpdateSnapshot)
        {
            Document.History.AddCollectionItemRemovedEntry(this, items.Count - 1, items[items.Count - 1], undo ? ChangeSource.Undo : ChangeSource.Redo);

            items.RemoveAt(items.Count - 1);
        }

        private void ApplyCollectionCleared(bool undo, IList<TItem> oldCollection, OnUpdateSnapshotDelegate onUpdateSnapshot)
        {
            Document.History.AddCollectionRestoredHistoryEntry(this, undo ? ChangeSource.Undo : ChangeSource.Redo);

            this.items.Clear();
            foreach (var item in oldCollection)
                this.items.Add(item);
        }

        internal override void Apply(BaseHistoryEntry entry, bool undo, OnUpdateSnapshotDelegate onUpdateSnapshot)
        {
            switch (entry)
            {
                case CollectionClearedHistoryEntry<TItem> collectionCleared:
                    {
                        ApplyCollectionCleared(undo, collectionCleared.Snapshot, onUpdateSnapshot);
                        break;
                    }
                case CollectionItemAddedHistoryEntry itemAdded:
                    {
                        ApplyCollectionItemAdded(undo, onUpdateSnapshot);
                        break;
                    }
                case CollectionItemInsertedHistoryEntry itemInserted:
                    {
                        ApplyCollectionItemInserted(undo, itemInserted.Index, onUpdateSnapshot);
                        break;
                    }
                case CollectionItemRemovedHistoryEntry<TItem> itemRemoved:
                    {
                        ApplyCollectionItemRemoved(undo, itemRemoved.Index, itemRemoved.OldItem, onUpdateSnapshot);
                        break;
                    }
                case CollectionItemReplacedHistoryEntry<TItem> itemReplaced:
                    {
                        ApplyCollectionItemReplaced(undo, itemReplaced.Index, itemReplaced.OldItem, onUpdateSnapshot);
                        break;
                    }
                case CollectionRestoredHistoryEntry restored:
                    {
                        ApplyCollectionRestored(undo, onUpdateSnapshot);
                        break;
                    }
                default:
                    throw new InvalidOperationException("Unsupported history entry!");
            }
        }

        internal override void Apply(BinaryReader reader, HistoryEntryType entryType, bool undo, OnUpdateSnapshotDelegate onUpdateSnapshot)
        {
            switch (entryType)
            {
                case HistoryEntryType.CollectionClearedChange:
                    {
                        List<TItem> oldCollection = BinaryCollections.ReadCollection<TItem>(reader, onUpdateSnapshot);
                        ApplyCollectionCleared(undo, oldCollection, onUpdateSnapshot);

                        break;
                    }
                case HistoryEntryType.CollectionItemAddedChange:
                    {
                        ApplyCollectionItemAdded(undo, onUpdateSnapshot);

                        break;
                    }
                case HistoryEntryType.CollectionItemInsertedChange:
                    {
                        int index = reader.ReadInt32();
                        ApplyCollectionItemInserted(undo, index, onUpdateSnapshot);

                        break;
                    }
                case HistoryEntryType.CollectionItemRemovedChange:
                    {
                        int index = reader.ReadInt32();
                        TItem item = (TItem)BinaryCollections.ReadItem<TItem>(reader, onUpdateSnapshot);

                        ApplyCollectionItemRemoved(undo, index, item, onUpdateSnapshot);

                        break;
                    }
                case HistoryEntryType.CollectionItemReplacedChange:
                    {
                        int index = reader.ReadInt32();
                        TItem item = (TItem)BinaryCollections.ReadItem<TItem>(reader, onUpdateSnapshot);

                        ApplyCollectionItemReplaced(undo, index, item, onUpdateSnapshot);

                        break;
                    }
                case HistoryEntryType.CollectionRestoredChange:
                    {
                        ApplyCollectionRestored(undo, onUpdateSnapshot);

                        break;
                    }
            }
        }

        internal override void BinarySnapshotInto(BinaryWriter writer, OnUpdateSnapshotDelegate onNewSnapshot)
        {
            BinaryCollections.WriteCollection(this, writer, onNewSnapshot);
        }

        internal override void DeserializeSnapshot(BinaryReader reader, OnUpdateSnapshotDelegate onUpdateSnapshot)
        {
            var items = BinaryCollections.ReadCollection<TItem>(reader, onUpdateSnapshot);
            this.Clear();
            AddRange(items);
        }

        // IEnumerable implementation -----------------------------------------

        IEnumerator IEnumerable.GetEnumerator()
        {
            return items.GetEnumerator();
        }

        // Public methods -----------------------------------------------------

        public int IndexOf(TItem item)
        {
            return items.IndexOf(item);
        }

        public void Insert(int index, TItem item)
        {
            Document?.History?.AddCollectionItemInsertedEntry(this, index, ChangeSource.Usage);

            items.Insert(index, item);
        }

        public void RemoveAt(int index)
        {
            Document?.History?.AddCollectionItemRemovedEntry(this, index, items[index], ChangeSource.Usage);

            items.RemoveAt(index);
        }

        public void Add(TItem item)
        {
            Document?.History?.AddCollectionItemAddedEntry(this, ChangeSource.Usage);

            items.Add(item);
        }

        public void AddRange(IEnumerable<TItem> items)
        {
            foreach (var item in items)
                Add(item);
        }

        public void Clear()
        {
            Document?.History?.AddCollectionClearedEntry(this, items.ToList(), ChangeSource.Usage);

            items.Clear();
        }

        public bool Contains(TItem item)
        {
            return items.Contains(item);
        }

        public void CopyTo(TItem[] array, int arrayIndex)
        {
            items.CopyTo(array, arrayIndex);
        }

        public bool Remove(TItem item)
        {
            int index = items.IndexOf(item);
            if (index >= 0)
            {
                Document?.History?.AddCollectionItemRemovedEntry(this, index, item, ChangeSource.Usage);

                items.RemoveAt(index);
                return true;
            }
            else
                return false;
        }

        public IEnumerator<TItem> GetEnumerator()
        {
            return items.GetEnumerator();
        }

        public void CopyTo(Array array, int index)
        {
            ((ICollection)items).CopyTo(array, index);
        }

        // Public properties --------------------------------------------------

        public int Count => items.Count;

        public bool IsReadOnly => ((ICollection<TItem>)items).IsReadOnly;

        public object SyncRoot => ((ICollection)items).SyncRoot;

        public bool IsSynchronized => ((ICollection)items).IsSynchronized;

        public TItem this[int index]
        {
            get => ((IList<TItem>)items)[index];
            set => SetItem(index, value);
        }
    }
}
