﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SmartModel.Infrastructure
{
    public abstract class BaseObject
    {
        // Private fields -----------------------------------------------------

        private Lazy<BaseDocument> document;

        // Private methods ----------------------------------------------------

        private BaseDocument GetDocument()
        {
            if (this is BaseDocument doc)
                return doc;

            var obj = GetParent();
            if (obj != null)
                return obj.Document;

            return null;
        }

        // Protected methods --------------------------------------------------

        protected void NotifyParentChanged()
        {
            ResetDocumentRecursive();
        }

        protected BaseObject()
        {
            document = new Lazy<BaseDocument>(() => GetDocument());
        }

        protected abstract BaseObject GetParent();

        // Internal methods ---------------------------------------------------

        internal virtual void ResetDocumentRecursive()
        {
            document = new Lazy<BaseDocument>(() => GetDocument());
        }

        // Public properties --------------------------------------------------

        public BaseDocument Document => document.Value;
    }
}
