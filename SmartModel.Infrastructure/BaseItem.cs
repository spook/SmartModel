﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SmartModel.Infrastructure
{
    public abstract class BaseItem : BaseFieldItemContainer
    {
        protected internal BaseFieldItemContainer parent;

        protected override BaseObject GetParent() => parent;

        internal void AttachToParent(BaseFieldItemContainer newParent)
        {
            if (parent != null)
                throw new InvalidOperationException("BaseItem is already attached to a parent!");

            parent = newParent;
            ResetDocumentRecursive();
        }

        internal void DetachFromParent(BaseFieldItemContainer currentParent)
        {
            if (this.parent != currentParent)
                throw new InvalidOperationException("Trying to detach from a wrong parent!");

            parent = null;
            ResetDocumentRecursive();
        }


        public BaseItem()
        {

        }

        public BaseFieldItemContainer Parent => parent;
    }
}
