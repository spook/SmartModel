﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SmartModel.Infrastructure
{
    internal abstract class BaseTypedField<T> : BaseField, IField<T>
    {
        internal BaseTypedField(string name, BaseFieldItemContainer parent)
            : base(name, parent)
        {

        }

        public abstract T Value
        {
            get;
            set;
        }
    }
}
