﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SmartModel.Infrastructure
{
    public abstract class BaseCollectionItem : BaseFieldItemContainer
    {
        private BaseCollection parent;

        protected override BaseObject GetParent() => parent;

        internal void AttachToCollection(BaseCollection collection)
        {
            if (parent != null)
                throw new InvalidOperationException("Cannot attach to multiple collections in the model at once!");

            parent = collection;
            ResetDocumentRecursive();
        }

        internal void DetachFromCollection(BaseCollection collection)
        {
            if (parent != collection)
                throw new InvalidOperationException("Trying to detach from wrong collection!");

            parent = null;
            ResetDocumentRecursive();
        }

        public BaseCollectionItem()
        {

        }

        public BaseCollection Parent => parent;
    }
}
