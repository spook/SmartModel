﻿using SmartModel.Infrastructure.HistoryEntries;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SmartModel.Infrastructure
{
    public partial class History
    {
        internal void AddSbyteValueChangedEntry(SbyteField field, sbyte value, ChangeSource source)
        {
            HistoryObjectReference reference = GetReference(field.Parent);

            if (scopes.Any() && scopes.Peek() is BinaryScope binaryScope)
            {
                SbyteFieldValueChangedHistoryEntry.Serialize(reference, field.Name, value, binaryScope);
            }
            else
            {
                var entry = new SbyteFieldValueChangedHistoryEntry(reference, field.Name, value);

                AddEntry(entry, source);
            }
        }

        internal void AddByteValueChangedEntry(ByteField field, byte value, ChangeSource source)
        {
            HistoryObjectReference reference = GetReference(field.Parent);

            if (scopes.Any() && scopes.Peek() is BinaryScope binaryScope)
            {
                ByteFieldValueChangedHistoryEntry.Serialize(reference, field.Name, value, binaryScope);
            }
            else
            {
                var entry = new ByteFieldValueChangedHistoryEntry(reference, field.Name, value);

                AddEntry(entry, source);
            }
        }

        internal void AddShortValueChangedEntry(ShortField field, short value, ChangeSource source)
        {
            HistoryObjectReference reference = GetReference(field.Parent);

            if (scopes.Any() && scopes.Peek() is BinaryScope binaryScope)
            {
                ShortFieldValueChangedHistoryEntry.Serialize(reference, field.Name, value, binaryScope);
            }
            else
            {
                var entry = new ShortFieldValueChangedHistoryEntry(reference, field.Name, value);

                AddEntry(entry, source);
            }
        }

        internal void AddUshortValueChangedEntry(UshortField field, ushort value, ChangeSource source)
        {
            HistoryObjectReference reference = GetReference(field.Parent);

            if (scopes.Any() && scopes.Peek() is BinaryScope binaryScope)
            {
                UshortFieldValueChangedHistoryEntry.Serialize(reference, field.Name, value, binaryScope);
            }
            else
            {
                var entry = new UshortFieldValueChangedHistoryEntry(reference, field.Name, value);

                AddEntry(entry, source);
            }
        }

        internal void AddIntValueChangedEntry(IntField field, int value, ChangeSource source)
        {
            HistoryObjectReference reference = GetReference(field.Parent);

            if (scopes.Any() && scopes.Peek() is BinaryScope binaryScope)
            {
                IntFieldValueChangedHistoryEntry.Serialize(reference, field.Name, value, binaryScope);
            }
            else
            {
                var entry = new IntFieldValueChangedHistoryEntry(reference, field.Name, value);

                AddEntry(entry, source);
            }
        }

        internal void AddUintValueChangedEntry(UintField field, uint value, ChangeSource source)
        {
            HistoryObjectReference reference = GetReference(field.Parent);

            if (scopes.Any() && scopes.Peek() is BinaryScope binaryScope)
            {
                UintFieldValueChangedHistoryEntry.Serialize(reference, field.Name, value, binaryScope);
            }
            else
            {
                var entry = new UintFieldValueChangedHistoryEntry(reference, field.Name, value);

                AddEntry(entry, source);
            }
        }

        internal void AddLongValueChangedEntry(LongField field, long value, ChangeSource source)
        {
            HistoryObjectReference reference = GetReference(field.Parent);

            if (scopes.Any() && scopes.Peek() is BinaryScope binaryScope)
            {
                LongFieldValueChangedHistoryEntry.Serialize(reference, field.Name, value, binaryScope);
            }
            else
            {
                var entry = new LongFieldValueChangedHistoryEntry(reference, field.Name, value);

                AddEntry(entry, source);
            }
        }

        internal void AddUlongValueChangedEntry(UlongField field, ulong value, ChangeSource source)
        {
            HistoryObjectReference reference = GetReference(field.Parent);

            if (scopes.Any() && scopes.Peek() is BinaryScope binaryScope)
            {
                UlongFieldValueChangedHistoryEntry.Serialize(reference, field.Name, value, binaryScope);
            }
            else
            {
                var entry = new UlongFieldValueChangedHistoryEntry(reference, field.Name, value);

                AddEntry(entry, source);
            }
        }

        internal void AddFloatValueChangedEntry(FloatField field, float value, ChangeSource source)
        {
            HistoryObjectReference reference = GetReference(field.Parent);

            if (scopes.Any() && scopes.Peek() is BinaryScope binaryScope)
            {
                FloatFieldValueChangedHistoryEntry.Serialize(reference, field.Name, value, binaryScope);
            }
            else
            {
                var entry = new FloatFieldValueChangedHistoryEntry(reference, field.Name, value);

                AddEntry(entry, source);
            }
        }

        internal void AddDoubleValueChangedEntry(DoubleField field, double value, ChangeSource source)
        {
            HistoryObjectReference reference = GetReference(field.Parent);

            if (scopes.Any() && scopes.Peek() is BinaryScope binaryScope)
            {
                DoubleFieldValueChangedHistoryEntry.Serialize(reference, field.Name, value, binaryScope);
            }
            else
            {
                var entry = new DoubleFieldValueChangedHistoryEntry(reference, field.Name, value);

                AddEntry(entry, source);
            }
        }

        internal void AddDecimalValueChangedEntry(DecimalField field, decimal value, ChangeSource source)
        {
            HistoryObjectReference reference = GetReference(field.Parent);

            if (scopes.Any() && scopes.Peek() is BinaryScope binaryScope)
            {
                DecimalFieldValueChangedHistoryEntry.Serialize(reference, field.Name, value, binaryScope);
            }
            else
            {
                var entry = new DecimalFieldValueChangedHistoryEntry(reference, field.Name, value);

                AddEntry(entry, source);
            }
        }

        internal void AddBoolValueChangedEntry(BoolField field, bool value, ChangeSource source)
        {
            HistoryObjectReference reference = GetReference(field.Parent);

            if (scopes.Any() && scopes.Peek() is BinaryScope binaryScope)
            {
                BoolFieldValueChangedHistoryEntry.Serialize(reference, field.Name, value, binaryScope);
            }
            else
            {
                var entry = new BoolFieldValueChangedHistoryEntry(reference, field.Name, value);

                AddEntry(entry, source);
            }
        }

        internal void AddCharValueChangedEntry(CharField field, char value, ChangeSource source)
        {
            HistoryObjectReference reference = GetReference(field.Parent);

            if (scopes.Any() && scopes.Peek() is BinaryScope binaryScope)
            {
                CharFieldValueChangedHistoryEntry.Serialize(reference, field.Name, value, binaryScope);
            }
            else
            {
                var entry = new CharFieldValueChangedHistoryEntry(reference, field.Name, value);

                AddEntry(entry, source);
            }
        }

        internal void AddStringValueChangedEntry(StringField field, string value, ChangeSource source)
        {
            HistoryObjectReference reference = GetReference(field.Parent);

            if (scopes.Any() && scopes.Peek() is BinaryScope binaryScope)
            {
                StringFieldValueChangedHistoryEntry.Serialize(reference, field.Name, value, binaryScope);
            }
            else
            {
                var entry = new StringFieldValueChangedHistoryEntry(reference, field.Name, value);

                AddEntry(entry, source);
            }
        }


        private void DoApplyBinaryEntry(bool undo, BinaryHistoryEntry binaryEntry)
        {
            binaryEntry.Data.Seek(-(sizeof(long)), SeekOrigin.End);

            var reader = new BinaryReader(binaryEntry.Data);

            long nextEntryOffset = reader.ReadInt64();

            while (nextEntryOffset >= 0)
            {
                reader.BaseStream.Seek(nextEntryOffset, SeekOrigin.Begin);
                var entryType = (HistoryEntryType)reader.ReadByte();           

                switch (entryType)
                {
                    case HistoryEntryType.SbyteValueChange:
                    {
                        long instanceId = reader.ReadInt64();
                        string fieldName = reader.ReadString();

                        // This item must exist in objectCache
                        var reference = objectCache[instanceId].Item;

                        // It must have specified field
                        var field = reference.Fields[fieldName] as SbyteField;

                        field.Apply(reader, entryType, undo, HandleSnapshotUpdated);
                        break;
                    }
                    case HistoryEntryType.ByteValueChange:
                    {
                        long instanceId = reader.ReadInt64();
                        string fieldName = reader.ReadString();

                        // This item must exist in objectCache
                        var reference = objectCache[instanceId].Item;

                        // It must have specified field
                        var field = reference.Fields[fieldName] as ByteField;

                        field.Apply(reader, entryType, undo, HandleSnapshotUpdated);
                        break;
                    }
                    case HistoryEntryType.ShortValueChange:
                    {
                        long instanceId = reader.ReadInt64();
                        string fieldName = reader.ReadString();

                        // This item must exist in objectCache
                        var reference = objectCache[instanceId].Item;

                        // It must have specified field
                        var field = reference.Fields[fieldName] as ShortField;

                        field.Apply(reader, entryType, undo, HandleSnapshotUpdated);
                        break;
                    }
                    case HistoryEntryType.UshortValueChange:
                    {
                        long instanceId = reader.ReadInt64();
                        string fieldName = reader.ReadString();

                        // This item must exist in objectCache
                        var reference = objectCache[instanceId].Item;

                        // It must have specified field
                        var field = reference.Fields[fieldName] as UshortField;

                        field.Apply(reader, entryType, undo, HandleSnapshotUpdated);
                        break;
                    }
                    case HistoryEntryType.IntValueChange:
                    {
                        long instanceId = reader.ReadInt64();
                        string fieldName = reader.ReadString();

                        // This item must exist in objectCache
                        var reference = objectCache[instanceId].Item;

                        // It must have specified field
                        var field = reference.Fields[fieldName] as IntField;

                        field.Apply(reader, entryType, undo, HandleSnapshotUpdated);
                        break;
                    }
                    case HistoryEntryType.UintValueChange:
                    {
                        long instanceId = reader.ReadInt64();
                        string fieldName = reader.ReadString();

                        // This item must exist in objectCache
                        var reference = objectCache[instanceId].Item;

                        // It must have specified field
                        var field = reference.Fields[fieldName] as UintField;

                        field.Apply(reader, entryType, undo, HandleSnapshotUpdated);
                        break;
                    }
                    case HistoryEntryType.LongValueChange:
                    {
                        long instanceId = reader.ReadInt64();
                        string fieldName = reader.ReadString();

                        // This item must exist in objectCache
                        var reference = objectCache[instanceId].Item;

                        // It must have specified field
                        var field = reference.Fields[fieldName] as LongField;

                        field.Apply(reader, entryType, undo, HandleSnapshotUpdated);
                        break;
                    }
                    case HistoryEntryType.UlongValueChange:
                    {
                        long instanceId = reader.ReadInt64();
                        string fieldName = reader.ReadString();

                        // This item must exist in objectCache
                        var reference = objectCache[instanceId].Item;

                        // It must have specified field
                        var field = reference.Fields[fieldName] as UlongField;

                        field.Apply(reader, entryType, undo, HandleSnapshotUpdated);
                        break;
                    }
                    case HistoryEntryType.FloatValueChange:
                    {
                        long instanceId = reader.ReadInt64();
                        string fieldName = reader.ReadString();

                        // This item must exist in objectCache
                        var reference = objectCache[instanceId].Item;

                        // It must have specified field
                        var field = reference.Fields[fieldName] as FloatField;

                        field.Apply(reader, entryType, undo, HandleSnapshotUpdated);
                        break;
                    }
                    case HistoryEntryType.DoubleValueChange:
                    {
                        long instanceId = reader.ReadInt64();
                        string fieldName = reader.ReadString();

                        // This item must exist in objectCache
                        var reference = objectCache[instanceId].Item;

                        // It must have specified field
                        var field = reference.Fields[fieldName] as DoubleField;

                        field.Apply(reader, entryType, undo, HandleSnapshotUpdated);
                        break;
                    }
                    case HistoryEntryType.DecimalValueChange:
                    {
                        long instanceId = reader.ReadInt64();
                        string fieldName = reader.ReadString();

                        // This item must exist in objectCache
                        var reference = objectCache[instanceId].Item;

                        // It must have specified field
                        var field = reference.Fields[fieldName] as DecimalField;

                        field.Apply(reader, entryType, undo, HandleSnapshotUpdated);
                        break;
                    }
                    case HistoryEntryType.BoolValueChange:
                    {
                        long instanceId = reader.ReadInt64();
                        string fieldName = reader.ReadString();

                        // This item must exist in objectCache
                        var reference = objectCache[instanceId].Item;

                        // It must have specified field
                        var field = reference.Fields[fieldName] as BoolField;

                        field.Apply(reader, entryType, undo, HandleSnapshotUpdated);
                        break;
                    }
                    case HistoryEntryType.CharValueChange:
                    {
                        long instanceId = reader.ReadInt64();
                        string fieldName = reader.ReadString();

                        // This item must exist in objectCache
                        var reference = objectCache[instanceId].Item;

                        // It must have specified field
                        var field = reference.Fields[fieldName] as CharField;

                        field.Apply(reader, entryType, undo, HandleSnapshotUpdated);
                        break;
                    }
                    case HistoryEntryType.StringValueChange:
                    {
                        long instanceId = reader.ReadInt64();
                        string fieldName = reader.ReadString();

                        // This item must exist in objectCache
                        var reference = objectCache[instanceId].Item;

                        // It must have specified field
                        var field = reference.Fields[fieldName] as StringField;

                        field.Apply(reader, entryType, undo, HandleSnapshotUpdated);
                        break;
                    }
                    case HistoryEntryType.RefFieldChange:
                    {
                        long instanceId = reader.ReadInt64();
                        string fieldName = reader.ReadString();

                        // This item must exist in objectCache
                        var reference = objectCache[instanceId].Item;

                        // It must have specified field
                        var field = reference.Fields[fieldName];

                        field.Apply(reader, entryType, undo, HandleSnapshotUpdated);
                        break;
                    }
                    case HistoryEntryType.CollectionClearedChange:
                    case HistoryEntryType.CollectionItemAddedChange:
                    case HistoryEntryType.CollectionItemInsertedChange:
                    case HistoryEntryType.CollectionItemRemovedChange:
                    case HistoryEntryType.CollectionItemReplacedChange:
                    case HistoryEntryType.CollectionRestoredChange:
                    {
                        long instanceId = reader.ReadInt64();
                        string collectionName = reader.ReadString();

                        // This item must exit in objectCache
                        var reference = objectCache[instanceId].Item;

                        // It must have specific collection
                        var collection = reference.Collections[collectionName];

                        collection.Apply(reader, entryType, undo, HandleSnapshotUpdated);

                        break;
                    }
                }

                nextEntryOffset = reader.ReadInt64();
            }
        }        
    }
}