﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SmartModel.Infrastructure
{
    internal delegate void OnLastReferenceRemovedDelegate(HistoryObjectReference sender);

    internal class HistoryObjectReference
    {
        private int references = 0;
        private readonly OnLastReferenceRemovedDelegate onLastReferenceRemovedHandler;

        public HistoryObjectReference(long instanceId, BaseFieldItemContainer item, OnLastReferenceRemovedDelegate onLastReferenceRemovedHandler)
        {
            this.onLastReferenceRemovedHandler = onLastReferenceRemovedHandler ?? throw new ArgumentNullException(nameof(onLastReferenceRemovedHandler));
            InstanceId = instanceId;
            Item = item;
        }

        public void AddReference()
        {
            references++;
        }

        public void RemoveReference()
        {
            references--;
            if (references == 0)
                onLastReferenceRemovedHandler(this);
        }

        public long InstanceId { get; }
        public BaseFieldItemContainer Item { get; set; }
    }
}
