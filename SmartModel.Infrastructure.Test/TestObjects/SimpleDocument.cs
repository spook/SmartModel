﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;

namespace SmartModel.Infrastructure.Test.TestObjects
{
    [XmlRoot("SimpleObject")]
    public class SimpleDocument : BaseDocument
    {
        // Fields are published for purpose of tests only.
        [XmlIgnore]
        public readonly IField<int> intField;
        [XmlIgnore]
        public readonly IField<string> stringField;
        [XmlIgnore]
        public readonly IField<SimpleItem> itemField;
        [XmlIgnore]
        public readonly RefCollection<SimpleCollectionItem> collection;
        [XmlIgnore]
        public readonly Collection<int> intCollection;
        [XmlIgnore]
        public readonly Collection<string> stringCollection;

        public SimpleDocument()
        {
            intField = RegisterIntField(nameof(IntProp), 42);
            stringField = RegisterStringField(nameof(StringProp), "Test");
            itemField = RegisterRefField<SimpleItem>(nameof(ItemProp), new SimpleItem());
            collection = RegisterRefCollection<SimpleCollectionItem>(nameof(Collection));
            intCollection = RegisterCollection<int>(nameof(IntCollection));
            stringCollection = RegisterCollection<string>(nameof(StringCollection));
        }

        [XmlAttribute("IntField")]
        public int IntProp
        {
            get => intField.Value;
            set => intField.Value = value;
        }

        [XmlAttribute("StringField")]
        public string StringProp
        {
            get => stringField.Value;
            set => stringField.Value = value;
        }

        [XmlElement("ItemField")]
        public SimpleItem ItemProp
        {
            get => itemField.Value;
            set => itemField.Value = value;
        }

        [XmlArray("Collection")]
        [XmlArrayItem(ElementName = "Item", Type = typeof(SimpleCollectionItem))]
        public RefCollection<SimpleCollectionItem> Collection => collection;
        
        [XmlArray("IntCollection")]
        public Collection<int> IntCollection => intCollection;

        [XmlArray("StringCollection")]
        public Collection<string> StringCollection => stringCollection;
    }
}
