﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;

namespace SmartModel.Infrastructure.Test.TestObjects
{
    public class SimpleCollectionItem : BaseCollectionItem
    {
        [XmlIgnore]
        public readonly IField<int> intField;

        public SimpleCollectionItem()
        {
            intField = RegisterIntField(nameof(IntProp), 96);
        }

        [XmlAttribute("IntField")]
        public int IntProp
        {
            get => intField.Value;
            set => intField.Value = value;
        }
    }
}
