﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;

namespace SmartModel.Infrastructure.Test.TestObjects
{
    public class SimpleItem : BaseItem
    {
        // Fields are published for purpose of tests only.
        [XmlIgnore]
        public readonly IField<int> intField;
        [XmlIgnore]
        public readonly IField<SimpleSubitem> subitemField;

        public SimpleItem()
        {
            intField = RegisterIntField(nameof(IntField), 58);
            subitemField = RegisterRefField<SimpleSubitem>(nameof(SimpleSubitem), new SimpleSubitem());
        }

        [XmlAttribute("IntField")]
        public int IntField
        {
            get => intField.Value;
            set => intField.Value = value;
        }

        [XmlElement("SubitemField")]
        public SimpleSubitem SubitemField
        {
            get => subitemField.Value;
            set => subitemField.Value = value;
        }
    }
}
