﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;

namespace SmartModel.Infrastructure.Test.TestObjects
{
    public class SimpleSubitem : BaseItem
    {
        private IField<int> intField;

        public SimpleSubitem()
        {
            intField = RegisterIntField(nameof(IntField), 0);
        }

        [XmlAttribute("IntField")]
        public int IntField
        {
            get => intField.Value;
            set => intField.Value = value;
        }
    }
}
