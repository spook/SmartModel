﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using SmartModel.Infrastructure.Test.TestObjects;
using System;

namespace SmartModel.Infrastructure.Test
{
    [TestClass]
    public class ParentingTest
    {
        [TestMethod]
        public void ItemParentingTest()
        {
            // Arrange

            var obj = new SimpleDocument();
            var oldItem = obj.ItemProp;

            // Act

            var newItem = new SimpleItem();
            obj.ItemProp = newItem;

            // Assert

            Assert.AreEqual(null, oldItem.Parent);
            Assert.AreEqual(obj, newItem.Parent);
        }

        [TestMethod]
        public void CollectionAddParentingTest()
        {
            // Arrange

            var obj = new SimpleDocument();

            // Act

            var item = new SimpleCollectionItem();
            obj.Collection.Add(item);

            // Assert

            Assert.AreEqual(obj.Collection, item.Parent);
        }

        [TestMethod]
        public void CollectionRemoveParentingTest()
        {
            // Arrange

            var obj = new SimpleDocument();
            var item = new SimpleCollectionItem();
            obj.Collection.Add(item);

            // Act

            obj.Collection.Remove(item);

            // Assert

            Assert.AreEqual(null, item.Parent);
        }

        [TestMethod]
        public void CollectionRemoveAtParentingTest()
        {
            // Arrange

            var obj = new SimpleDocument();
            var item = new SimpleCollectionItem();
            obj.Collection.Add(item);

            // Act

            obj.Collection.RemoveAt(0);

            // Assert

            Assert.AreEqual(null, item.Parent);
        }

        [TestMethod]
        public void CollectionInsertParentingTest()
        {
            // Arrange

            var obj = new SimpleDocument();

            // Act

            var item = new SimpleCollectionItem();
            obj.Collection.Insert(0, item);

            // Assert

            Assert.AreEqual(obj.Collection, item.Parent);
        }

        [TestMethod]
        public void CollectionIndexedSetParentingTest()
        {
            // Arrange

            var obj = new SimpleDocument();
            var oldItem = new SimpleCollectionItem();
            var newItem = new SimpleCollectionItem();
            obj.Collection.Add(oldItem);

            // Act

            obj.Collection[0] = newItem;

            // Assert

            Assert.AreEqual(null, oldItem.Parent);
            Assert.AreEqual(obj.Collection, newItem.Parent);
        }
    }
}
