﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using SmartModel.Infrastructure.Test.TestObjects;
using System;

namespace SmartModel.Infrastructure.Test
{
    [TestClass]
    public class DocumentManagementTests
    {
        [TestMethod]
        public void SelfDocumentCheck()
        {
            // Arrange

            var obj = new SimpleDocument();

            // Assert

            Assert.AreEqual(obj, obj.Document);
        }

        [TestMethod]
        public void ValueFieldDocumentCheck()
        {
            // Arrange

            var obj = new SimpleDocument();

            // Assert

            Assert.AreEqual(obj, ((IntField)obj.intField).Document);
        }

        [TestMethod]
        public void StringFieldDocumentCheck()
        {
            // Arrange

            var obj = new SimpleDocument();

            // Assert

            Assert.AreEqual(obj, ((StringField)obj.stringField).Document);
        }

        [TestMethod]
        public void RefFieldDocumentCheck()
        {
            // Arrange

            var obj = new SimpleDocument();

            // Assert

            Assert.AreEqual(obj, ((RefField<SimpleItem>)obj.itemField).Document);
        }

        [TestMethod]
        public void SubItemDocumentCheck()
        {
            // Arrange

            var obj = new SimpleDocument();

            // Assert

            Assert.AreEqual(obj, obj.ItemProp.Document);
        }

        [TestMethod]
        public void SubItemChangeDocumentClearCheck()
        {
            // Arrange

            var obj = new SimpleDocument();
            var newItem = new SimpleItem();

            // Force resolve document for old item
            var oldItem = obj.ItemProp;
            var temp = oldItem.Document;

            // Act

            obj.ItemProp = newItem;

            // Assert

            Assert.AreEqual(null, oldItem.Document);
            Assert.AreEqual(obj, newItem.Document);
        }

        // TODO collections
    }
}
