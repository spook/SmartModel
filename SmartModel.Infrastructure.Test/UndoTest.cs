﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using SmartModel.Infrastructure.Test.TestObjects;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SmartModel.Infrastructure.Test
{
    [TestClass]
    public class UndoTest
    {
        [TestMethod, TestCategory("Basic redo tests")]
        public void RefCollectionAddRedoTest()
        {
            // Arrange

            var obj = new SimpleDocument();
            obj.Collection.Add(new SimpleCollectionItem { IntProp = 10 });

            var history = new History();
            obj.History = history;

            // Act

            obj.Collection.Add(new SimpleCollectionItem { IntProp = 20 });
            history.Undo();
            history.Redo();

            // Assert

            Assert.AreEqual(2, obj.Collection.Count);
            Assert.AreEqual(10, obj.Collection[0].IntProp);
            Assert.AreEqual(20, obj.Collection[1].IntProp);

            history.Dispose();
        }

        [TestMethod, TestCategory("Basic undo tests")]
        public void RefCollectionAddUndoTest()
        {
            // Arrange

            var obj = new SimpleDocument();
            obj.Collection.Add(new SimpleCollectionItem { IntProp = 10 });

            var history = new History();
            obj.History = history;

            // Act

            obj.Collection.Add(new SimpleCollectionItem { IntProp = 20 });
            history.Undo();

            // Assert

            Assert.AreEqual(1, obj.Collection.Count);
            Assert.AreEqual(10, obj.Collection[0].IntProp);

            history.Dispose();
        }

        [TestMethod, TestCategory("Basic redo tests")]
        public void RefCollectionClearRedoTest()
        {
            // Arrange

            var obj = new SimpleDocument();
            obj.Collection.Add(new SimpleCollectionItem { IntProp = 10 });
            var item = new SimpleCollectionItem { IntProp = 30 };
            obj.Collection.Add(item);

            var history = new History();
            obj.History = history;

            // Act

            obj.Collection.Clear();
            history.Undo();
            history.Redo();

            // Assert

            Assert.AreEqual(0, obj.Collection.Count);

            history.Dispose();
        }

        [TestMethod, TestCategory("Basic undo tests")]
        public void RefCollectionClearUndoTest()
        {
            // Arrange

            var obj = new SimpleDocument();
            obj.Collection.Add(new SimpleCollectionItem { IntProp = 10 });
            var item = new SimpleCollectionItem { IntProp = 30 };
            obj.Collection.Add(item);

            var history = new History();
            obj.History = history;

            // Act

            obj.Collection.Clear();
            history.Undo();

            // Assert

            Assert.AreEqual(2, obj.Collection.Count);
            Assert.AreEqual(10, obj.Collection[0].IntProp);
            Assert.AreEqual(30, obj.Collection[1].IntProp);

            history.Dispose();
        }

        [TestMethod, TestCategory("Basic redo tests")]
        public void RefCollectionInsertRedoTest()
        {
            // Arrange

            var obj = new SimpleDocument();
            obj.Collection.Add(new SimpleCollectionItem { IntProp = 10 });
            obj.Collection.Add(new SimpleCollectionItem { IntProp = 30 });

            var history = new History();
            obj.History = history;

            // Act

            obj.Collection.Insert(1, new SimpleCollectionItem { IntProp = 20 });
            history.Undo();
            history.Redo();

            // Assert

            Assert.AreEqual(3, obj.Collection.Count);
            Assert.AreEqual(10, obj.Collection[0].IntProp);
            Assert.AreEqual(20, obj.Collection[1].IntProp);
            Assert.AreEqual(30, obj.Collection[2].IntProp);

            history.Dispose();
        }

        [TestMethod, TestCategory("Basic undo tests")]
        public void RefCollectionInsertUndoTest()
        {
            // Arrange

            var obj = new SimpleDocument();
            obj.Collection.Add(new SimpleCollectionItem { IntProp = 10 });
            obj.Collection.Add(new SimpleCollectionItem { IntProp = 30 });

            var history = new History();
            obj.History = history;

            // Act

            obj.Collection.Insert(1, new SimpleCollectionItem { IntProp = 20 });
            history.Undo();

            // Assert

            Assert.AreEqual(2, obj.Collection.Count);
            Assert.AreEqual(10, obj.Collection[0].IntProp);
            Assert.AreEqual(30, obj.Collection[1].IntProp);

            history.Dispose();
        }

        [TestMethod, TestCategory("Basic redo tests")]
        public void RefCollectionRemoveAtRedoTest()
        {
            // Arrange

            var obj = new SimpleDocument();
            obj.Collection.Add(new SimpleCollectionItem { IntProp = 10 });
            obj.Collection.Add(new SimpleCollectionItem { IntProp = 30 });

            var history = new History();
            obj.History = history;

            // Act

            obj.Collection.RemoveAt(0);
            history.Undo();
            history.Redo();

            // Assert

            Assert.AreEqual(1, obj.Collection.Count);
            Assert.AreEqual(30, obj.Collection[0].IntProp);

            history.Dispose();
        }

        [TestMethod, TestCategory("Basic undo tests")]
        public void RefCollectionRemoveAtUndoTest()
        {
            // Arrange

            var obj = new SimpleDocument();
            obj.Collection.Add(new SimpleCollectionItem { IntProp = 10 });
            obj.Collection.Add(new SimpleCollectionItem { IntProp = 30 });

            var history = new History();
            obj.History = history;

            // Act

            obj.Collection.RemoveAt(0);
            history.Undo();

            // Assert

            Assert.AreEqual(2, obj.Collection.Count);
            Assert.AreEqual(10, obj.Collection[0].IntProp);
            Assert.AreEqual(30, obj.Collection[1].IntProp);

            history.Dispose();
        }

        [TestMethod, TestCategory("Basic redo tests")]
        public void RefCollectionRemoveRedoTest()
        {
            // Arrange

            var obj = new SimpleDocument();
            obj.Collection.Add(new SimpleCollectionItem { IntProp = 10 });
            var item = new SimpleCollectionItem { IntProp = 30 };
            obj.Collection.Add(item);

            var history = new History();
            obj.History = history;

            // Act

            obj.Collection.Remove(item);
            history.Undo();
            history.Redo();

            // Assert

            Assert.AreEqual(1, obj.Collection.Count);
            Assert.AreEqual(10, obj.Collection[0].IntProp);

            history.Dispose();
        }

        [TestMethod, TestCategory("Basic undo tests")]
        public void RefCollectionRemoveUndoTest()
        {
            // Arrange

            var obj = new SimpleDocument();
            obj.Collection.Add(new SimpleCollectionItem { IntProp = 10 });
            var item = new SimpleCollectionItem { IntProp = 30 };
            obj.Collection.Add(item);

            var history = new History();
            obj.History = history;

            // Act

            obj.Collection.Remove(item);
            history.Undo();

            // Assert

            Assert.AreEqual(2, obj.Collection.Count);
            Assert.AreEqual(10, obj.Collection[0].IntProp);
            Assert.AreEqual(30, obj.Collection[1].IntProp);

            history.Dispose();
        }

        [TestMethod, TestCategory("Basic redo tests")]
        public void RefCollectionReplaceRedoTest()
        {
            // Arrange

            var obj = new SimpleDocument();
            obj.Collection.Add(new SimpleCollectionItem { IntProp = 10 });
            obj.Collection.Add(new SimpleCollectionItem { IntProp = 30 });

            var history = new History();
            obj.History = history;

            // Act

            obj.Collection[1] = new SimpleCollectionItem { IntProp = 20 };
            history.Undo();
            history.Redo();

            // Assert

            Assert.AreEqual(2, obj.Collection.Count);
            Assert.AreEqual(10, obj.Collection[0].IntProp);
            Assert.AreEqual(20, obj.Collection[1].IntProp);

            history.Dispose();
        }

        [TestMethod, TestCategory("Basic undo tests")]
        public void RefCollectionReplaceUndoTest()
        {
            // Arrange

            var obj = new SimpleDocument();
            obj.Collection.Add(new SimpleCollectionItem { IntProp = 10 });
            obj.Collection.Add(new SimpleCollectionItem { IntProp = 30 });

            var history = new History();
            obj.History = history;

            // Act

            obj.Collection[1] = new SimpleCollectionItem { IntProp = 20 };
            history.Undo();

            // Assert

            Assert.AreEqual(2, obj.Collection.Count);
            Assert.AreEqual(10, obj.Collection[0].IntProp);
            Assert.AreEqual(30, obj.Collection[1].IntProp);

            history.Dispose();
        }

        [TestMethod, TestCategory("Basic redo tests")]
        public void RefFieldRedoTest()
        {
            // Arrange
            var obj = new SimpleDocument();
            obj.ItemProp = new SimpleItem { IntField = 10 };
            var history = new History();
            obj.History = history;

            // Act
            obj.ItemProp = new SimpleItem { IntField = 20 };

            // Assert
            Assert.AreEqual(20, obj.ItemProp.IntField);

            history.Undo();
            Assert.AreEqual(10, obj.ItemProp.IntField);

            history.Redo();
            Assert.AreEqual(20, obj.ItemProp.IntField);

            history.Dispose();
        }

        [TestMethod, TestCategory("Basic undo tests")]
        public void RefFieldUndoTest()
        {
            // Arrange

            var obj = new SimpleDocument();
            obj.ItemProp = new SimpleItem { IntField = 10 };
            var history = new History();
            obj.History = history;

            // Act

            obj.ItemProp = new SimpleItem { IntField = 20 };
            history.Undo();

            // Assert

            Assert.AreEqual(10, obj.ItemProp.IntField);

            history.Dispose();
        }

        [TestMethod, TestCategory("Basic undo tests")]
        public void StringFieldUndoTest()
        {
            // Arrange

            var obj = new SimpleDocument();
            obj.StringProp = "10";
            var history = new History();
            obj.History = history;

            // Act

            obj.StringProp = "20";
            history.Undo();

            // Assert

            Assert.AreEqual("10", obj.StringProp);

            history.Dispose();
        }

        [TestMethod, TestCategory("History integrity tests")]
        public void UndoMultipleChangesTest()
        {
            // Arrange
            var obj = new SimpleDocument();
            var history = new History();
            obj.History = history;

            // Act
            obj.IntProp = 10;
            obj.IntProp = 20;
            obj.IntProp = 30;
            obj.IntProp = 40;

            // Assert
            Assert.AreEqual(40, obj.IntProp);

            history.Undo();
            Assert.AreEqual(30, obj.IntProp);

            history.Undo();
            Assert.AreEqual(20, obj.IntProp);

            history.Undo();
            Assert.AreEqual(10, obj.IntProp);

            history.Dispose();
        }

        [TestMethod, TestCategory("History integrity tests")]
        public void UndoMultipleCollectionChangesTest()
        {
            // Arrange
            var obj = new SimpleDocument();
            var history = new History();
            obj.History = history;

            // Act
            obj.Collection.Add(new SimpleCollectionItem { IntProp = 10 });
            obj.Collection.Add(new SimpleCollectionItem { IntProp = 20 });
            obj.Collection.Add(new SimpleCollectionItem { IntProp = 30 });
            obj.Collection.Add(new SimpleCollectionItem { IntProp = 40 });

            // Assert
            Assert.AreEqual(4, obj.Collection.Count);
            Assert.AreEqual(40, obj.Collection.Last().IntProp);

            history.Undo();
            Assert.AreEqual(3, obj.Collection.Count);
            Assert.AreEqual(30, obj.Collection.Last().IntProp);

            history.Undo();
            Assert.AreEqual(2, obj.Collection.Count);
            Assert.AreEqual(20, obj.Collection.Last().IntProp);

            history.Undo();
            Assert.AreEqual(1, obj.Collection.Count);
            Assert.AreEqual(10, obj.Collection.Last().IntProp);

            history.Dispose();
        }

        [TestMethod, TestCategory("Multiple level undo tests")]
        public void UndoMultipleLevelTest()
        {
            // Arrange
            var obj = new SimpleDocument();
            var history = new History();
            obj.History = history;

            // Act
            obj.ItemProp.SubitemField.IntField = 10;
            obj.ItemProp.SubitemField = new SimpleSubitem { IntField = 20 };
            obj.ItemProp = new SimpleItem { SubitemField = new SimpleSubitem { IntField = 30 } };

            // Assert

            Assert.AreEqual(30, obj.ItemProp.SubitemField.IntField);

            history.Undo();
            Assert.AreEqual(20, obj.ItemProp.SubitemField.IntField);

            history.Undo();
            Assert.AreEqual(10, obj.ItemProp.SubitemField.IntField);

            history.Dispose();
        }

        [TestMethod, TestCategory("History integrity tests")]
        public void UndoMultipleRefChangesTest()
        {
            // Arrange
            var obj = new SimpleDocument();
            var history = new History();
            obj.History = history;

            // Act
            obj.ItemProp = new SimpleItem { IntField = 10 };
            obj.ItemProp = new SimpleItem { IntField = 20 };
            obj.ItemProp = new SimpleItem { IntField = 30 };
            obj.ItemProp = new SimpleItem { IntField = 40 };

            // Assert
            Assert.AreEqual(40, obj.ItemProp.IntField);

            history.Undo();
            Assert.AreEqual(30, obj.ItemProp.IntField);

            history.Undo();
            Assert.AreEqual(20, obj.ItemProp.IntField);

            history.Undo();
            Assert.AreEqual(10, obj.ItemProp.IntField);

            history.Dispose();
        }

        [TestMethod, TestCategory("Basic redo tests")]
        public void ValueFieldRedoTest()
        {
            // Arrange
            var obj = new SimpleDocument();
            obj.IntProp = 10;
            var history = new History();
            obj.History = history;

            // Act
            obj.IntProp = 20;

            // Assert
            Assert.AreEqual(20, obj.IntProp);

            history.Undo();
            Assert.AreEqual(10, obj.IntProp);

            history.Redo();
            Assert.AreEqual(20, obj.IntProp);

            history.Dispose();
        }

        [TestMethod, TestCategory("Basic undo tests")]
        public void ValueFieldUndoTest()
        {
            // Arrange

            var obj = new SimpleDocument();
            obj.IntProp = 10;
            var history = new History();
            obj.History = history;

            // Act

            obj.IntProp = 20;
            history.Undo();

            // Assert

            Assert.AreEqual(10, obj.IntProp);

            history.Dispose();
        }

        [TestMethod, TestCategory("Basic redo tests")]
        public void CollectionAddRedoTest()
        {
            // Arrange

            var obj = new SimpleDocument();
            obj.IntCollection.Add(10);

            var history = new History();
            obj.History = history;

            // Act

            obj.IntCollection.Add(20);
            history.Undo();
            history.Redo();

            // Assert

            Assert.AreEqual(2, obj.IntCollection.Count);
            Assert.AreEqual(10, obj.IntCollection[0]);
            Assert.AreEqual(20, obj.IntCollection[1]);

            history.Dispose();
        }

        [TestMethod, TestCategory("Basic undo tests")]
        public void CollectionAddUndoTest()
        {
            // Arrange

            var obj = new SimpleDocument();
            obj.IntCollection.Add(10);

            var history = new History();
            obj.History = history;

            // Act

            obj.IntCollection.Add(20);
            history.Undo();

            // Assert

            Assert.AreEqual(1, obj.IntCollection.Count);
            Assert.AreEqual(10, obj.IntCollection[0]);

            history.Dispose();
        }

        [TestMethod, TestCategory("Basic redo tests")]
        public void CollectionClearRedoTest()
        {
            // Arrange

            var obj = new SimpleDocument();
            obj.IntCollection.Add(10);
            obj.IntCollection.Add(30);

            var history = new History();
            obj.History = history;

            // Act

            obj.IntCollection.Clear();
            history.Undo();
            history.Redo();

            // Assert

            Assert.AreEqual(0, obj.IntCollection.Count);

            history.Dispose();
        }

        [TestMethod, TestCategory("Basic undo tests")]
        public void CollectionClearUndoTest()
        {
            // Arrange

            var obj = new SimpleDocument();
            obj.IntCollection.Add(10);            
            obj.IntCollection.Add(30);

            var history = new History();
            obj.History = history;

            // Act

            obj.IntCollection.Clear();
            history.Undo();

            // Assert

            Assert.AreEqual(2, obj.IntCollection.Count);
            Assert.AreEqual(10, obj.IntCollection[0]);
            Assert.AreEqual(30, obj.IntCollection[1]);

            history.Dispose();
        }

        [TestMethod, TestCategory("Basic redo tests")]
        public void CollectionInsertRedoTest()
        {
            // Arrange

            var obj = new SimpleDocument();
            obj.IntCollection.Add(10);
            obj.IntCollection.Add(30);

            var history = new History();
            obj.History = history;

            // Act

            obj.IntCollection.Insert(1, 20);
            history.Undo();
            history.Redo();

            // Assert

            Assert.AreEqual(3, obj.IntCollection.Count);
            Assert.AreEqual(10, obj.IntCollection[0]);
            Assert.AreEqual(20, obj.IntCollection[1]);
            Assert.AreEqual(30, obj.IntCollection[2]);

            history.Dispose();
        }

        [TestMethod, TestCategory("Basic undo tests")]
        public void CollectionInsertUndoTest()
        {
            // Arrange

            var obj = new SimpleDocument();
            obj.IntCollection.Add(10);
            obj.IntCollection.Add(30);

            var history = new History();
            obj.History = history;

            // Act

            obj.IntCollection.Insert(1, 20);
            history.Undo();

            // Assert

            Assert.AreEqual(2, obj.IntCollection.Count);
            Assert.AreEqual(10, obj.IntCollection[0]);
            Assert.AreEqual(30, obj.IntCollection[1]);

            history.Dispose();
        }

        [TestMethod, TestCategory("Basic redo tests")]
        public void CollectionRemoveAtRedoTest()
        {
            // Arrange

            var obj = new SimpleDocument();
            obj.IntCollection.Add(10);
            obj.IntCollection.Add(30);

            var history = new History();
            obj.History = history;

            // Act

            obj.IntCollection.RemoveAt(0);
            history.Undo();
            history.Redo();

            // Assert

            Assert.AreEqual(1, obj.IntCollection.Count);
            Assert.AreEqual(30, obj.IntCollection[0]);

            history.Dispose();
        }

        [TestMethod, TestCategory("Basic undo tests")]
        public void CollectionRemoveAtUndoTest()
        {
            // Arrange

            var obj = new SimpleDocument();
            obj.IntCollection.Add(10);
            obj.IntCollection.Add(30);

            var history = new History();
            obj.History = history;

            // Act

            obj.IntCollection.RemoveAt(0);
            history.Undo();

            // Assert

            Assert.AreEqual(2, obj.IntCollection.Count);
            Assert.AreEqual(10, obj.IntCollection[0]);
            Assert.AreEqual(30, obj.IntCollection[1]);

            history.Dispose();
        }

        [TestMethod, TestCategory("Basic redo tests")]
        public void CollectionRemoveRedoTest()
        {
            // Arrange

            var obj = new SimpleDocument();
            obj.IntCollection.Add(10);
            var item = 30;
            obj.IntCollection.Add(item);

            var history = new History();
            obj.History = history;

            // Act

            obj.IntCollection.Remove(item);
            history.Undo();
            history.Redo();

            // Assert

            Assert.AreEqual(1, obj.IntCollection.Count);
            Assert.AreEqual(10, obj.IntCollection[0]);

            history.Dispose();
        }

        [TestMethod, TestCategory("Basic undo tests")]
        public void CollectionRemoveUndoTest()
        {
            // Arrange

            var obj = new SimpleDocument();
            obj.IntCollection.Add(10);
            var item = 30;
            obj.IntCollection.Add(item);

            var history = new History();
            obj.History = history;

            // Act

            obj.IntCollection.Remove(item);
            history.Undo();

            // Assert

            Assert.AreEqual(2, obj.IntCollection.Count);
            Assert.AreEqual(10, obj.IntCollection[0]);
            Assert.AreEqual(30, obj.IntCollection[1]);

            history.Dispose();
        }

        [TestMethod, TestCategory("Basic redo tests")]
        public void CollectionReplaceRedoTest()
        {
            // Arrange

            var obj = new SimpleDocument();
            obj.IntCollection.Add(10);
            obj.IntCollection.Add(30);

            var history = new History();
            obj.History = history;

            // Act

            obj.IntCollection[1] = 20;
            history.Undo();
            history.Redo();

            // Assert

            Assert.AreEqual(2, obj.IntCollection.Count);
            Assert.AreEqual(10, obj.IntCollection[0]);
            Assert.AreEqual(20, obj.IntCollection[1]);

            history.Dispose();
        }

        [TestMethod, TestCategory("Basic undo tests")]
        public void CollectionReplaceUndoTest()
        {
            // Arrange

            var obj = new SimpleDocument();
            obj.IntCollection.Add(10);
            obj.IntCollection.Add(30);

            var history = new History();
            obj.History = history;

            // Act

            obj.IntCollection[1] = 20;
            history.Undo();

            // Assert

            Assert.AreEqual(2, obj.IntCollection.Count);
            Assert.AreEqual(10, obj.IntCollection[0]);
            Assert.AreEqual(30, obj.IntCollection[1]);

            history.Dispose();
        }

        [TestMethod]
        public void TestRemovingRefObj()
        {
            // Arrange

            var obj = new SimpleDocument();
            obj.ItemProp = new SimpleItem { IntField = 20 };
            var history = new History();
            obj.History = history;

            // Act

            obj.ItemProp.IntField = 30;

            var temp = obj.ItemProp;
            obj.ItemProp = null;

            temp.IntField = 100;

            history.Undo();

            // Assert

            Assert.AreNotEqual(temp, obj.ItemProp);
            Assert.AreEqual(30, obj.ItemProp.IntField);

            history.Undo();

            Assert.AreEqual(20, obj.ItemProp.IntField);
        }

        [TestMethod]
        public void ChangeCollectionItemFieldTest()
        {
            // Arrange

            var obj = new SimpleDocument();
            obj.Collection.Add(new SimpleCollectionItem { IntProp = 20 });
            var history = new History();
            obj.History = history;

            // Act

            obj.Collection[0].IntProp = 30;
            history.Undo();

            // Assert

            Assert.AreEqual(20, obj.Collection[0].IntProp);
        }
    }
}
