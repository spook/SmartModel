﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using SmartModel.Infrastructure.Test.TestObjects;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SmartModel.Infrastructure.Test
{
    [TestClass]
    public class ScopeTest
    {
        [TestMethod, TestCategory("Simple scope tests")]
        public void ScopeTest1()
        {
            // Arrange
            var obj = new SimpleDocument();
            obj.IntProp = 10;
            obj.StringProp = "Test";
            var history = new History();
            obj.History = history;

            // Act
            using (history.StartUndoScope())
            {
                obj.IntProp = 20;
                obj.StringProp = "Test 2";
            }

            // Assert
            history.Undo();
            Assert.AreEqual(10, obj.IntProp);
            Assert.AreEqual("Test", obj.StringProp);

            history.Dispose();
        }

        [TestMethod, TestCategory("Simple scope tests")]
        public void ScopeTest2()
        {
            // Arrange
            var obj = new SimpleDocument();
            obj.IntProp = 10;
            obj.StringProp = "Test";
            var history = new History();
            obj.History = history;

            // Act
            using (history.StartUndoScope())
            {
                obj.IntProp = 20;
                obj.StringProp = "Test 2";
            }

            using (history.StartUndoScope())
            {
                obj.IntProp = 30;
                obj.StringProp = "Test 3";
            }

            // Assert
            history.Undo();
            Assert.AreEqual(20, obj.IntProp);
            Assert.AreEqual("Test 2", obj.StringProp);

            history.Undo();
            Assert.AreEqual(10, obj.IntProp);
            Assert.AreEqual("Test", obj.StringProp);

            history.Dispose();
        }

        [TestMethod, TestCategory("Simple scope tests")]
        public void NestedScopeTest()
        {
            // Arrange
            var obj = new SimpleDocument();
            obj.IntProp = 10;
            obj.StringProp = "Test";
            var history = new History();
            obj.History = history;

            // Act
            using (history.StartUndoScope())
            {
                obj.IntProp = 20;
                obj.StringProp = "Test 2";

                using (history.StartUndoScope())
                {
                    obj.IntProp = 30;
                    obj.StringProp = "Test 3";
                }
            }

            // Assert
            Assert.AreEqual(30, obj.IntProp);
            Assert.AreEqual("Test 3", obj.StringProp);

            history.Undo();

            Assert.AreEqual(10, obj.IntProp);
            Assert.AreEqual("Test", obj.StringProp);

            history.Dispose();
        }

        [TestMethod, TestCategory("Simple scope tests")]
        public void ScopeRedoTest1()
        {
            // Arrange
            var obj = new SimpleDocument();
            obj.IntProp = 10;
            obj.StringProp = "Test";
            var history = new History();
            obj.History = history;

            // Act
            using (history.StartUndoScope())
            {
                obj.IntProp = 20;
                obj.StringProp = "Test 2";
            }

            // Assert
            history.Undo();
            history.Redo();
            Assert.AreEqual(20, obj.IntProp);
            Assert.AreEqual("Test 2", obj.StringProp);

            history.Dispose();
        }

        [TestMethod, TestCategory("Simple scope tests")]
        public void ScopeRedoTest2()
        {
            // Arrange
            var obj = new SimpleDocument();
            obj.IntProp = 10;
            obj.StringProp = "Test";
            var history = new History();
            obj.History = history;

            // Act
            using (history.StartUndoScope())
            {
                obj.IntProp = 20;
                obj.StringProp = "Test 2";
            }

            using (history.StartUndoScope())
            {
                obj.IntProp = 30;
                obj.StringProp = "Test 3";
            }

            // Assert
            history.Undo();
            history.Undo();

            history.Redo();
            Assert.AreEqual(20, obj.IntProp);
            Assert.AreEqual("Test 2", obj.StringProp);

            history.Redo();
            Assert.AreEqual(30, obj.IntProp);
            Assert.AreEqual("Test 3", obj.StringProp);

            history.Dispose();
        }

        [TestMethod, TestCategory("Simple scope tests")]
        public void NestedScopeRedoTest()
        {
            // Arrange
            var obj = new SimpleDocument();
            obj.IntProp = 10;
            obj.StringProp = "Test";
            var history = new History();
            obj.History = history;

            // Act
            using (history.StartUndoScope())
            {
                obj.IntProp = 20;
                obj.StringProp = "Test 2";

                using (history.StartUndoScope())
                {
                    obj.IntProp = 30;
                    obj.StringProp = "Test 3";
                }
            }

            // Assert
            Assert.AreEqual(30, obj.IntProp);
            Assert.AreEqual("Test 3", obj.StringProp);

            history.Undo();
            history.Redo();

            Assert.AreEqual(30, obj.IntProp);
            Assert.AreEqual("Test 3", obj.StringProp);

            history.Dispose();
        }
    }
}
