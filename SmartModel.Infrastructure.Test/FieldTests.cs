﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using SmartModel.Infrastructure.Test.TestObjects;
using System;

namespace SmartModel.Infrastructure.Test
{
    [TestClass]
    public class FieldTests
    {
        [TestMethod]
        public void DefaultValueTest()
        {
            // Arrange

            var obj = new SimpleDocument();

            // Assert

            Assert.AreEqual(42, obj.IntProp);
            Assert.AreEqual("Test", obj.StringProp);
        }

        [TestMethod]
        public void SetGetValueTest()
        {
            // Arrange

            var obj = new SimpleDocument();

            // Act

            obj.IntProp = 5;

            // Assert

            Assert.AreEqual(5, obj.IntProp);
        }

        [TestMethod]
        public void GetSetRefValueTest()
        {
            // Arrange

            var obj = new SimpleDocument();
            var newItem = new SimpleItem();

            // Act

            obj.ItemProp = newItem;

            // Assert

            Assert.AreEqual(newItem, obj.ItemProp);
        }

        [TestMethod]
        public void ItemAccessTest()
        {
            // Arrange

            var obj = new SimpleDocument();

            // Assert

            Assert.AreEqual(58, obj.ItemProp.IntField);
        }
    }
}
