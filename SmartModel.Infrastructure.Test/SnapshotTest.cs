﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using SmartModel.Infrastructure.Test.TestObjects;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SmartModel.Infrastructure.Test
{
    [TestClass]
    public class SnapshotTest
    {
        [TestMethod]
        public void CreateSnapshotTest()
        {
            // Arrange

            var obj = new SimpleDocument();
            obj.IntProp = 1;
            obj.StringProp = "Test";

            var collectionItem = new SimpleCollectionItem();
            collectionItem.IntProp = 2;
            obj.Collection.Add(collectionItem);

            var item = new SimpleItem();
            item.IntField = 3;
            obj.ItemProp = item;

            // Act

            var cloned = (SimpleDocument)obj.CreateSnapshot(null);

            // Assert

            // Values are copied properly
            Assert.AreEqual(1, cloned.IntProp);
            Assert.AreEqual("Test", cloned.StringProp);
            Assert.AreEqual(1, cloned.Collection.Count);
            Assert.AreEqual(2, cloned.Collection[0].IntProp);
            Assert.AreEqual(3, cloned.ItemProp.IntField);

            // InstanceIDs differs
            Assert.AreNotEqual(obj.InstanceId, cloned.InstanceId);
            Assert.AreNotEqual(obj.Collection[0].InstanceId, cloned.Collection[0].InstanceId);
            Assert.AreNotEqual(obj.ItemProp.InstanceId, cloned.ItemProp.InstanceId);
        }
    }
}
