﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using SmartModel.Infrastructure.Test.TestObjects;
using System;
using System.IO;
using System.Xml;
using System.Xml.Serialization;

namespace SmartModel.Infrastructure.Test
{
    [TestClass]
    public class XmlSerializationTest
    {
        public const bool OUTPUT_XML = true;

        private SimpleDocument XmlReload(SimpleDocument source)
        {
            XmlSerializer serializer = new XmlSerializer(typeof(SimpleDocument));
            var ms = new MemoryStream();
            serializer.Serialize(ms, source);

            ms.Seek(0, SeekOrigin.Begin);

            if (OUTPUT_XML)
            {
                TextReader reader = new StreamReader(ms);
                var xml = reader.ReadToEnd();

                XmlDocument doc = new XmlDocument();
                doc.LoadXml(xml);

                StringWriter sw = new StringWriter();
                XmlTextWriter xtw = new XmlTextWriter(sw);
                xtw.Formatting = Formatting.Indented;
                doc.WriteTo(xtw);

                System.Diagnostics.Debug.WriteLine(sw.ToString());
                ms.Seek(0, SeekOrigin.Begin);
            }

            SimpleDocument result = (SimpleDocument)serializer.Deserialize(ms);
            return result;
        }

        [TestMethod]
        public void SerializeValueFieldTest()
        {
            // Arrange

            var obj = new SimpleDocument();
            obj.IntProp = 12345;

            // Act

            obj = XmlReload(obj);

            // Assert

            Assert.AreEqual(12345, obj.IntProp);
        }

        [TestMethod]
        public void SerializeRefFieldTest()
        {
            // Arrange

            var obj = new SimpleDocument();
            var item = new SimpleItem();
            item.IntField = 12345;
            obj.ItemProp = item;

            // Act

            obj = XmlReload(obj);

            // Assert

            Assert.AreEqual(12345, item.IntField);
        }
        
        [TestMethod]
        public void SerializeRefCollectionTest()
        {
            // Arrange

            var obj = new SimpleDocument();
            var item = new SimpleCollectionItem();
            item.IntProp = 1;
            obj.Collection.Add(item);
            var item2 = new SimpleCollectionItem();
            item2.IntProp = 2;
            obj.Collection.Add(item2);

            // Act

            obj = XmlReload(obj);

            Assert.AreEqual(2, obj.Collection.Count);
            Assert.AreEqual(1, obj.Collection[0].IntProp);
            Assert.AreEqual(2, obj.Collection[1].IntProp);
        }

        [TestMethod]
        public void SerializeCollectionTest()
        {
            // Arrange

            var obj = new SimpleDocument();
            obj.IntCollection.Add(1);
            obj.IntCollection.Add(2);

            obj.StringCollection.Add("Test 1");
            obj.StringCollection.Add("Test 2");

            // Act

            obj = XmlReload(obj);

            Assert.AreEqual(2, obj.IntCollection.Count);
            Assert.AreEqual(1, obj.IntCollection[0]);
            Assert.AreEqual(2, obj.IntCollection[1]);

            Assert.AreEqual(2, obj.StringCollection.Count);
            Assert.AreEqual("Test 1", obj.StringCollection[0]);
            Assert.AreEqual("Test 2", obj.StringCollection[1]);
        }
    }
}
