﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using SmartModel.Infrastructure.Test.TestObjects;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SmartModel.Infrastructure.Test
{
    [TestClass]
    public class BinaryScopeTest
    {
        [TestMethod, TestCategory("Simple binary scope tests")]
        public void PropertyChangeScopeTest1()
        {
            // Arrange
            var obj = new SimpleDocument();
            obj.IntProp = 1;
            var history = new History();
            obj.History = history;

            // Act
            using (history.StartBinaryUndoScope())
            {
                obj.IntProp = 2;
            }

            // Assert
            history.Undo();
            Assert.AreEqual(1, obj.IntProp);
        }

        [TestMethod, TestCategory("Simple binary scope tests")]
        public void PropertyChangeScopeTest2()
        {
            // Arrange
            var obj = new SimpleDocument();
            obj.StringProp = "Test 123";
            var history = new History();
            obj.History = history;

            // Act
            using (history.StartBinaryUndoScope())
            {
                obj.StringProp = "Ala ma kota";
            }

            // Assert
            history.Undo();
            Assert.AreEqual("Test 123", obj.StringProp);
        }

        [TestMethod, TestCategory("Simple binary scope tests")]
        public void PropertyChangeScopeTest3()
        {
            // Arrange
            var obj = new SimpleDocument();
            obj.IntProp = 10;
            obj.StringProp = "Test";
            var history = new History();
            obj.History = history;

            // Act
            using (history.StartUndoScope())
            {
                obj.IntProp = 20;
                obj.StringProp = "Test 2";
            }

            // Assert
            history.Undo();
            Assert.AreEqual(10, obj.IntProp);
            Assert.AreEqual("Test", obj.StringProp);

            history.Dispose();
        }

        [TestMethod, TestCategory("Simple binary scope tests")]
        public void PropertyChangeScopeTest4()
        {
            // Arrange
            var obj = new SimpleDocument();
            obj.IntProp = 10;
            obj.StringProp = "Test";
            var history = new History();
            obj.History = history;

            // Act
            using (history.StartBinaryUndoScope())
            {
                obj.IntProp = 20;
                obj.StringProp = "Test 2";
            }

            using (history.StartBinaryUndoScope())
            {
                obj.IntProp = 30;
                obj.StringProp = "Test 3";
            }

            // Assert
            history.Undo();
            Assert.AreEqual(20, obj.IntProp);
            Assert.AreEqual("Test 2", obj.StringProp);

            history.Undo();
            Assert.AreEqual(10, obj.IntProp);
            Assert.AreEqual("Test", obj.StringProp);

            history.Dispose();
        }

        [TestMethod, TestCategory("Simple binary scope tests")]
        public void NestedBinaryScopeTest()
        {
            // Arrange
            var obj = new SimpleDocument();
            obj.IntProp = 10;
            obj.StringProp = "Test";
            var history = new History();
            obj.History = history;

            // Act
            using (history.StartBinaryUndoScope())
            {
                obj.IntProp = 20;
                obj.StringProp = "Test 2";

                using (history.StartUndoScope())
                {
                    obj.IntProp = 30;
                    obj.StringProp = "Test 3";
                }
            }

            // Assert
            Assert.AreEqual(30, obj.IntProp);
            Assert.AreEqual("Test 3", obj.StringProp);

            history.Undo();

            Assert.AreEqual(10, obj.IntProp);
            Assert.AreEqual("Test", obj.StringProp);

            history.Dispose();
        }

        [TestMethod, TestCategory("Simple binary scope tests")]
        public void BinaryScopeRedoTest1()
        {
            // Arrange
            var obj = new SimpleDocument();
            obj.IntProp = 10;
            obj.StringProp = "Test";
            var history = new History();
            obj.History = history;

            // Act
            using (history.StartBinaryUndoScope())
            {
                obj.IntProp = 20;
                obj.StringProp = "Test 2";
            }

            // Assert
            history.Undo();
            history.Redo();
            Assert.AreEqual(20, obj.IntProp);
            Assert.AreEqual("Test 2", obj.StringProp);

            history.Dispose();
        }

        [TestMethod, TestCategory("Simple binary scope tests")]
        public void BinaryScopeRedoTest2()
        {
            // Arrange
            var obj = new SimpleDocument();
            obj.IntProp = 10;
            obj.StringProp = "Test";
            var history = new History();
            obj.History = history;

            // Act
            using (history.StartBinaryUndoScope())
            {
                obj.IntProp = 20;
                obj.StringProp = "Test 2";
            }

            using (history.StartBinaryUndoScope())
            {
                obj.IntProp = 30;
                obj.StringProp = "Test 3";
            }

            // Assert
            history.Undo();
            history.Undo();

            history.Redo();
            Assert.AreEqual(20, obj.IntProp);
            Assert.AreEqual("Test 2", obj.StringProp);

            history.Redo();
            Assert.AreEqual(30, obj.IntProp);
            Assert.AreEqual("Test 3", obj.StringProp);

            history.Dispose();
        }

        [TestMethod, TestCategory("Simple binary scope tests")]
        public void NestedBinaryScopeRedoTest()
        {
            // Arrange
            var obj = new SimpleDocument();
            obj.IntProp = 10;
            obj.StringProp = "Test";
            var history = new History();
            obj.History = history;

            // Act
            using (history.StartBinaryUndoScope())
            {
                obj.IntProp = 20;
                obj.StringProp = "Test 2";

                using (history.StartUndoScope())
                {
                    obj.IntProp = 30;
                    obj.StringProp = "Test 3";
                }
            }

            // Assert
            Assert.AreEqual(30, obj.IntProp);
            Assert.AreEqual("Test 3", obj.StringProp);

            history.Undo();
            history.Redo();

            Assert.AreEqual(30, obj.IntProp);
            Assert.AreEqual("Test 3", obj.StringProp);

            history.Dispose();
        }      

        [TestMethod, TestCategory("Value collection binary scope tests")]
        public void ValueCollectionAddBinaryScopeTest()
        {
            // Arrange
            var obj = new SimpleDocument();
            obj.IntCollection.Add(10);

            var history = new History();
            obj.History = history;

            // Act
            using (history.StartBinaryUndoScope())
            {
                obj.IntCollection.Add(20);
            }

            // Assert
            history.Undo();

            Assert.AreEqual(1, obj.IntCollection.Count);
            Assert.AreEqual(10, obj.IntCollection[0]);

            history.Dispose();
        }

        [TestMethod, TestCategory("Value collection binary scope tests")]
        public void ValueCollectionInsertBinaryScopeTest()
        {
            // Arrange
            var obj = new SimpleDocument();
            obj.IntCollection.Add(10);
            obj.IntCollection.Add(20);

            var history = new History();
            obj.History = history;

            // Act
            using (history.StartBinaryUndoScope())
            {
                obj.IntCollection.Insert(1, 30);
            }

            // Assert
            history.Undo();

            Assert.AreEqual(2, obj.IntCollection.Count);
            Assert.AreEqual(10, obj.IntCollection[0]);
            Assert.AreEqual(20, obj.IntCollection[1]);

            history.Dispose();
        }

        [TestMethod, TestCategory("Value collection binary scope tests")]
        public void ValueCollectionRemoveBinaryScopeTest()
        {
            // Arrange
            var obj = new SimpleDocument();
            obj.IntCollection.Add(10);
            obj.IntCollection.Add(20);

            var history = new History();
            obj.History = history;

            // Act
            using (history.StartBinaryUndoScope())
            {
                obj.IntCollection.RemoveAt(1);
            }

            // Assert
            history.Undo();

            Assert.AreEqual(2, obj.IntCollection.Count);
            Assert.AreEqual(10, obj.IntCollection[0]);
            Assert.AreEqual(20, obj.IntCollection[1]);

            history.Dispose();
        }

        [TestMethod, TestCategory("Value collection binary scope tests")]
        public void ValueCollectionReplaceBinaryScopeTest()
        {
            // Arrange
            var obj = new SimpleDocument();
            obj.IntCollection.Add(10);
            obj.IntCollection.Add(20);

            var history = new History();
            obj.History = history;

            // Act
            using (history.StartBinaryUndoScope())
            {
                obj.IntCollection[1] = 30;
            }

            // Assert
            history.Undo();

            Assert.AreEqual(2, obj.IntCollection.Count);
            Assert.AreEqual(10, obj.IntCollection[0]);
            Assert.AreEqual(20, obj.IntCollection[1]);

            history.Dispose();
        }

        [TestMethod, TestCategory("Value collection binary scope tests")]
        public void ValueCollectionClearBinaryScopeTest()
        {
            // Arrange
            var obj = new SimpleDocument();
            obj.IntCollection.Add(10);
            obj.IntCollection.Add(20);

            var history = new History();
            obj.History = history;

            // Act
            using (history.StartBinaryUndoScope())
            {
                obj.IntCollection.Clear();
            }

            // Assert
            history.Undo();

            Assert.AreEqual(2, obj.IntCollection.Count);
            Assert.AreEqual(10, obj.IntCollection[0]);
            Assert.AreEqual(20, obj.IntCollection[1]);

            history.Dispose();
        }

        [TestMethod, TestCategory("Reference field tests")]
        public void RefFieldChangeBinaryScopeTest()
        {
            // Arrange
            var obj = new SimpleDocument();
            var item = new SimpleItem { IntField = 10 };
            obj.ItemProp = item;
            var history = new History();
            obj.History = history;

            // Act
            using (history.StartBinaryUndoScope())
            {
                obj.ItemProp = new SimpleItem { IntField = 20 };
            }
            
            item.IntField = 99;

            // Assert
            history.Undo();
            Assert.AreEqual(10, obj.ItemProp.IntField);
        }

        [TestMethod, TestCategory("Value collection binary scope tests")]
        public void RefCollectionAddBinaryScopeTest()
        {
            // Arrange
            var obj = new SimpleDocument();
            obj.Collection.Add(new SimpleCollectionItem { IntProp = 10 });

            var history = new History();
            obj.History = history;

            // Act
            using (history.StartBinaryUndoScope())
            {
                obj.Collection.Add(new SimpleCollectionItem { IntProp = 20 });
            }

            // Assert
            history.Undo();

            Assert.AreEqual(1, obj.Collection.Count);
            Assert.AreEqual(10, obj.Collection[0].IntProp);

            history.Dispose();
        }

        [TestMethod, TestCategory("Value collection binary scope tests")]
        public void RefCollectionInsertBinaryScopeTest()
        {
            // Arrange
            var obj = new SimpleDocument();
            obj.Collection.Add(new SimpleCollectionItem { IntProp = 10 });
            obj.Collection.Add(new SimpleCollectionItem { IntProp = 20 });

            var history = new History();
            obj.History = history;

            // Act
            using (history.StartBinaryUndoScope())
            {
                obj.Collection.Insert(1, new SimpleCollectionItem { IntProp = 30 });
            }

            // Assert
            history.Undo();

            Assert.AreEqual(2, obj.Collection.Count);
            Assert.AreEqual(10, obj.Collection[0].IntProp);
            Assert.AreEqual(20, obj.Collection[1].IntProp);

            history.Dispose();
        }

        [TestMethod, TestCategory("Value collection binary scope tests")]
        public void RefCollectionRemoveBinaryScopeTest()
        {
            // Arrange
            var obj = new SimpleDocument();
            obj.Collection.Add(new SimpleCollectionItem { IntProp = 10 });
            obj.Collection.Add(new SimpleCollectionItem { IntProp = 20 });

            var history = new History();
            obj.History = history;

            // Act
            using (history.StartBinaryUndoScope())
            {
                obj.Collection.RemoveAt(1);
            }

            // Assert
            history.Undo();

            Assert.AreEqual(2, obj.Collection.Count);
            Assert.AreEqual(10, obj.Collection[0].IntProp);
            Assert.AreEqual(20, obj.Collection[1].IntProp);

            history.Dispose();
        }

        [TestMethod, TestCategory("Value collection binary scope tests")]
        public void RefCollectionReplaceBinaryScopeTest()
        {
            // Arrange
            var obj = new SimpleDocument();
            obj.Collection.Add(new SimpleCollectionItem { IntProp = 10 });
            obj.Collection.Add(new SimpleCollectionItem { IntProp = 20 });

            var history = new History();
            obj.History = history;

            // Act
            using (history.StartBinaryUndoScope())
            {
                obj.Collection[1] = new SimpleCollectionItem { IntProp = 30 };
            }

            // Assert
            history.Undo();

            Assert.AreEqual(2, obj.Collection.Count);
            Assert.AreEqual(10, obj.Collection[0].IntProp);
            Assert.AreEqual(20, obj.Collection[1].IntProp);

            history.Dispose();
        }

        [TestMethod, TestCategory("Value collection binary scope tests")]
        public void RefCollectionClearBinaryScopeTest()
        {
            // Arrange
            var obj = new SimpleDocument();
            obj.Collection.Add(new SimpleCollectionItem { IntProp = 10 });
            obj.Collection.Add(new SimpleCollectionItem { IntProp = 20 });

            var history = new History();
            obj.History = history;

            // Act
            using (history.StartBinaryUndoScope())
            {
                obj.Collection.Clear();
            }

            // Assert
            history.Undo();

            Assert.AreEqual(2, obj.Collection.Count);
            Assert.AreEqual(10, obj.Collection[0].IntProp);
            Assert.AreEqual(20, obj.Collection[1].IntProp);

            history.Dispose();
        }
    }
}
